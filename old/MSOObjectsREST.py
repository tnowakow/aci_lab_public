import ErrorHandler


class MSOobject:
    """ Class that includes general session related variables - url to APIC, Tenant and Pod under which all lower level
    objects should be configured.
    Those parameters are available for other ACI object related classes.
    """

    def __init__(self, url, session, dry=True):
        self.session = session
        self.base_API_url = url + '/api/v1/'
        self.url = url
        self.dry = dry
        self.well_known_ports = {
            'https': '443',
            'smtp': '25',
            'http': '80',
            'ftp-data': '20',
            'dns': '53',
            'pop3': '110',
            'rtsp': '554',
            'ssh': '22'
        }
        # defined in function set_base_vars
        self.schema_url = None
        self.template_path = None
        self.query_url = None
        self.template_displayName = None
        self.tenant_id = None
        self.template_name = None
        self.schema_id = None
        self.pod = None

    def get_sites(self):
        return self.session.invoke(self.base_API_url + 'sites')

    def get_tenants(self):
        return self.session.invoke(self.base_API_url + 'tenants')

    def get_schemas(self):
        return self.session.invoke(self.base_API_url + 'schemas')

    def refresh_schema(self):
        return self.session.invoke(self.schema_url)

    def update_schema(self, payload, requester=None):
        self.session.invoke(self.schema_url, method='PUT', data=payload, requester=requester)

    def set_base_vars(self, data):
        self.schema_url = self.base_API_url + 'schemas/' + data['Schema-id']
        self.template_path = '/templates/' + data['Template-name'] + '/'
        self.query_url = self.schema_url + self.template_path
        self.template_displayName = data['Template-displayName']
        self.tenant_id = data['Tenant-id']
        self.template_name = data['Template-name']
        self.schema_id = data['Schema-id']
        self.pod = data['Pod-name']

    def check_if_exist(self, object_type, object_name):
        url = self.query_url + f'{object_type + "s"}/{object_name}'
        return False if self.session.invoke(url, method='GET')[object_type] is None else True

    @staticmethod
    def compare_params(conf=None, input_data=None):
        if not conf:
            conf = {}
        if not input_data:
            input_data = {}
        diff = False
        for param in conf:
            if param not in input_data:
                diff = True
                print('-' * 40)
                print(param, '- running config: ', conf[param])
                print(param, '- input data: not present in input data')
            else:
                if conf[param] != input_data[param]:
                    diff = True
                    print('-' * 40)
                    print(param, '- running config: ', conf[param])
                    print(param, '- input data:     ', input_data[param])
        for param in input_data:
            if param not in conf:
                diff = True
                print('-' * 40)
                print(param, '- running config:  not present in running config')
                print(param, '- input data:     ', input_data[param])
        if diff:
            print('-'*80)
        return diff

    def create_vrf_patch(self, params, op='add'):
        payload = [{
            "op": op,
            "path": self.template_path + 'vrfs/-',
            "value": {
                "displayName": params[3]['name'],
                "name": params[3]['name'],
                "pcEnfPref": params[3].get('pcEnfPref', 'enforced'),
                "pcEnfDir": params[3].get('pcEnfDir', 'ingress'),
                "ipDataPlaneLearning": params[3].get('ipDataPlaneLearning', 'enabled')
            }
        }]
        self.session.invoke(self.schema_url, method='PATCH', data=payload)

    def create_vrf(self, params):
        schema = self.refresh_schema()
        vrf_name = params[3]['name']
        vrf_found = False
        payload = {
            "name": vrf_name,
            "displayName": vrf_name,
            "vrfRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/vrfs/{vrf_name}",
            "l3MCast": params[3].get('l3MCast', False),
            "preferredGroup": params[3].get('preferredGroup', False),
            "vzAnyEnabled": params[3].get('vzAnyEnabled', False),
            "vzAnyProviderContracts": params[3].get('vzAnyProviderContracts', []),
            "vzAnyConsumerContracts": params[3].get('vzAnyConsumerContracts', []),
            "rpConfigs": params[3].get('rpConfigs', []),
            "pcEnfPref": params[3].get('pcEnfPref', 'enforced'),
            "pcEnfDir": params[3].get('pcEnfDir', 'ingress'),
            "ipDataPlaneLearning": params[3].get('ipDataPlaneLearning', 'enabled')
        }

        for template in schema['templates']:
            if template['displayName'] == self.template_displayName and template['tenantId'] == self.tenant_id \
                    and template['name'] == self.template_name:
                for vrf in template['vrfs']:
                    if vrf['name'] == vrf_name or vrf['displayName'] == vrf_name:
                        vrf_found = True
                        if self.dry:
                            print(f"VRF: {vrf_name} - already exists - comparing parameters:")
                            diff = self.compare_params(conf=vrf, input_data=payload)
                            if not diff:
                                print(f'VRF {vrf_name} no difference in parameters.')
                        else:
                            for param in payload:
                                vrf[param] = payload[param]
                if self.dry and not vrf_found:
                    print(f'Creating VRF object - {vrf_name}')
                elif not self.dry and not vrf_found:
                    template['vrfs'].append(payload)
        self.update_schema(schema, requester=f"create_vrf, {vrf_name}")

    def create_BD(self, params):
        vrf = params[4].get('vrf', None)
        schema = self.refresh_schema()
        subnets = []
        bd_site_found = False
        bd_template_found = False
        if 'subnets' in params[4]:
            for subnet in params[4]['subnets']:
                subnets_dict_tmp = {
                    "ip": subnet['ip'],
                    "scope": "private" if "private" in subnet["scope"] else "public",
                    "shared": True if "shared" in subnet["scope"] else False,
                    "querier": True if "querier" in subnet["ctrl"] else False,
                    "noDefaultGateway": True if "noDefaultGateway" in subnet["ctrl"] else False,
                    "virtual": subnet.get("virtual", False)
                }
                subnets.append(subnets_dict_tmp)

        if params[4].get('l2Stretch', False):
            l2stretch = True
        else:
            l2stretch = False
        payload = {
            "name": params[3],
            "displayName": params[3],
            "bdRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/bds/{params[3]}",
            "l2UnknownUnicast": params[4].get('l2UnknownUnicast', "proxy"),
            "intersiteBumTrafficAllow": params[4].get('intersiteBumTrafficAllow', False),
            "optimizeWanBandwidth": params[4].get('optimizeWanBandwidth', False),
            "l2Stretch": l2stretch,
            "subnets": subnets if l2stretch else [],
            "vrfRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/vrfs/{vrf}",
            "unkMcastAct": params[4].get('unkMcastAct', "flood"),
            "v6unkMcastAct": params[4].get('v6unkMcastAct', "flood"),
            "arpFlood": True if params[4].get('arpFlood', 'true') == 'true' else False,
            "multiDstPktAct": params[4].get('multiDstPktAct', "bd-flood")
        }

        if payload["arpFlood"] and payload["l2Stretch"]:
            raise ErrorHandler.MutualExlusiveParametersSet(msg=payload)

        if not payload["l2Stretch"]:
            del payload["optimizeWanBandwidth"]
            del payload["intersiteBumTrafficAllow"]
            for site in schema['sites']:
                if site['templateName'] == self.template_name:
                    payload_site = {
                        "bdRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/bds/{params[3]}",
                        "subnets": subnets
                    }
                    for bd in site['bds']:
                        if bd['bdRef'] == payload_site['bdRef']:
                            bd_site_found = True
                            if self.dry:
                                print(f'BD {params[3]} - site - found in config - comparing parameters:')
                            if bd['subnets'] != payload_site['subnets']:
                                if self.dry:
                                    print(f'{params[3]}: different site specific subnets:')
                                    print('subnets: ', '- running config: ', bd['subnets'])
                                    print('subnets: ', '- input data: ', payload_site['subnets'])
                                    print('-' * 80)
                                else:
                                    bd['subnets'] = payload_site['subnets']
                            else:
                                if self.dry:
                                    print(f'BD {params[3]} - site - no difference in parameters.')
                                print('-' * 80)
                    if self.dry and not bd_site_found:
                        print(f'Creating BD object (site) - {params[3]}')
                    elif not self.dry and not bd_site_found:
                        site['bds'].append(payload_site)

        for template in schema['templates']:
            if template['displayName'] == self.template_displayName and template['tenantId'] == self.tenant_id \
                    and template['name'] == self.template_name:
                for bds in template['bds']:
                    if bds['name'] == payload['name'] or bds['displayName'] == payload['displayName']:
                        bd_template_found = True
                        if self.dry:
                            print(f'BD {params[3]} - template - found in config - comparing parameters:')
                            diff = self.compare_params(conf=bds, input_data=payload)
                            if not diff:
                                print(f'BD {params[3]} no difference in parameters')
                        else:
                            for param in payload:
                                bds[param] = payload[param]
                if self.dry and not bd_template_found:
                    print(f'Creating BD object (template) - {params[3]}')
                elif not self.dry and not bd_template_found:
                    template['bds'].append(payload)
                break
        self.update_schema(schema, requester=f"create_BD, {payload['name']}")

    def create_filter(self, params):
        schema = self.refresh_schema()
        for template in schema['templates']:
            if template['name'] == self.template_name:
                for fil in template['filters']:
                    if fil['name'] == params[3]['name']:
                        self.session.log.info(f'Filter object - {params[3]["name"]} already configured.')
                        return
        payload = {
            "name": params[3]['name'],
            "displayName": params[3]['name'],
            "filterRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/filters/{params[3]['name']}",
            "entries": []
        }
        if params[3]["entries"]:
            for entry in params[3]["entries"]:
                tmp_entry_dict = {
                    "name": entry["name"],
                    "displayName": entry["name"],
                    "etherType": entry.get("etherT", "unspecified"),
                    "arpFlag": entry.get("arpFlag", "unspecified"),
                    "ipProtocol": entry.get("prot", "unspecified"),
                    "matchOnlyFragments": entry.get("matchOnlyFragments", False),
                    "stateful": entry.get("stateful", False),
                    "sourceFrom": entry.get("sFromPort", "unspecified"),
                    "sourceTo": entry.get("sToPort", "unspecified"),
                    "destinationFrom": entry.get("dFromPort", "unspecified"),
                    "destinationTo": entry.get("dToPort", "unspecified"),
                    "tcpSessionRules": entry.get("tcpSessionRules", ["unspecified"])
                }
                payload["entries"].append(tmp_entry_dict)
        for template in schema['templates']:
            if template['displayName'] == self.template_displayName and template['tenantId'] == self.tenant_id \
                    and template['name'] == self.template_name:
                template['filters'].append(payload)
                break
        self.session.log.info(f'Creating FILTER object - {params[3]["name"]}')
        self.update_schema(schema, requester=f"create_filter, {payload['name']}")

    def create_contract(self, params):
        filters = []
        existing_filters = []
        schema = self.refresh_schema()
        for template in schema['templates']:
            if template['displayName'] == self.template_displayName and template['tenantId'] == self.tenant_id \
                    and template['name'] == self.template_name:
                for contract in template['contracts']:
                    if contract['name'] == params[3]['name']:
                        self.session.log.info(f'Contract object - {params[3]["name"]} already configured.')
                        return
                for fil in template['filters']:
                    existing_filters.append(fil.get('name', ''))
        existing_filters = list(set(existing_filters))
        for subj in params[3]['subjects']:
            for filt in subj['filters']:
                if 'name' not in filt or filt['name'] not in existing_filters:
                    self.session.log.error(f'Non exisitng filter object - {filt}')
                if filt not in filters:
                    tmp_directives = []
                    if not filt.get('directives', None):
                        tmp_directives = ['none']
                    else:
                        tmp_directives.append(filt['directives'])
                    tmp_filt = {
                        "filterRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/filters/{filt['name']}",
                        "directives": tmp_directives,
                        "action": filt.get("action", "permit"),
                        "priorityOverride": filt.get("priorityOverride", "default")
                    }
                    filters.append(tmp_filt)
        scope = params[3].get("scope", "context")
        if scope == 'vrf':
            scope = 'context'
        payload = {
            "name": params[3]['name'],
            "displayName": params[3]['name'],
            "contractRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/contracts/{params[3]['name']}",
            "filterRelationships": filters,
            "prio": params[3].get("prio", "unspecified"),
            "scope": scope,
            "filterType": params[3].get("filterType", "bothWay"),
            "filterRelationshipsProviderToConsumer": params[3].get("filterRelationshipsProviderToConsumer", []),
            "filterRelationshipsConsumerToProvider": params[3].get("filterRelationshipsConsumerToProvider", [])
        }
        for template in schema['templates']:
            if template['displayName'] == self.template_displayName and template['tenantId'] == self.tenant_id \
                    and template['name'] == self.template_name:
                template['contracts'].append(payload)
                break
        self.session.log.info(f'Creating contract object - {params[3]["name"]}')
        self.update_schema(schema, requester=f"create_contract, {payload['name']}")

    def create_anps(self, params):
        schema = self.refresh_schema()
        anpRef = f"/schemas/{self.schema_id}/templates/{self.template_name}/anps/{params[2]}"
        epgRef = f"/schemas/{self.schema_id}/templates/{self.template_name}/anps/{params[2]}/epgs/{params[3]}"
        payload_epg = {
            "name": params[3],
            "displayName": params[3],
            "epgRef": epgRef,
            "contractRelationships": params[4].get('contractRelationships', []),
            "subnets": params[4].get('subnets', []),
            "uSegEpg": params[4].get("uSegEpg", False),
            "uSegAttrs": params[4].get('uSegAttrs', []),
            "intraEpg": params[4].get('intraEpg', "unenforced"),
            "prio": params[4].get('prio', "unspecified"),
            "proxyArp": params[4].get("proxyArp", False),
            "preferredGroup": params[4].get("preferredGroup", False),
            "bdRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/bds/{params[4]['bridge-domain']}",
            "vrfRef": params[4].get("vrfRef", ""),
            "selectors": params[4].get('selectors', []),
            "epgType": params[4].get("epgType", "application")
        }
        static_ports = []
        for port in params[4].get('static-ports', []):
            port_path = port['path']
            port_encap = int(port['encap'])
            port_mode = port.get('mode', 'regular')
            if port_mode == 'trunk':
                port_mode = 'regular'
            port_deploymentImmediacy = port.get('instrImedcy', 'lazy')
            if port_deploymentImmediacy == 'on-demand':
                port_deploymentImmediacy = 'lazy'
            if 'vpc' in port['path'].lower():
                type_p = 'vpc'
                vpc_id_1 = port_path.split('-')[1]
                vpc_id_2 = port_path.split('-')[2]
                path = f'topology/{self.pod}/protpaths-{vpc_id_1}-{vpc_id_2}/pathep-[{port_path}]'
            else:
                type_p = 'port'
                leaf_id = port_path.split('-')[1]
                port_id = port_path.split('-')[-1]
                path = f'topology/{self.pod}/paths-{leaf_id}/pathep-[{port_id}]'
            tmp_port_dict = {
                "type": type_p,
                "path": path,
                "portEncapVlan": port_encap,
                "deploymentImmediacy": port_deploymentImmediacy,
                "mode": port_mode
            }
            static_ports.append(tmp_port_dict)
        payload_epg_site = {
            "epgRef": epgRef,
            "domainAssociations": [{
                "dn": f"uni/phys-{params[4]['fabric-domain']}",
                "domainType": params[4].get('domainType', "physicalDomain"),
                "deployImmediacy": params[4].get('deployImmediacy', "lazy"),
                "resolutionImmediacy": params[4].get('resolutionImmediacy', "lazy")
            }],
            "staticPorts": static_ports,
            "staticLeafs": params[4].get('staticLeafs', []),
            "uSegAttrs": params[4].get('uSegAttrs', []),
            "subnets": params[4].get('subnets', []),
            "selectors": params[4].get('selectors', [])
        }
        for site in schema['sites']:
            existing_app = False
            if site['templateName'] == self.template_name:
                for anp in site['anps']:
                    if anp.get('anpRef', '') == anpRef:
                        anp['epgs'].append(payload_epg_site)
                        existing_app = True
                        break
                if not existing_app:
                    payload_site = {
                        "anpRef": anpRef,
                        "epgs": [payload_epg_site]
                    }
                    site['anps'].append(payload_site)
        for template in schema['templates']:
            existing_app = False
            if template['displayName'] == self.template_displayName and template['tenantId'] == self.tenant_id \
                    and template['name'] == self.template_name:
                for anp in template['anps']:
                    if anp['name'] == params[2]:
                        anp['epgs'].append(payload_epg)
                        existing_app = True
                        break
                if not existing_app:
                    payload = {
                        "name": params[2],
                        "displayName": params[2],
                        "anpRef": anpRef,
                        "epgs": [payload_epg]
                    }
                    template['anps'].append(payload)
        self.session.log.info(f'Creating Application Profile object - {params[2]}')
        self.update_schema(schema, requester=f"create_anps, {params[2]}")

    def assign_contract(self, params):
        schema = self.refresh_schema()
        epg_name = params[3]['epg']
        epg_provides = params[3].get('provides', [])
        epg_consumes = params[3].get('consumes', [])
        for template in schema['templates']:
            for anp in template['anps']:
                for epg in anp['epgs']:
                    if epg['name'] == epg_name:
                        for prov in epg_provides:
                            contractRef = f"/schemas/{self.schema_id}/templates/{self.template_name}/contracts/{prov}"
                            payload_provider = {
                                "relationshipType": "provider",
                                "contractRef": contractRef
                            }
                            if payload_provider not in epg['contractRelationships']:
                                epg['contractRelationships'].append(payload_provider)
                        for con in epg_consumes:
                            contractRef = f"/schemas/{self.schema_id}/templates/{self.template_name}/contracts/{con}"
                            payload_consumer = {
                                "relationshipType": "consumer",
                                "contractRef": contractRef
                            }
                            if payload_consumer not in epg['contractRelationships']:
                                epg['contractRelationships'].append(payload_consumer)
        self.session.log.info(f'Assigning Contract to EPG - {epg_name}')
        self.update_schema(schema, requester=f"assign_contract, {epg_name}")

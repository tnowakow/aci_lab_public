import ACI_Monitor_shared_methods as ACI_sh_meth
from RESTapi import RESTaci


class RESTSite:
    tenants_to_skip = ['infra', 'common', 'mgmt']

    def __init__(self, url, name):
        self.site_name = name
        self.url = url
        self.session = RESTaci('admin', 'ciscocisco', self.url)
        self.objects_paths = []
        self.aci_global_objects = {
            'Tenants': set(),
            'Apps': set(),
            'EPGs': set(),
            'Contracts': set()
        }
        self.contract_to_epg = {}
        self.contract_to_sub = {}
        self.filter_to_entry = {}
        self.tenants = None
        self.fill_dict()

    def fill_dict(self):
        self.tenants = self.get_tenants(self.session)
        tmp_dict_ext = {'Tenants': []}
        for tenant in self.tenants:
            tmp_dict_int = {
                'Name': tenant,
                'Apps': self.get_app_epg(tenant)
            }
            tmp_dict_ext['Tenants'].append(tmp_dict_int)
        self.objects_paths.append(tmp_dict_ext)
        self.epg_to_contract()
        self.contract_to_subject()
        self.subject_to_filter()
        self.filt_to_ent()
        self.subject_to_filter()

    def get_tenants(self, session_rest):
        url_tenant = f"{self.url}/api/node/class/fvTenant.json"
        tenants_to_return = []
        tenants = session_rest.invoke(url_tenant, method='GET')['imdata']
        if tenants:
            for tenant in tenants:
                curr_tenant = tenant.get('fvTenant', {}).get('attributes', {}).get('name', False)
                if curr_tenant and curr_tenant not in RESTSite.tenants_to_skip:
                    self.aci_global_objects['Tenants'].add(curr_tenant)
                    tenants_to_return.append(curr_tenant)
        return tenants_to_return

    def epg_to_contract(self):
        for obj in self.objects_paths:
            for tenant in obj['Tenants']:
                curr_tenant = tenant['Name']
                if curr_tenant not in self.contract_to_epg:
                    self.contract_to_epg[curr_tenant] = {}
                for app in tenant['Apps']:
                    curr_app = app['Name']
                    for epg in app['EPGs']:
                        curr_epg = epg
                        url_epg = f"{self.url}/api/node/mo/uni/tn-{curr_tenant}/ap-{curr_app}/epg-{curr_epg}.json?" \
                                  f"query-target=subtree&target-subtree-class=fvRsCons&" \
                                  f"target-subtree-class=fvRsConsIf,fvRsProtBy,fvRsProv,vzConsSubjLbl,vzProvSubjLbl," \
                                  f"vzConsLbl,vzProvLbl,fvRsIntraEpg&query-target=subtree"
                        epg_data = self.session.invoke(url_epg, method='GET')['imdata']
                        for con in epg_data:
                            prov = con.get('fvRsProv', {}).get('attributes', {}).get('tnVzBrCPName', False)
                            cons = con.get('fvRsCons', {}).get('attributes', {}).get('tnVzBrCPName', False)
                            if prov:
                                if curr_epg not in self.contract_to_epg[curr_tenant]:
                                    self.contract_to_epg[curr_tenant][curr_epg] = {'prov': [prov], 'cons': []}
                                else:
                                    if prov not in self.contract_to_epg[curr_tenant][curr_epg]['prov']:
                                        self.contract_to_epg[curr_tenant][curr_epg]['prov'].append(prov)
                            if cons:
                                if curr_epg not in self.contract_to_epg[curr_tenant]:
                                    self.contract_to_epg[curr_tenant][curr_epg] = {'prov': [], 'cons': [cons]}
                                else:
                                    if cons not in self.contract_to_epg[curr_tenant][curr_epg]['cons']:
                                        self.contract_to_epg[curr_tenant][curr_epg]['cons'].append(cons)

    def get_app_epg(self, tenant):
        url_app = f"{self.url}/api/node/mo/uni/tn-{tenant}.json?query-target=subtree&target-subtree-class=fvAEPg"
        apps = self.session.invoke(url_app, method='GET')['imdata']
        apps_to_return = []
        tmp_app = []
        for app in apps:
            curr_dn = app.get('fvAEPg', {}).get('attributes', {}).get('dn', False)
            if curr_dn:
                curr_app_name = curr_dn.split('ap-')[-1].split('/')[0]
                curr_epg_name = curr_dn.split('epg-')[-1]
                self.aci_global_objects['Apps'].add(curr_app_name)
                self.aci_global_objects['EPGs'].add(curr_epg_name)
                if curr_app_name not in tmp_app:
                    tmp_app.append(curr_app_name)
                    apps_to_return.append({'Name': curr_app_name, 'EPGs': [curr_epg_name]})
                else:
                    for a in apps_to_return:
                        if a['Name'] == curr_app_name:
                            a['EPGs'].append(curr_epg_name)
        return apps_to_return

    def subject_to_filter(self):
        for tenant in self.contract_to_sub:
            for contract in self.contract_to_sub[tenant]:
                for subject in self.contract_to_sub[tenant][contract]:
                    url_filt = f"{self.url}/api/node/mo/uni/tn-{tenant}/brc-{contract}/subj-{subject}.json?" \
                               f"query-target=children&target-subtree-class=vzRsSubjFiltAtt"
                    filt_data = self.session.invoke(url_filt, method='GET')['imdata']
                    for filt in filt_data:
                        filt_act = filt.get('vzRsSubjFiltAtt', {}).get('attributes', {}).get('action', False)
                        filt_name = filt.get('vzRsSubjFiltAtt', {}).get('attributes', {}).get('tnVzFilterName', False)
                        if filt_act and filt_name:
                            if filt_act == 'permit':
                                self.contract_to_sub[tenant][contract][subject]['permit_filt'].append(filt_name)
                            else:
                                self.contract_to_sub[tenant][contract][subject]['deny_filt'].append(filt_name)

    def filt_to_ent(self):
        url_entries = f"{self.url}/api/node/class/vzEntry.json"
        entries_data = self.session.invoke(url_entries, method='GET')['imdata']
        for entry in entries_data:
            dn = entry.get('vzEntry', {}).get('attributes', {}).get('dn', False)
            name = entry.get('vzEntry', {}).get('attributes', {}).get('name', False)
            if dn and name:
                split_dn = dn.split('/')
                tenant = split_dn[1][3:]
                if tenant in RESTSite.tenants_to_skip:
                    continue
                flt = split_dn[2][4:]
                tmp_entry = {
                    'name': name,
                    'dFromPort': entry.get('vzEntry', {}).get('attributes', {}).get('dFromPort', False),
                    'dToPort': entry.get('vzEntry', {}).get('attributes', {}).get('dToPort', False),
                    'sFromPort': entry.get('vzEntry', {}).get('attributes', {}).get('sFromPort', False),
                    'sToPort': entry.get('vzEntry', {}).get('attributes', {}).get('sToPort', False),
                    'etherT': entry.get('vzEntry', {}).get('attributes', {}).get('etherT', 'ip'),
                    'prot': entry.get('vzEntry', {}).get('attributes', {}).get('prot', 'unspecified')
                }
                if tenant not in self.filter_to_entry:
                    self.filter_to_entry[tenant] = {flt: [tmp_entry]}
                else:
                    if flt not in self.filter_to_entry[tenant]:
                        self.filter_to_entry[tenant][flt] = [tmp_entry]
                    else:
                        self.filter_to_entry[tenant][flt].append(tmp_entry)

    def contract_to_subject(self):
        for obj in self.objects_paths:
            for tenant in obj['Tenants']:
                curr_tenant = tenant['Name']
                if curr_tenant not in self.contract_to_sub:
                    self.contract_to_sub[curr_tenant] = {}
                url_cont = f"{self.url}/api/node/mo/uni/tn-{curr_tenant}.json?query-target=children" \
                           f"&target-subtree-class=vzBrCP&query-target=subtree&target-subtree-class=vzBrCP" \
                           f"&rsp-subtree-class=vzSubj,tagInst,vzRtIf&rsp-subtree=children"
                contract_data = self.session.invoke(url_cont, method='GET')['imdata']
                for contract in contract_data:
                    curr_cont = contract.get('vzBrCP', {}).get('attributes', {}).get('name', False)
                    children = contract.get('vzBrCP', {}).get('children', [])
                    if curr_cont:
                        self.aci_global_objects['Contracts'].add(curr_cont)
                        if curr_cont not in self.contract_to_sub[curr_tenant]:
                            self.contract_to_sub[curr_tenant][curr_cont] = {}
                        for child in children:
                            curr_subj = child.get('vzSubj', {}).get('attributes', {}).get('name', False)
                            if curr_subj and curr_subj not in self.contract_to_sub[curr_tenant][curr_cont]:
                                self.contract_to_sub[curr_tenant][curr_cont][curr_subj] = \
                                    {'permit_filt': [], 'deny_filt': []}

    def disp_contract_details(self, contract, data_return=False):
        epgs = []
        filters = []
        filt_to_disp = []
        for tenant in self.contract_to_epg:
            for epg in self.contract_to_epg[tenant]:
                if contract in self.contract_to_epg[tenant][epg]['prov']:
                    epgs.append((tenant, epg, 'Provides'))
                elif contract in self.contract_to_epg[tenant][epg]['cons']:
                    epgs.append((tenant, epg, 'Consumes'))
        if not epgs:
            print(f'{self.url} - no EPGs Consuming/Providing this contract.')
            return
        epgs = sorted(epgs, key=lambda xx: xx[2])
        if not data_return:
            ACI_sh_meth.formatted_print(epgs, ("Tenant", "EPG", "Relation"))
        for tenant in self.contract_to_sub:
            for contr in self.contract_to_sub[tenant]:
                if contr == contract:
                    tmp_permits = []
                    tmp_denies = []
                    for subj in self.contract_to_sub[tenant][contr]:
                        for x in self.contract_to_sub[tenant][contr][subj]['permit_filt']:
                            if x not in tmp_permits:
                                tmp_permits.append(x)
                        for x in self.contract_to_sub[tenant][contr][subj]['deny_filt']:
                            if x not in tmp_denies:
                                tmp_denies.append(x)
                    filters.append((tenant, contract, tmp_permits, tmp_denies))
        for entry in filters:
            if entry[2]:
                for filt in entry[2]:
                    for tenant in self.filter_to_entry:
                        if tenant == entry[0]:
                            for fil in self.filter_to_entry[tenant]:
                                if fil == filt:
                                    for ent in self.filter_to_entry[tenant][fil]:
                                        dfrom = ent.get("dFromPort", "unspec")
                                        dto = ent.get("dToPort", "unspec")
                                        sfrom = ent.get("sFromPort", "unspec")
                                        sto = ent.get("sToPort", "unspec")
                                        ethert = ent.get("etherT", "ip")
                                        prot = ent.get("prot", "unspec")
                                        if data_return:
                                            filt_to_disp.append((
                                                tenant, contract, filt, ethert, prot, str(sfrom + ' - ' + sto),
                                                str(dfrom + ' - ' + dto), 'Permit'))
                                            continue
                                        filt_to_disp.append((tenant, filt, ethert, prot, str(sfrom + ' - ' + sto),
                                                             str(dfrom + ' - ' + dto), 'Permit'))
            if entry[3]:
                for filt in entry[3]:
                    for tenant in self.filter_to_entry:
                        if tenant == entry[0]:
                            for fil in self.filter_to_entry[tenant]:
                                if fil == filt:
                                    for ent in self.filter_to_entry[tenant][fil]:
                                        dfrom = ent.get("dFromPort", "unspec")
                                        dto = ent.get("dToPort", "unspec")
                                        sfrom = ent.get("sFromPort", "unspec")
                                        sto = ent.get("sToPort", "unspec")
                                        ethert = ent.get("etherT", "ip")
                                        prot = ent.get("prot", "unspec")
                                        if data_return:
                                            filt_to_disp.append((
                                                tenant, contract, filt, ethert, prot, str(sfrom + ' - ' + sto),
                                                str(dfrom + ' - ' + dto), 'Deny'))
                                            continue
                                        filt_to_disp.append((tenant, filt, ethert, prot, str(sfrom + ' - ' + sto),
                                                             str(dfrom + ' - ' + dto), 'Deny'))
        if data_return:
            filt_to_disp = sorted(filt_to_disp, key=lambda xx: (xx[0], xx[-1], xx[4]))
            return filt_to_disp
        filt_to_disp = sorted(filt_to_disp, key=lambda xx: (xx[0], xx[-1], xx[3]))
        print()
        ACI_sh_meth.formatted_print(filt_to_disp, ("Tenant", "Filter", "EtherT", "Protocol", "Src port-range",
                                                   "Dst port-range", "Action"))

    def disp_epg_details(self, epg_to_check):
        epg_to_check_data = []
        epg_out_contracts = []
        epg_out_ports = []
        for obj in self.objects_paths:
            for tenant in obj['Tenants']:
                curr_tenant = tenant['Name']
                for app in tenant['Apps']:
                    curr_app = app['Name']
                    for epg in app['EPGs']:
                        if epg == epg_to_check:
                            epg_to_check_data.append((self.url, curr_tenant, curr_app))
        for rec in epg_to_check_data:
            url_epg = f'{self.url}/api/node/mo/uni/epp/fv-[uni/tn-{rec[1]}/ap-{rec[2]}/epg-{epg_to_check}]' \
                      f'.json?query-target=subtree&target-subtree-class=fvIfConn'
            epg_data = self.session.invoke(url_epg, method='GET')['imdata']
            if epg_data:
                for entry in epg_data:
                    encap = entry['fvIfConn']['attributes'].get('encap', 'unknown')
                    mode = entry['fvIfConn']['attributes'].get('mode', 'unknown')
                    if mode == 'regular':
                        mode = 'Trunk'
                    elif mode == 'untagged':
                        mode = 'Access (Untagged)'
                    elif mode == 'native':
                        mode = 'Access (802.1P)'
                    dn = entry['fvIfConn']['attributes'].get('dn', 'unknown').split('stpathatt-[')
                    port = 'unknown'
                    tmp_port = ''
                    if len(dn) > 1:
                        for x in dn[1]:
                            if x == ']':
                                break
                            tmp_port += x
                    if tmp_port:
                        port = tmp_port
                    tmp_tup = (self.url, rec[1], rec[2], port, encap, mode)
                    if tmp_tup not in epg_out_ports:
                        epg_out_ports.append(tmp_tup)
            epg_to_check_prov = self.contract_to_epg.get(rec[1], {}).get(epg_to_check, {}).get('prov', [])
            epg_to_check_cons = self.contract_to_epg.get(rec[1], {}).get(epg_to_check, {}).get('cons', [])
            epg_out_contracts.append((self.url, rec[1], rec[2], str(epg_to_check_prov), str(epg_to_check_cons)))
        print('Contracts:')
        if epg_out_contracts:
            ACI_sh_meth.formatted_print(epg_out_contracts, ("Site", "Tenant", "Application", "Provides", "Consumes"))
        else:
            print('No Contracts found.')
        print('\nInterfaces:')
        if epg_out_ports:
            ACI_sh_meth.formatted_print(epg_out_ports, ("Site", "Tenant", "Application", "Port", "Encapsulation",
                                                        "Mode"))
        else:
            print('No Interfaces found.')

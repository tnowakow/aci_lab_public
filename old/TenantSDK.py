import cobra.mit.access
import cobra.mit.request
import cobra.mit.session
import cobra.model.fv
import cobra.model.pol
import cobra.model.vz
import ErrorHandler
import argparse
import DataHandler
from SDKsession import SDKsession



def create_tenant(name):
    """ Creates Tenant object with name passed as argument.
    Checks if object has been created successfully.
    """

    obj = SDK_session.check_if_created('fvTenant', name, 'create_tenant',name, pre_check=True)
    if not obj:
        tenantMo = cobra.model.fv.Tenant(SDK_session.polUni, name)
        SDK_session.cobra_commit(tenantMo)
    SDK_session.check_if_created('fvTenant', name, 'create_tenant', name)


def create_vrf(params):
    """ Creates VRF object under 'fvTenant -> Networking -> VRFs'.
    Checks if all necessary parameters are present in passed 'params' argument (SDK_session.params_check).
    Checks if object has been created successfully.
    """

    SDK_session.params_check(params, [(0, 'name')])
    name = params[0]['name']
    pcEnfPref = params[0].get('pcEnfPref', 'enforced')
    pcEnfDir = params[0].get('pcEnfDir', 'ingress')
    ipDataPlaneLearning = params[0].get('ipDataPlaneLearning', 'enabled')
    fvCtx = cobra.model.fv.Ctx(fvTenant, name=name, pcEnfPref=pcEnfPref, pcEnfDir=pcEnfDir,
                               ipDataPlaneLearning=ipDataPlaneLearning)
    SDK_session.cobra_commit(fvTenant)
    SDK_session.check_if_created('fvCtx', name, 'create_vrf', params)


def create_bridge_domain(params):
    """ Creates Bridge-Domain object under 'fvTenant -> Networking -> Bridge Domains'.
    Checks if all necessary parameters are present in passed 'params' argument (SDK_session.params_check).
    Assigns VRF to Bridge-Domain if vrf is included in params.
    Validates and creates subnets objects under Bridge-Domain object if they are included in params.
    Checks if object has been created successfully.
    """

    name = params[0]
    arpFlood = params[1].get('arpFlood', 'true')
    vrf = params[1].get('vrf', '')
    fvBD = cobra.model.fv.BD(fvTenant, arpFlood=arpFlood, name=name)
    if vrf:
        if SDK_session.check_if_created('fvCtx', vrf, 'create_bridge_domin-VRF-pre-check', params, pre_check=True):
            fvRsCtx = cobra.model.fv.RsCtx(fvBD, tnFvCtxName=vrf)
    SDK_session.cobra_commit(fvTenant)
    SDK_session.check_if_created('fvBD', name, 'create_bridge_domain', params)
    subnets_list = params[1].get('subnets', [])
    for subnet in subnets_list:
        ip = subnet.get('ip', '')
        if not ip:
            raise ErrorHandler.MissingArgumentError(log=SDK_session.log, msg=f'IP missing in {subnet}')
        ctrl = subnet.get('ctrl', '')
        preferred = subnet.get('preferred', 'false')
        scope = subnet.get('scope', 'private')
        fvBD = cobra.model.fv.BD(fvTenant, name)
        if preferred:
            fvSubnet = cobra.model.fv.Subnet(fvBD, ctrl=ctrl, ip=ip, preferred=preferred, scope=scope)
        else:
            fvSubnet = cobra.model.fv.Subnet(fvBD, ctrl=ctrl, ip=ip, scope=scope)
        SDK_session.cobra_commit(fvBD)
        check_subnet = SDK_session.moDir.lookupByClass('fvSubnet', propFilter=f'eq(fvSubnet.ip, "{ip}")')
        check_subnet = check_subnet[0] if check_subnet else None
        if check_subnet:
            SDK_session.log.info(f"OK - 'create_bridge_domain' created subnet: {subnet}")
        else:
            SDK_session.log.error(f"ERROR - 'create_bridge_domain' failed to create subnet: {subnet}")
            SDK_session.object_not_created('create_bridge_domain', ip)


def create_filter(params):
    """ Creates Filter under 'fvTenant -> Contracts -> Filters'.
    Checks if all necessary parameters are present in passed 'params' argument (params_check).
    Assigns Entries to Filter if entries are included in params.
    Checks if object has been created successfully.
    """

    SDK_session.params_check(params, [(0, 'name')])
    name = params[0]['name']
    vzFilter = cobra.model.vz.Filter(fvTenant, name=name)
    SDK_session.cobra_commit(fvTenant)
    SDK_session.check_if_created('vzFilter', name, 'create_filter', params)
    for entry in params[0]['entries']:
        etherT = entry['etherT']
        entry_name = entry['name']
        prot = entry.get('prot', 'unspecified')
        dFromPort = entry.get('dFromPort', 'unspecified')
        dToPort = entry.get('dToPort', 'unspecified')
        sFromPort = entry.get('sFromPort', 'unspecified')
        sToPort = entry.get('sToPort', 'unspecified')
        vzEntry = cobra.model.vz.Entry(vzFilter, name=entry_name, etherT=etherT, prot=prot, sFromPort=sFromPort,
                                       sToPort=sToPort, dFromPort=dFromPort, dToPort=dToPort)
        SDK_session.cobra_commit(fvTenant)
        SDK_session.check_if_created('vzEntry', entry_name, f'create_filter - entry for filter: {name}', params)


def create_standard_contract(params):
    """ Creates Contract under 'fvTenant -> Contracts -> Standard'.
    Checks if all necessary parameters are present in passed 'params' argument (params_check).
    Creates Contract object with scope defined in params ('vrf' is default).
    Assigns Subjects to Contract if subjects are included in params - 'fvTenant -> Contracts -> Standard -> Subject'
    Assigns Filters to Subjects if filters are included in params.
    Checks if object has been created successfully.
    """

    SDK_session.params_check(params, [(0, 'name')])
    name = params[0]['name']
    scope = params[0].get('scope', 'vrf')
    if scope != 'vrf':
        vzBrCP = cobra.model.vz.BrCP(fvTenant, name=name, scope=scope)
    else:
        vzBrCP = cobra.model.vz.BrCP(fvTenant, name=name)
    SDK_session.cobra_commit(vzBrCP)
    contract_check = SDK_session.check_if_created('vzBrCP', name, 'create_standard_contract', params, pre_check=True)
    if not contract_check:
        SDK_session.log.error(f"ERROR - 'create_standard_contract' failed to create contract: {params}")
        SDK_session.object_not_created('create_standard_contract', params)
    for subject in params[0]['subjects']:
        subject_name = subject.get('name', '')
        revFltPorts = subject.get('revFltPorts', 'true')
        if subject_name:
            vzSubj = cobra.model.vz.Subj(vzBrCP, name=subject_name, revFltPorts=revFltPorts)
            SDK_session.cobra_commit(vzBrCP)
            subject_check = SDK_session.check_if_created('vzSubj', subject_name, 'create_standard_contract', params,
                                                         pre_check=True)
            if not subject_check:
                SDK_session.log.error(f"ERROR - 'create_standard_contract' failed to create subject: {subject_name}")
                SDK_session.object_not_created('create_standard_contract', params)
            else:
                SDK_session.log.info(f"OK - 'create_standard_contract' created object (subject): {subject_name}")
                filters_list = subject.get('filters', [])
                for filter in filters_list:
                    filter_name = filter.get('name', '')
                    action = filter.get('action', 'permit')
                    directives = filter.get('directives', '')
                    filter_check = SDK_session.check_if_created('vzFilter', filter_name, 'create_standard_contract',
                                                                params, pre_check=True)
                    if filter_check:
                        vzRsSubjFiltAtt = cobra.model.vz.RsSubjFiltAtt(vzSubj, tnVzFilterName=filter_name,
                                                                       action=action, directives=directives)
                        SDK_session.cobra_commit(vzBrCP)
                    else:
                        SDK_session.log.error(f"ERROR - 'create_standard_contract' failed to add entry: {filter}, "
                                              f"to subject {subject_name} - filter does not exist")
                        SDK_session.object_not_created('create_standard_contract', params)
                    SDK_session.check_if_created('vzRsSubjFiltAtt', filter_name, 'create_standard_contract - '
                                                                                 'filter attachment', filter)


def create_app_profile(params):
    """ Creates Application Profile under 'fvTenant -> Application Profiles'
    Checks if all necessary parameters are present in passed 'params' argument (params_check).
    Creates Application Profile object if it's not present in configuration.
    Assigns Bridge-Domain to Application Profile.
    Assigns EPG to Application Profile and static ports (VPC or standalone access ports) to EPG.
    Checks if object has been created successfully.
    """

    pod = 'pod-1'
    SDK_session.params_check(params, [(2, 'bridge-domain')])
    app_name = params[0]
    epg_name = params[1]
    bridge_domain = params[2]['bridge-domain']
    fabric_domain = params[2].get('fabric-domain', '')
    static_ports = params[2].get('static-ports', [])
    resImedcy = params[2].get('resImedcy', 'immediate')
    app_exists = SDK_session.check_if_created('fvAp', app_name, 'create_app_profile - app', app_name, pre_check=True)
    fvAp = cobra.model.fv.Ap(fvTenant, name=app_name)
    if not app_exists:
        SDK_session.cobra_commit(fvTenant)
    SDK_session.check_if_created('fvAp', app_name, 'create_app_profile - app', app_name)
    fvAEPg = cobra.model.fv.AEPg(fvAp, name=epg_name)
    fvRsBd = cobra.model.fv.RsBd(fvAEPg, tnFvBDName=bridge_domain)
    SDK_session.cobra_commit(fvTenant)
    SDK_session.check_if_created('fvAEPg', epg_name, 'create_app_profile - EPG', epg_name)
    fvRsDomAtt = cobra.model.fv.RsDomAtt(fvAEPg, resImedcy=resImedcy, tDn=f'uni/phys-{fabric_domain}')
    for port in static_ports:
        mode = port.get('mode', False)
        encap = port.get('encap', False)
        path = port.get('path', False)
        instrImedcy = port.get('instrImedcy', 'on-demand')
        if not mode or not encap or not path:
            raise ErrorHandler.MissingArgumentError(log=SDK_session.log, msg=f'mode or encap or path missing in {port}')
        encap = 'vlan-' + encap
        if 'vpc' in path.lower():
            vpc_id_1 = path.split('-')[1]
            vpc_id_2 = path.split('-')[2]
            if mode == 'trunk':
                if instrImedcy == 'on-demand':
                    fvRsPathAtt = cobra.model.fv.RsPathAtt(fvAEPg, encap=encap,
                                                           tDn=f'topology/{pod}/protpaths-{vpc_id_1}-{vpc_id_2}/'
                                                               f'pathep-[{path}]')
                else:
                    fvRsPathAtt = cobra.model.fv.RsPathAtt(fvAEPg, encap=encap, instrImedcy=instrImedcy,
                                                           tDn=f'topology/{pod}/protpaths-{vpc_id_1}-{vpc_id_2}/'
                                                               f'pathep-[{path}]')
            else:
                if instrImedcy == 'on-demand':
                    fvRsPathAtt = cobra.model.fv.RsPathAtt(fvAEPg, encap=encap, mode=mode,
                                                           tDn=f'topology/{pod}/protpaths-{vpc_id_1}-{vpc_id_2}/'
                                                               f'pathep-[{path}]')
                else:
                    fvRsPathAtt = cobra.model.fv.RsPathAtt(fvAEPg, encap=encap, mode=mode, instrImedcy=instrImedcy,
                                                           tDn=f'topology/{pod}/protpaths-{vpc_id_1}-{vpc_id_2}/'
                                                               f'pathep-[{path}]')
        else:
            leaf_id = path.split('-')[1]
            port_id = path.split('-')[-1]
            if mode == 'trunk':
                if instrImedcy == 'on-demand':
                    fvRsPathAtt = cobra.model.fv.RsPathAtt(fvAEPg, encap=encap,
                                                           tDn=f'topology/{pod}/paths-{leaf_id}/pathep-[{port_id}]')
                else:
                    fvRsPathAtt = cobra.model.fv.RsPathAtt(fvAEPg, encap=encap, instrImedcy=instrImedcy,
                                                           tDn=f'topology/{pod}/paths-{leaf_id}/pathep-[{port_id}]')
            else:
                if instrImedcy == 'on-demand':
                    fvRsPathAtt = cobra.model.fv.RsPathAtt(fvAEPg, encap=encap, mode=mode,
                                                           tDn=f'topology/{pod}/paths-{leaf_id}/pathep-[{port_id}]')
                else:
                    fvRsPathAtt = cobra.model.fv.RsPathAtt(fvAEPg, encap=encap, mode=mode, instrImedcy=instrImedcy,
                                                           tDn=f'topology/{pod}/paths-{leaf_id}/pathep-[{port_id}]')
        SDK_session.cobra_commit(fvTenant)


def create_EPG_to_contract_relation(params):
    """ Assigns contracts to EPGs. EPG can either consume or provide contract.
    Params - includes dictionary with name of EPG and two lists with names of contracts - 'provides' and 'consumes'.
    Raises error if EPG passed in params doesn't exist in configuration.
    Checks if object has been created successfully.
    """

    SDK_session.params_check(params, [(0, 'epg')])
    epg = params[0]['epg']
    consumed_contracts = params[0].get('consumes', [])
    provided_contracts = params[0].get('provides', [])
    epg_obj = SDK_session.check_if_created('fvAEPg', epg, 'create_app_profile - EPG', epg, pre_check=True)
    if not epg_obj:
        raise ErrorHandler.MissingParentError(log=SDK_session.log, msg=epg)
    parent = str(epg_obj._BaseMo__parentDnStr) + f'/epg-{epg}/'
    for contract in consumed_contracts:
        fvRsCons = cobra.model.fv.RsCons(parent, tnVzBrCPName=contract)
        SDK_session.cobra_commit(fvRsCons)
    for contract in provided_contracts:
        fvRsProv = cobra.model.fv.RsProv(parent, tnVzBrCPName=contract)
        SDK_session.cobra_commit(fvRsProv)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", type=str, help="Name of file including input data")
    parser.add_argument("-u", "--url", type=str, help="APIC URL")
    args = parser.parse_args()
    if args.url:
        URL = args.url
    else:
        URL = 'https://192.168.10.2'
    if args.file:
        input_data_file = args.file
    else:
        input_data_file = '../InputFiles/TestTenantInputFile'
    SDK_session = SDKsession('admin', 'ciscocisco', URL)
    data = DataHandler.DataHandler(input_data_file, log=SDK_session.log)
    data.data_reader()
    create_tenant(data.paths[0][0])

    # sets fvTenant under which all the lower level objects will be created
    fvTenant = cobra.model.fv.Tenant(SDK_session.polUni, data.paths[0][0])

    # checks last item in a path which is a pointer to proper function
    # removes last item and path to the object itself so all passed parameters are object related
    # invokes function with 'path' list or dictionary as parameter
    for path in data.paths_sorted_by_priority:
        if path[-1] == 'create_vrf':
            del path[-1]; del path[:3]
            create_vrf(path)
        elif path[-1] == 'create_bridge_domain':
            del path[-1]; del path[:3]
            create_bridge_domain(path)
        elif path[-1] == 'create_filter':
            del path[-1]; del path[:3]
            create_filter(path)
        elif path[-1] == 'create_standard_contract':
            del path[-1]; del path[:3]
            create_standard_contract(path)
        elif path[-1] == 'create_app_profile':
            del path[-1]; del path[:2]
            create_app_profile(path)
        elif path[-1] == 'create_EPG_to_contract_relation':
            del path[-1]; del path[:3]
            create_EPG_to_contract_relation(path)
        else:
            raise ErrorHandler.MissingMethodError(log=SDK_session.log, msg=path)
    SDK_session.logout()
    exit(0)
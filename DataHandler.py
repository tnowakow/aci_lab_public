import json
import ErrorHandler
import ipaddress
import re


class DataHandler:
    """ Class that includes input data related methods."""

    def __init__(self, input_file, log=None):
        """ Constructor. Takes as parameters:
        input_file - path to a file with input data
        log - instance of a logger class
        paths_for_name_checker - includes all the objects listed in input_file, for naming check purposes only
        paths_sorted_by_priority - includes paths from leaf objects (from input_file) to the root object.
                        Objects are sorted to make the number of operations as small as possible from ACI perspective.
        MSO_blobal_vars - includes global variables for MSO - Site, Schema, POD, Tenant
        If logger instance is not present logs will not be produced.
        """

        self.input_file = input_file
        self.paths = []
        self.paths_for_name_checker = []
        self.paths_sorted_by_priority = []
        self.MSO_global_vars = {}
        if log:
            self.log = log
        else:
            self.log = None

    def name_checker(self, list_to_check):
        """ Checks if names of objects listed in the input_file follow naming convention.
        Takes list of objects as a parameter.
        - name can't be longer than 64 characters
        - subnets have to include '/subnet mask' (10.0.0.1/24), IP itself is also being validated
        - MAC addresses have to be in format - 0000.0000.0000 (0-9, a-f)
        Raises proper error whenever name doesn't meet the criteria.
        """

        for path in list_to_check:
            for name in path:
                if isinstance(name, bool):
                    continue
                if len(name) > 64:
                    raise ErrorHandler.NameTooLongError(log=self.log, msg=(str(name), str(len(name))))
                if str(name.lower()).count('.') == 3 or str(name.lower()).count(':') >= 2:
                    if '/' not in name:
                        raise ErrorHandler.WrongIPError(log=self.log, msg=f'{name} - missing subnet')
                    try:
                        ipaddress.ip_network(str(name.split('-')[-1]), strict=False)
                    except ValueError as e:
                        raise ErrorHandler.WrongIPError(log=self.log, msg=(str(name), e))
                elif 'cep' in name.lower():
                    tmp_mac = str(name.split('-')[-1])
                    if not re.match("[0-9a-f]{4}(.)[0-9a-f]{4}(.)[0-9a-f]{4}$", tmp_mac.lower()):
                        raise ErrorHandler.WrongMACError(log=self.log, msg=name)
                else:
                    parsed_name = name.replace('-', 'a').replace('_', 'a').replace(' ', 'a').replace(',', 'a')
                    if 'eth' in name.lower():
                        parsed_name = parsed_name.replace('/', 'a')
                    if parsed_name and not parsed_name.isalnum():
                        raise ErrorHandler.WrongNameError(log=self.log, msg=name)

    def data_reader(self):
        """ Reads data from the input_file. Depending on data format (only list and dictionary are allowed)
        invokes proper method for further data processing. Those methods create chains from leaf objects to the root.
        There is a separate method and list for name_checker as chains of dependencies are longer in this case -
        (separate chain for every parameter, in case of 'paths' list there is only one chain to a leaf object -
        all the parameters are treated as a part of the object).
        Once 'paths_for_name_checker' and 'paths' lists are populated 'name_checker' and 'sort_by_priority' methods
        are being invoked with proper input.
        """

        try:
            with open(self.input_file, 'r') as file:
                data = json.load(file)
        except Exception as e:
            raise ErrorHandler.InputFileError(log=self.log, msg=e)
        if isinstance(data, dict):
            self.NestedDictValues(data, [], False)
            self.NestedDictValuesFull(data, [], False)
        elif isinstance(data, list):
            self.ListValues(data, [])
            self.NestedListValuesFull(data, [])
        else:
            raise ErrorHandler.InputDataError(log=self.log)
        self.name_checker(self.paths_for_name_checker)

    def ListValues(self, current_list, path):
        """ Creates paths for objects included in a list.
        Takes list encountered by 'NestedDictValues' method and list including all the paths to objects as parameters.
        List is considered to be a leaf object (not for a name checker) in a chain.
        It is the last step of recursion for chain creating process. This method may be invoked by 'NestedDictValues'
        only when list is encountered.
        Populates 'paths' list with created paths.
        """

        if current_list == []:
            path.append([])
            self.paths.append(path[:])
            del path[-2:]
        else:
            for v in current_list:
                path.append(v)
                self.paths.append(path[:])
                del path[-1:]
            del path[-1:]

    def NestedDictValues(self, current_dict, path, inside_list):
        """ Creates paths from leaf objects to the root.
        Takes dictionary to be checked, list including paths to objects and bool object (describing whether
        current_dict is inside a list - this info is needed to create a valid path) as parameters.
        Recursive method. Invokes itself or 'ListValues' depending on the type of encountered object.
        Populates 'paths' list with created paths.
        """

        for k, v in current_dict.items():
            if isinstance(v, dict):
                path.append(k)
                self.NestedDictValues(v, path, False)
            elif isinstance(v, list) or isinstance(v, tuple):
                path.append(k)
                self.ListValues(v, path)
            else:
                path.append(k)
                path.append(v)
                self.paths.append(path[:])
                del path[-2:]
        if not inside_list:
            del path[-1:]

    def NestedListValuesFull(self, current_list, path):
        """ Creates paths from leaf objects to the root - for naming check purposes only. Creates a path per parameter
        - not per object like 'NestedDictValues' method.
        Takes list to be checked and list including paths to objects as parameters.
        Recursive method. Invokes itself or 'NestedDictValuesFull' depending on the type of encountered object.
        Populates 'paths_for_name_checker' list with created paths.
        """

        if current_list == []:
            path.append([])
            self.paths_for_name_checker.append(path[:])
            del path[-2:]
        else:
            for v in current_list:
                if isinstance(v, list) or isinstance(v, tuple):
                    self.NestedListValuesFull(v, path)
                elif isinstance(v, dict):
                    self.NestedDictValuesFull(v, path, True)
                else:
                    path.append(v)
                    self.paths_for_name_checker.append(path[:])
                    del path[-1:]
            del path[-1:]

    def NestedDictValuesFull(self, current_dict, path, inside_list):
        """ Creates paths from leaf objects to the root - for naming check purposes only. Creates a path per parameter
        - not per object like 'NestedDictValues' method.
        Takes dictionary to be checked and list including paths to objects as parameters.
        Recursive method. Invokes itself or 'NestedListValuesFull' depending on the type of encountered object.
        Populates 'paths_for_name_checker' list with created paths.
        """

        for k, v in current_dict.items():
            if isinstance(v, dict):
                path.append(k)
                self.NestedDictValuesFull(v, path, False)
            elif isinstance(v, list) or isinstance(v, tuple):
                path.append(k)
                self.NestedListValuesFull(v, path)
            else:
                path.append(k)
                path.append(v)
                self.paths_for_name_checker.append(path[:])
                del path[-2:]
        if not inside_list:
            del path[-1:]

    def params_check(self, params_list, names_tuples):
        """ Checks if all necessary parameters are included in dictionary passed to a method as parameter.
        Only parameters crucial for object creation are being checked.
        Takes params (list or dictionary) to be checked and list of tuples (index, name) as arguments.
        """

        for tup in names_tuples:
            tmp_var = params_list[tup[0]].get(tup[1], False)
            if not tmp_var:
                raise ErrorHandler.MissingArgumentError(log=self.log, msg=f'{tup[1]} missing in {params_list[tup[0]]}')


class DataHandlerInfra(DataHandler):

    def __init__(self, input_file, log):
        super().__init__(input_file, log)
        self.data_reader()
        self.sort_by_priority(self.paths)

    def sort_by_priority(self, paths_to_sort):
        """ Sorts chains of dependencies in a way that minimizes number of operation from ACI perspective.
        Takes list of paths as a parameter.
        Artificial variable ('1a', '3b', etc) is being added for sorting purposes.
        Those variables are removed from the paths after sorting.
        Appends name of a function that should handle particular path to the 'path' list.
        """

        for paths in paths_to_sort:
            if paths[0] == 'Infra':
                if paths[1] == 'Policies' and paths[2] == 'Switch':
                    paths.append('create_switch_policy')
                    paths.append('1a')
                elif paths[1] == 'Policies' and paths[2] == 'Interface':
                    paths.append('create_interface_policy')
                    paths.append('1a')
                elif paths[1] == 'Pools':
                    paths.append('create_pool')
                    paths.append('1a')
                elif paths[1] == 'Physical and External Domains':
                    paths.append('create_domain')
                    paths.append('1b')
                elif paths[1] == 'Policies' and paths[2] == 'Global':
                    paths.append('create_global_policy')
                    paths.append('1c')
                elif paths[1] == 'Interfaces' and paths[2] == 'Leaf Interfaces' and paths[3] == 'Policy Groups':
                    paths.append('create_leaf_interface_policy_group')
                    paths.append('2a')
                elif paths[1] == 'Interfaces' and paths[2] == 'Leaf Interfaces' and paths[3] == 'Profiles':
                    paths.append('create_leaf_interface_profile')
                    paths.append('2b')
                elif paths[1] == 'Switches' and paths[2] == 'Leaf Switches' and paths[3] == 'Policy Groups':
                    paths.append('create_leaf_switch_policy_group')
                    paths.append('3a')
                elif paths[1] == 'Switches' and paths[2] == 'Leaf Switches' and paths[3] == 'Profiles':
                    paths.append('create_leaf_switch_profile')
                    paths.append('3b')
                else:
                    paths.append('unknown')
                    paths.append('zz')
            self.paths_sorted_by_priority.append(paths)
        self.paths_sorted_by_priority = sorted(self.paths_sorted_by_priority, key=lambda x: (x[-1], x[0]))
        for x in self.paths_sorted_by_priority:
            del x[-1]


class DataHandlerTenant(DataHandler):

    def __init__(self, input_file, log):
        super().__init__(input_file, log)
        self.data_reader()
        self.sort_by_priority(self.paths)

    def sort_by_priority(self, paths_to_sort):
        """ Sorts chains of dependencies in a way that minimizes number of operation from ACI perspective.
        Takes list of paths as a parameter.
        Artificial variable ('1a', '3b', etc) is being added for sorting purposes.
        Those variables are removed from the paths after sorting.
        Appends name of a function that should handle particular path to the 'path' list.
        """

        for paths in paths_to_sort:
            if 'tenant' in paths[0].lower():
                if paths[1] == 'Networking' and paths[2] == 'VRFs':
                    paths.append('create_vrf')
                    paths.append('11a')
                elif paths[1] == 'Networking' and paths[2] == 'Bridge Domains':
                    paths.append('create_bridge_domain')
                    paths.append('11b')
                elif paths[1] == 'Application Profiles':
                    paths.append('create_app_profile')
                    paths.append('13a')
                elif paths[1] == 'Contracts' and paths[2] == 'Filters':
                    paths.append('create_filter')
                    paths.append('12a')
                elif paths[1] == 'Contracts' and paths[2] == 'Standard':
                    paths.append('create_standard_contract')
                    paths.append('12b')
                elif paths[1] == 'Contracts' and paths[2] == 'Relations':
                    paths.append('create_EPG_to_contract_relation')
                    paths.append('14a')
                else:
                    paths.append('unknown')
                    paths.append('zzz')
            self.paths_sorted_by_priority.append(paths)
        self.paths_sorted_by_priority = sorted(self.paths_sorted_by_priority, key=lambda x: (x[-1], x[0]))
        for x in self.paths_sorted_by_priority:
            del x[-1]


class DataHandlerTenantMSO(DataHandler):

    def __init__(self, input_file, log):
        super().__init__(input_file, log)
        self.data_reader()
        self.sort_by_priority(self.paths)

    def sort_by_priority(self, paths_to_sort):
        """ Sorts chains of dependencies in a way that minimizes number of operation from ACI perspective.
        Takes list of paths as a parameter.
        Artificial variable ('1a', '3b', etc) is being added for sorting purposes.
        Those variables are removed from the paths after sorting.
        Appends name of a function that should handle particular path to the 'path' list.
        """

        for paths in paths_to_sort:
            if 'tenant' in paths[0].lower():
                if paths[1] == 'Networking' and paths[2] == 'VRFs':
                    paths.append('create_vrf')
                    paths.append('11a')
                elif paths[1] == 'Networking' and paths[2] == 'Bridge Domains':
                    paths.append('create_bridge_domain')
                    paths.append('11b')
                elif paths[1] == 'Application Profiles':
                    paths.append('create_app_profile')
                    paths.append('13a')
                elif paths[1] == 'Contracts' and paths[2] == 'Filters':
                    paths.append('create_filter')
                    paths.append('12a')
                elif paths[1] == 'Contracts' and paths[2] == 'Standard':
                    paths.append('create_standard_contract')
                    paths.append('12b')
                elif paths[1] == 'Contracts' and paths[2] == 'Relations':
                    paths.append('create_EPG_to_contract_relation')
                    paths.append('14a')
                else:
                    paths.append('unknown')
                    paths.append('zzz')
            elif 'global-variables' in paths[0].lower():
                del paths[0]
                self.MSO_global_vars[paths[0]] = paths[1]
                continue
            self.paths_sorted_by_priority.append(paths)
        self.paths_sorted_by_priority = sorted(self.paths_sorted_by_priority, key=lambda x: (x[-1], x[0]))
        for x in self.paths_sorted_by_priority:
            del x[-1]

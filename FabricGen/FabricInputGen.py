import re


def check_values(input_data):
    """ Checks input provided by user. All the values have to be integers, number of vpcs has to be from
    range 0-45 and number of switches per range has to be even as they will be paired into vpc domains.
    Ranges can't overlap.
    Returns False if one of the values doesn't meet the conditions
    or sorted list of tuples if all the values are correct.
    """

    input_list = input_data.split(';')
    sections = []
    for entry in input_list:
        tmp_entry_list = re.split(',|-', entry)
        try:
            beg = int(tmp_entry_list[0])
            end = int(tmp_entry_list[1])
            vpc = int(tmp_entry_list[2])
        except Exception as e:
            print(e)
            return False
        else:
            if end < beg or vpc > 44 or vpc < 0 or (end-beg) % 2 != 1:
                return False
            sections.append((beg, end, vpc))
    sections = sorted(sections, key=lambda x: (x[0], x[1]))
    for sec in range(1, len(sections)):
        if sections[sec][0] <= sections[sec-1][1]:
            return False
    return sections


def get_values():
    """ Takes (from user provided input) number of leaf switches, vpc port-channels and AAEP domains
    to be configured, then checks if the values are correct and returns them.
    'While' loops will run till proper input is provided.
    Input format: 1-2, 20; 13-14, 10; 5-8, 15 = from_leaf - to_leaf, number_of_vpcs for particular set of leaves.
    Returns list of tuples in format [(from_leaf - to_leaf, number_of_vpcs), (from_leaf - to_leaf, number_of_vpcs)]
    and aaeps (int).
    """

    sections = None
    aaeps = None
    while True:
        try:
            print("Provide input in format 'from_leaf - to_leaf, number_of_vpcs; from_leaf - to_leaf, number_of_vpcs'")
            leaves_number = str(input(": "))
            sections = check_values(leaves_number)
            if not sections:
                print("wrong values")
                continue
        except Exception as e:
            print(e)
            continue
        else:
            break
    while True:
        try:
            aaeps = int(input("Number of AAEPs: "))
            if aaeps < 1 or aaeps > 100:
                print("Number of AAEPs should be an integer value (1 to 100)")
                continue
        except Exception as e:
            print(e)
            continue
        else:
            break
    return sections, aaeps


def generate_vpcs():
    """ Generates VPC Domains (first for loop) and Interface Policy Groups for VPC port-channels (second for loop).
    Number of VPC domains is based on from_leaf - to_leaf values,
    VPC port-channels on number_of_vpcs - (1st-2nd,3rd) values from a tuple.
    First 'for' loop iterates over pairs of leaves, second - nested 'for' loop,
    iterates over interfaces to be bundled for this particular pair of switches.
    At the moment AAEP is fixed - should be changed in the future.
    Updates proper value in fabric_objects dictionary.
    """

    vpc_list_str = ''
    vpc_pol_str = ''
    for sec in range(0, len(sections)):
        beg, end, vpc_pairs = sections[sec][0], sections[sec][1], sections[sec][2]
        for leaf in range(beg, end, +2):
            leaf += 100
            tmp_str = str({"name": f"vpc_{leaf}_{leaf+1}", "vpc_id": f"{leaf}", "sw_1_id": f"{leaf}",
                           "sw_2_id": f"{leaf+1}"})
            if sec == len(sections)-1 and leaf == end+99:
                pass
            else:
                tmp_str += ',' + '\n'
            vpc_list_str += tmp_str
            for vpc in range(vpc_pairs):
                tmp_str_2 = str({"name": f"vpc_{leaf}_{leaf+1}_PolGrp_{vpc+1}", "aaep": "LAB_AAEP_1",
                                 "port-channel policy": "LACP_Active_Port_Channel_Policy_1",
                                 "policies": ["CDP_ON_IntPol", "LLDP_ON_IntPol"]})
                if vpc == vpc_pairs - 1 and leaf == end + 99 and sec == len(sections) - 1:
                    pass
                else:
                    tmp_str_2 += ',' + '\n'
                vpc_pol_str += tmp_str_2
    fabric_objects["VPC Interface PolGrp"] = vpc_pol_str
    fabric_objects["Virtual Port Channel default"] = vpc_list_str


def generate_int_policies():
    """ Generates configuration for Leaf Interface Policy Groups
    Assumption is - one Policy Group will be needed per AAEP. Those Policy Groups are for standalone ports only.
    Updates proper value in fabric_objects dictionary.
    """

    tmp_str = ''
    for aaep in range(aaeps):
        tmp_str += str({"name": f"AAEP_{aaep + 1}_Access_Ports_PolGrp_1", "aaep": f"LAB_AAEP_{aaep + 1}",
                        "policies": ["CDP_ON_IntPol", "LLDP_ON_IntPol"]})
        if aaep != aaeps - 1:
            tmp_str += ',\n'
    fabric_objects["Leaf Access Port PolGrp"] = tmp_str


def generate_int_profiles():
    """ Generates configuration for Leaf Interfaces Profiles
    One Leaf Int Profile is being generated per VPC domain and another one per Leaf switch - for standalone ports.
    VPC ports 1 to number_of_vpcs value are configured under shared profiles with proper VPC Policy Group.
    Ports numbered from number_of_vpcs to 44 are configured under additional per switch profile (for standalone ports).
    Assumption is ports 45-48 will be used as uplinks so they are not included.
    Updates proper value in fabric_objects dictionary.
    """

    pairs = ''
    for sec in range(0, len(sections)):
        beg, end, vpc_pairs = sections[sec][0], sections[sec][1], sections[sec][2]
        for leaf in range(beg, end, + 2):
            leaf += 100
            tmp_test = f'"Leaf_{leaf}_{leaf+1}_IntProf_1":[\n'
            for port in range(1, vpc_pairs + 1):
                tmp_str = str({"name": f"Eth1_{port}_PortSel", "fromPort": f"{port}", "toPort": f"{port}",
                               "block": f"block{port}", "policy_group": f"vpc_{leaf}_{leaf+1}_PolGrp_{port}"})
                if port != vpc_pairs:
                    tmp_str += ',\n'
                else:
                    tmp_str += '],\n'
                tmp_test += tmp_str
            pairs += tmp_test
        for leaf in range(beg, end + 1):
            leaf += 100
            tmp_test = f'"Leaf_{leaf}_IntProf_1":[\n'
            for port in range(vpc_pairs+1, 45):
                tmp_str = str({"name": f"Eth1_{port}_PortSel", "fromPort": f"{port}", "toPort": f"{port}",
                               "block": f"block{port}", "policy_group": "AAEP_1_Access_Ports_PolGrp_1"})
                if port != 44:
                    tmp_str += ',\n'
                elif port == 44 and sec == len(sections) - 1 and leaf == end + 100:
                    tmp_str += ']'
                else:
                    tmp_str += '],\n'
                tmp_test += tmp_str
            pairs += tmp_test
    fabric_objects['Interface Profiles'] = pairs


def generate_leaf_sw_profiles():
    """ Generates config for Leaf Switch Profiles.
    One switch profile per VPC domain is generated with selector covering both switches from the pair and
    proper Leaf Interface Profile is being assigned.
    On top of that one switch profile is generated per leaf switch (for standalone ports).
    Updates proper value in fabric_objects dictionary.
    """

    profiles = ''
    for sec in range(0, len(sections)):
        beg, end, vpc_pairs = sections[sec][0], sections[sec][1], sections[sec][2]
        for leaf in range(beg, end, +2):
            leaf += 100
            tmp_str = str(f'"Leaf_{leaf}_{leaf+1}_Profile": [\n')
            tmp_str += str({"name": f"Leaf_{leaf}_{leaf+1}_SwSel", "type": "range", "fromLeaf": f"{leaf}",
                            "toLeaf": f"{leaf+1}", "selector": f"{leaf}_{leaf+1}_sel",
                            "policy_group": "Access_SW_PolGrp_Common",
                            "int_profiles": [f"Leaf_{leaf}_{leaf+1}_IntProf_1"]})
            profiles += tmp_str + '],' + '\n'
        for leaf in range(beg, end+1):
            leaf += 100
            tmp_str = str(f'"Leaf_{leaf}_Profile": [\n')
            tmp_str += str({"name": f"Leaf_{leaf}_Sel", "type": "range", "fromLeaf": f"{leaf}", "toLeaf": f"{leaf}",
                            "selector": f"{leaf}_sel", "policy_group": "Access_SW_PolGrp_Common",
                            "int_profiles": [f"Leaf_{leaf}_IntProf_1"]})
            if leaf == end + 100 and sec == len(sections) - 1:
                profiles += tmp_str + ']'
            else:
                profiles += tmp_str + '],' + '\n'
    fabric_objects['Leaf Switch Profiles'] = profiles


if __name__ == '__main__':
    sections, aaeps = get_values()
    fabric_objects = {}
    generate_vpcs()
    generate_int_policies()
    generate_int_profiles()
    generate_leaf_sw_profiles()
    out_data = []
    out_file = 'FabricAutoGenOut'
    input_file = 'TEMPLATEfabricInputFile'
    with open(input_file, 'r') as read_file:
        out_str = str(read_file.read())

    # Variables with << x >> are included in the template. Script is replacing those values with configuration
    # generated by proper functions (from fabric_objects dictionary).

    out_str = out_str.replace('<<Virtual Port Channel default>>', fabric_objects['Virtual Port Channel default'])
    out_str = out_str.replace('<<Leaf Access Port PolGrp>>', fabric_objects['Leaf Access Port PolGrp'])
    out_str = out_str.replace('<<VPC Interface PolGrp>>', fabric_objects['VPC Interface PolGrp'])
    out_str = out_str.replace('<<Interface Profiles>>', fabric_objects['Interface Profiles'])
    out_str = out_str.replace('<<Leaf Switch Profiles>>', fabric_objects['Leaf Switch Profiles'])
    out_str = out_str.replace("'", '"')
    with open(out_file, 'w') as write_file:
        write_file.write(out_str)

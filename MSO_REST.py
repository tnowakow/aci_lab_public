import argparse
import ErrorHandler
import DataHandler
import MSOObjectsCLS
from RESTapi import RESTmso


def create_vrf(params):
    data.params_check(params, [(3, 'name')])
    MSO.create_vrf(params)


def create_bridge_domain(params):
    data.params_check(params, [(4, 'arpFlood')])
    MSO.create_BD(params)


def create_filter(params):
    data.params_check(params, [(3, 'name')])
    MSO.create_filter(params)


def create_standard_contract(params):
    data.params_check(params, [(3, 'name')])
    MSO.create_contract(params)


def create_app_profile(params):
    data.params_check(params, [(4, 'bridge-domain')])
    MSO.create_anps(params)


def create_epg_to_contract_relation(params):
    data.params_check(params, [(3, 'epg')])
    MSO.assign_contract(params)


def get_global_vars():
    MSO_objects_dict['schemas'] = MSO.get_schemas()['schemas']
    MSO_objects_dict['tenants'] = MSO.get_tenants()['tenants']
    MSO_objects_dict['sites'] = MSO.get_sites()['sites']
    schema_found = False
    tenant_found = False
    template_found = False
    tenant_assignment = False
    for tenant in MSO_objects_dict['tenants']:
        if data.MSO_global_vars['Tenant-displayName'] == tenant['displayName']:
            data.MSO_global_vars['Tenant-id'] = tenant['id']
            data.MSO_global_vars['Tenant-name'] = tenant['name']
            REST_session.log.info(f'Found Tenant object - {tenant["name"]}')
            tenant_found = True
    if not tenant_found:
        raise ErrorHandler.MissingOrWrongTenant(log=REST_session.log, msg=data.MSO_global_vars['Tenant-name'])
    for schema in MSO_objects_dict['schemas']:
        if data.MSO_global_vars['Schema-displayName'] == schema['displayName']:
            data.MSO_global_vars['Schema-id'] = schema['id']
            REST_session.log.info(f'Found Schema - {schema["displayName"]}')
            schema_found = True
            for template in schema['templates']:
                if data.MSO_global_vars['Template-displayName'] == template['displayName']:
                    data.MSO_global_vars['Template-name'] = template['name']
                    REST_session.log.info(f'Found Template - {template["name"]}')
                    template_found = True
                    MSO_objects_dict['current_template'] = template
                    if data.MSO_global_vars['Tenant-id'] != template['tenantId']:
                        raise ErrorHandler.MissingOrWrongTenant(log=REST_session.log,
                                                                msg=data.MSO_global_vars['Tenant-name'])
                    else:
                        tenant_assignment = True
                        break
        if schema_found:
            break
    if not schema_found or not template_found or not tenant_assignment:
        raise ErrorHandler.WrongGlobalVARS(log=REST_session.log, msg=('schema_found', schema_found,
                                                                      'template_found', template_found,
                                                                      'tenant_assignment', tenant_assignment))
    MSO.set_base_vars(data.MSO_global_vars)


def display_dry_log():
    print('Dry-run LOG:')
    for log in MSO.log:
        print(log)
    print('-'*100)
    if MSO.errors:
        print('!!! Below errors occurred during dry-run: !!!')
        for error in MSO.errors:
            print(error)
        print('-' * 100)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", type=str, help="Name of file including input data")
    parser.add_argument("-u", "--url", type=str, help="APIC URL")
    args = parser.parse_args()
    if args.url:
        url = args.url
    else:
        url = 'https://192.168.10.3'
    if args.file:
        input_data_file = args.file
    else:
        input_data_file = 'InputFiles\MSOTestInput-Changes'
    REST_session = RESTmso('admin', 'ciscocisco123!', url)
    MSO = MSOObjectsCLS.MSOobject(url, REST_session, dry=True)
    data = DataHandler.DataHandlerTenantMSO(input_data_file, log=REST_session.log)
    MSO_objects_dict = {'userId': REST_session.userId}
    get_global_vars()
    for path in data.paths_sorted_by_priority:
        if path[-1] == 'create_vrf':
            create_vrf(path[:-1])
        elif path[-1] == 'create_bridge_domain':
            create_bridge_domain(path[:-1])
        elif path[-1] == 'create_filter':
            create_filter(path[:-1])
        elif path[-1] == 'create_standard_contract':
            create_standard_contract(path[:-1])
        elif path[-1] == 'create_app_profile':
            create_app_profile(path[:-1])
        elif path[-1] == 'create_EPG_to_contract_relation':
            create_epg_to_contract_relation(path[:-1])
        else:
            raise ErrorHandler.MissingMethodError(log=REST_session.log, msg=path)
    REST_session.log.info(' !!! --- SESSION - REST - END --- !!! ')
    if MSO.dry:
        display_dry_log()

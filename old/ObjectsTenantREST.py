import json
import ErrorHandler


class ACIobject:
    """ Class that includes general session related variables - url to APIC, Tenant and Pod under which all lower level
    objects should be configured.
    Those parameters are available for other ACI object related classes.
    """

    base_API_url = ''
    objects_dict = {}
    url = ''
    session = None
    current_tenant = ''
    pod = ''

    def __init__(self, url, session, pod):
        ACIobject.session = session
        ACIobject.base_API_url = url + '/api/node/mo/'
        ACIobject.url = url
        ACIobject.pod = pod


class Tenant:
    """ Class representing Tenant object.
    Includes variables needed to create Tenant object:
    name, rn, dn, params dictionary - which is a payload for POST method.
    exist - boolean value which is True if object already exists or False otherwise
    ACIobject.current_tenant is being set by this class.
    ACIobject.objects_dict is being updated with rn as this value may be required by some lower level objects.
    """

    def __init__(self, name):
        self.name = name
        self.rn = f'tn-{name}'
        self.dn = 'uni/' + self.rn
        self.status = ''
        self.children = []
        self.url = ACIobject.base_API_url + self.dn + '.json'
        ACIobject.objects_dict[name] = self.rn
        self.exist = True if int(ACIobject.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        if self.exist:
            ACIobject.current_tenant = self.rn
        ACIobject.session.log.info(f'Checking Tenant object - {self.name} - EXIST - {self.exist}.')
        self.params = {
            "fvTenant": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "rn": self.rn,
                }
            }
        }

    def create(self):
        """ Creates Tenant object in ACI. Params dictionary is a payload for REST POST method."""

        ACIobject.session.log.info(f'Creating TENANT object - {self.name}')
        self.status = 'created'
        ACIobject.session.invoke(self.url, method='POST', data=json.dumps(self.params))
        ACIobject.current_tenant = self.rn


class VRF:
    """ Class representing VRF object.
    Includes variables needed to create VRF object:
    name, rn, dn, params dictionary - which is a payload for POST method.
    exist - boolean value which is True if object already exists or False otherwise
    ACIobject.objects_dict is being updated with rn as this value may be required by some lower level objects.
    """

    def __init__(self, params):
        self.name = params.get('name')
        self.rn = f'ctx-{self.name}'
        self.dn = f"uni/{ACIobject.current_tenant}/{self.rn}"
        self.status = ''
        self.children = []
        self.url = ACIobject.base_API_url + self.dn + '.json'
        ACIobject.objects_dict[self.name] = self.rn
        self.exist = True if int(ACIobject.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIobject.session.log.info(f'Checking VRF object - {self.name} - EXIST - {self.exist}.')
        self.params = {
            "fvCtx": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "rn": self.rn,
                    "pcEnfPref": params.get('pcEnfPref', 'enforced'),
                    "pcEnfDir": params.get('pcEnfDir', 'ingress'),
                    "ipDataPlaneLearning": params.get('ipDataPlaneLearning', 'enabled')
                }
            }
        }

    def create(self):
        """ Creates VRF object in ACI. Params dictionary is a payload for REST POST method."""

        ACIobject.session.log.info(f'Creating VRF object - {self.name}')
        self.status = 'created'
        ACIobject.session.invoke(self.url, method='POST', data=json.dumps(self.params))


class BD:
    """ Class representing Bridge-Domain object.
    Includes variables needed to create Bridge-Domain object:
    name, rn, dn, params dictionary - which is a payload for POST method.
    exist - boolean value which is True if object already exists or False otherwise
    Checks if vrf that should be assigned to BD is present in passed params argument - set None if it's not.
    ACIobject.objects_dict is being updated with rn as this value may be required by some lower level objects.
    """

    def __init__(self, params):
        self.name = params[3]
        self.rn = f'BD-{self.name}'
        self.dn = f"uni/{ACIobject.current_tenant}/{self.rn}"
        self.status = ''
        self.children = []
        self.url = ACIobject.base_API_url + self.dn + '.json'
        self.url_assignments = ACIobject.base_API_url + self.dn + '/'
        ACIobject.objects_dict[self.name] = self.rn
        self.exist = True if int(ACIobject.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIobject.session.log.info(f'Checking BD object - {self.name} - EXIST - {self.exist}.')
        self.vrf = params[4].get('vrf', None)
        self.params = {
            "fvBD": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "rn": self.rn,
                    "arpFlood": params[4].get('arpFlood', 'true')
                }
            }
        }

    def create(self):
        """ Creates Bridge-Domain object in ACI. Params dictionary is a payload for REST POST method."""

        ACIobject.session.log.info(f'Creating BD object - {self.name}')
        self.status = 'created'
        ACIobject.session.invoke(self.url, method='POST', data=json.dumps(self.params))

    def assign_vrf(self):
        """ Assigns VRF to a Bridge-Domain. vrf_params dictionary is a payload for REST POST method."""

        ACIobject.session.log.info(f'Assigning VRF - {self.vrf} to BD - {self.name}')
        vrf_params = {
            "fvRsCtx": {
                "attributes": {
                    "tnFvCtxName": self.vrf
                }
            }
        }
        ACIobject.session.invoke(self.url_assignments + 'rsctx.json', method='POST', data=json.dumps(vrf_params))

    def assign_subnet(self, subnet_dict):
        """ Assigns subnet to a Bridge-Domain. subnet dictionary is a payload for REST POST method."""

        subnet = {
            "fvSubnet": {
                "attributes": {
                    "dn": f"{self.dn}/subnet-[{subnet_dict['ip']}]",
                    "ctrl": subnet_dict.get('ctrl', ''),
                    "ip": subnet_dict['ip'],
                    "scope": subnet_dict.get('scope', 'private'),
                    "preferred": subnet_dict.get('preferred', 'false'),
                    "rn": f"subnet-[{subnet_dict['ip']}]"
                }
            }
        }
        ACIobject.session.invoke(self.url_assignments + f'subnet-[{subnet_dict["ip"]}].json', method='POST',
                                 data=json.dumps(subnet))


class Filter:
    """ Class representing Filter object.
    Includes variables needed to create Filter object:
    name, rn, dn, params dictionary - which is a payload for POST method.
    exist - boolean value which is True if object already exists or False otherwise
    ACIobject.objects_dict is being updated with rn as this value may be required by some lower level objects.
    """

    def __init__(self, params):
        self.name = params[3]['name']
        self.rn = f'flt-{self.name}'
        self.dn = f"uni/{ACIobject.current_tenant}/{self.rn}"
        self.status = ''
        self.children = []
        self.url = ACIobject.base_API_url + self.dn + '.json'
        self.url_assignments = ACIobject.base_API_url + self.dn + '/'
        ACIobject.objects_dict[self.name] = self.rn
        self.exist = True if int(ACIobject.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIobject.session.log.info(f'Checking FILTER object - {self.name} - EXIST - {self.exist}.')
        self.params = {
            "vzFilter": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "rn": self.rn
                }
            }
        }

    def create(self):
        """ Creates Filter object in ACI. Params dictionary is a payload for REST POST method."""

        ACIobject.session.log.info(f'Creating FILTER object - {self.name}')
        self.status = 'created'
        ACIobject.session.invoke(self.url, method='POST', data=json.dumps(self.params))

    def assign_entry(self, entry_dict):
        """ Assigns Entry to a Filter. entry_params dictionary is a payload for REST POST method.
        If 'sToPort' or 'dToPort' parameters are missing they're replaced by 'sFromPort' and 'dFromPort' respectively.
        If more parameters is missing they're replaced by 'unspecified' and a kind of placeholder is being created.
        """

        if "sFromPort" in entry_dict and "sToPort" not in entry_dict:
            entry_dict["sToPort"] = entry_dict["sFromPort"]
        if "dFromPort" in entry_dict and "dToPort" not in entry_dict:
            entry_dict["dToPort"] = entry_dict["dFromPort"]
        entry_params = {
            "vzEntry": {
                "attributes": {
                    "dn": f'{self.dn}/e-{entry_dict["name"]}',
                    "name": entry_dict["name"],
                    "etherT": entry_dict["etherT"],
                    "prot": entry_dict.get("prot", "unspecified"),
                    "sFromPort": entry_dict.get("sFromPort", "unspecified"),
                    "sToPort": entry_dict.get("sToPort", "unspecified"),
                    "dFromPort": entry_dict.get("dFromPort", "unspecified"),
                    "dToPort": entry_dict.get("dToPort", "unspecified"),
                    "rn": f'e-{entry_dict["name"]}'
                }
            }
        }
        if entry_params["vzEntry"]["attributes"]["sFromPort"] == "unspecified":
            del entry_params["vzEntry"]["attributes"]["sFromPort"]
            del entry_params["vzEntry"]["attributes"]["sToPort"]
        if entry_params["vzEntry"]["attributes"]["dFromPort"] == "unspecified":
            del entry_params["vzEntry"]["attributes"]["dFromPort"]
            del entry_params["vzEntry"]["attributes"]["dToPort"]
        ACIobject.session.invoke(self.url_assignments + f'/e-{entry_dict["name"]}.json', method='POST',
                                 data=json.dumps(entry_params))


class Contract:
    """ Class representing Contract object.
    Includes variables needed to create Contract object:
    name, rn, dn, params dictionary - which is a payload for POST method.
    exist - boolean value which is True if object already exists or False otherwise
    Checks the scope and sets it to 'vrf' if scope is not present.
    ACIobject.objects_dict is being updated with rn as this value may be required by some lower level objects.
    """

    def __init__(self, params):
        self.scope = params.get('scope', 'vrf')
        self.name = params['name']
        self.rn = f'brc-{self.name}'
        self.dn = f"uni/{ACIobject.current_tenant}/{self.rn}"
        self.status = ''
        self.children = []
        self.url = ACIobject.base_API_url + self.dn + '.json'
        self.url_assignments = ACIobject.base_API_url + self.dn + '/'
        ACIobject.objects_dict[self.name] = self.rn
        self.exist = True if int(ACIobject.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIobject.session.log.info(f'Checking CONTRACT object - {self.name} - EXIST - {self.exist}.')
        self.params = {
            "vzBrCP": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "rn": self.rn
                }
            }
        }
        if self.scope != 'vrf':
            self.params["vzBrCP"]["attributes"]["scope"] = self.scope

    def create(self):
        """ Creates Contract object in ACI. Params dictionary is a payload for REST POST method."""

        ACIobject.session.log.info(f'Creating CONTRACT object - {self.name}')
        self.status = 'created'
        ACIobject.session.invoke(self.url, method='POST', data=json.dumps(self.params))

    def assign_subject(self, subj_dict):
        """ Assigns Subject to Contract. subject_params dictionary is a payload for REST POST method."""

        self.subject_name = subj_dict["name"]
        subject_params = {
            "vzSubj": {
                "attributes": {
                    "dn": f'{self.dn}/subj-{subj_dict["name"]}',
                    "name":  self.subject_name,
                    "rn": f'subj-{subj_dict["name"]}',
                    "revFltPorts": subj_dict.get("revFltPorts", "true")
                }
            }
        }
        ACIobject.session.invoke(self.url_assignments + f'/subj-{subj_dict["name"]}.json', method='POST',
                                 data=json.dumps(subject_params))

    def assign_filter(self, filter_dict):
        """ Assigns Filter to Subject. filter_params dictionary is a payload for REST POST method."""

        filter_params = {
            "vzRsSubjFiltAtt": {
                "attributes": {
                    "tnVzFilterName": filter_dict["name"],
                    "directives": filter_dict.get("directives", ""),
                    "action": filter_dict.get("action", "permit"),
                    "priorityOverride": filter_dict.get("priorityOverride", "")
                }
            }
        }
        if not filter_params['vzRsSubjFiltAtt']['attributes']["priorityOverride"]:
            del filter_params['vzRsSubjFiltAtt']['attributes']["priorityOverride"]
        ACIobject.session.invoke(self.url_assignments + f'/subj-{self.subject_name}.json', method='POST',
                                 data=json.dumps(filter_params))


class AppProfile:
    """ Class representing Application Profile object.
    Includes variables needed to create Application Profile object:
    name, rn, dn, params dictionary - which is a payload for POST method.
    exist - boolean value which is True if object already exists or False otherwise
    Checks if EPG is present in params. If it is, variables related to EPG configuration are being populated.
    ACIobject.objects_dict is being updated with rn as this value may be required by some lower level objects.
    """

    def __init__(self, params):
        self.name = params[2]
        self.rn = f'ap-{self.name}'
        self.dn = f"uni/{ACIobject.current_tenant}/{self.rn}"
        self.status = ''
        self.children = []
        self.url = ACIobject.base_API_url + self.dn + '.json'
        self.url_assignments = ACIobject.base_API_url + self.dn + '/'
        ACIobject.objects_dict[self.name] = self.rn
        self.exist = True if int(ACIobject.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIobject.session.log.info(f'Checking APP PROFILE object - {self.name} - EXIST - {self.exist}.')
        self.epg_present = True if len(params) > 3 else False
        if self.epg_present:
            self.epg_name = params[3]
            self.epg_rn = f'epg-{self.epg_name}'
            self.epg_dn = self.dn + f'/{self.epg_rn}'
            self.epg_url = self.url_assignments + f'{self.epg_rn}.json'
            self.fabric_domain = params[4].get("fabric-domain", "")
            self.epg_ports = params[4].get("static-ports", [])
        self.params = {
            "fvAp": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "rn": self.rn
                }
            }
        }

    def create(self):
        """ Creates Application Profile object in ACI. Params dictionary is a payload for REST POST method."""

        ACIobject.session.log.info(f'Creating APP PROFILE object - {self.name}')
        self.status = 'created'
        ACIobject.session.invoke(self.url, method='POST', data=json.dumps(self.params))

    def assign_epg(self, epg_params_in):
        """ Assigns EPG to Application Profile. epg_params dictionary is a payload for REST POST method.
        Assigns Bridge-Domain to EPG - Bridge-Domain configuration is represented as a 'children' object in epg_params.
        """

        epg_params = {
            "fvAEPg": {
                "attributes": {
                    "dn": self.epg_dn,
                    "name": self.epg_name,
                    "rn": self.epg_rn
                },
                "children": [
                    {
                        "fvRsBd": {
                            "attributes": {
                                "tnFvBDName": epg_params_in['bridge-domain']
                            }
                        }
                     }
                ]
            }
        }
        ACIobject.session.invoke(self.epg_url, method='POST', data=json.dumps(epg_params))

        # If Physical-Domain (fabric_domain) is present in EPG parameters it's also getting assigned to EPG in a
        # separate POST request.

        if self.fabric_domain:
            fabric_params = {
                "fvRsDomAtt": {
                    "attributes": {
                        "resImedcy": epg_params.get("resImedcy", "immediate"),
                        "tDn": f"uni/phys-{self.fabric_domain}"
                    }
                }
            }
            ACIobject.session.invoke(self.epg_url, method='POST', data=json.dumps(fabric_params))

        # VPC port-channels and standalone ports are being assigned to EPG with proper parameters (mode, encap, etc.).

        for port in self.epg_ports:
            port_path = port['path']
            port_encap = 'vlan-' + str(port['encap'])
            port_mode = port.get('mode', 'trunk')
            port_instrImedcy = port.get('instrImedcy', 'on-demand')
            port_params = {
                "fvRsPathAtt": {
                    "attributes": {
                        "mode": port_mode,
                        "encap": port_encap,
                        "instrImedcy": port_instrImedcy
                    }
                }
            }
            if 'vpc' in port_path.lower():
                vpc_id_1 = port_path.split('-')[1]
                vpc_id_2 = port_path.split('-')[2]
                tDn = f'topology/{ACIobject.pod}/protpaths-{vpc_id_1}-{vpc_id_2}/pathep-[{port_path}]'
            else:
                leaf_id = port_path.split('-')[1]
                port_id = port_path.split('-')[-1]
                tDn = f'topology/{ACIobject.pod}/paths-{leaf_id}/pathep-[{port_id}]'
            rn = f'rspathAtt-[{tDn}]'
            dn = self.epg_dn + f'/{rn}'
            port_params["fvRsPathAtt"]["attributes"]["tDn"] = tDn
            port_params["fvRsPathAtt"]["attributes"]["rn"] = rn
            port_params["fvRsPathAtt"]["attributes"]["dn"] = dn
            port_url = self.url_assignments + f'{self.epg_rn}/{rn}.json'
            if port_params["fvRsPathAtt"]["attributes"]["mode"] == 'trunk':
                del port_params["fvRsPathAtt"]["attributes"]["mode"]
            if port_params["fvRsPathAtt"]["attributes"]["instrImedcy"] == 'on-demand':
                del port_params["fvRsPathAtt"]["attributes"]["instrImedcy"]
            ACIobject.session.invoke(port_url, method='POST', data=json.dumps(port_params))


class EPGrelation:
    """ Class representing relations between EPGs and Contracts.
    Parent's dn for EPG is needed in order to create proper URL (for contract registering).
    Based on Contract names listed in 'consumes' and 'provides' lists (per EPG) proper relations are being built.
    """

    def __init__(self, params):
        self.epg_name = params['epg']
        self.consumes = params.get('consumes', [])
        self.provides = params.get('provides', [])
        self.query_params = {
            "class": "fvAEPg",
            "parameter_type": "name",
            "parameter_value": self.epg_name
        }
        query_url = ACIobject.url + f'/api/class/{self.query_params["class"]}.json'
        parent = ACIobject.session.invoke(query_url, method='GET_with_filter', data=self.query_params)
        try:
            parent_dn = parent['imdata'][0]['fvAEPg']['attributes']['dn']
        except Exception as e:
            raise ErrorHandler.MissingParentError(msg=e)
        contract_url = ACIobject.base_API_url + parent_dn + '.json'
        for contract in self.provides:
            contract_params = {
                "fvRsProv": {
                    "attributes": {
                        "tnVzBrCPName": contract
                    }
                }
            }
            ACIobject.session.invoke(contract_url, method='POST', data=json.dumps(contract_params))
        for contract in self.consumes:
            contract_params = {
                "fvRsCons": {
                    "attributes": {
                        "tnVzBrCPName": contract
                    }
                }
            }
            ACIobject.session.invoke(contract_url, method='POST', data=json.dumps(contract_params))

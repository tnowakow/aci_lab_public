import ErrorHandler


class MSOobject:
    """ Class that includes general session related variables - url to APIC, Tenant and Pod under which all lower level
    objects should be configured.
    Those parameters are available for other ACI object related classes.
    """

    def __init__(self, url, session, dry=True):
        self.session = session
        self.base_API_url = url + '/api/v1/'
        self.url = url
        self.dry = dry
        self.errors = []
        self.well_known_ports = {
            '443': 'https',
            '25': 'smtp',
            '80': 'http',
            '20': 'ftp-data',
            '53': 'dns',
            '110': 'pop3',
            '554': 'rtsp',
            '22': 'ssh'
        }
        # updated by set_base_vars function
        self.schema_url = None
        self.template_path = None
        self.query_url = None
        self.template_displayName = None
        self.tenant_id = None
        self.template_name = None
        self.schema_id = None
        self.pod = None
        self.log = None
        self.dry_schema = None
        self.original_schema = None

    def get_sites(self):
        return self.session.invoke(self.base_API_url + 'sites')

    def get_tenants(self):
        return self.session.invoke(self.base_API_url + 'tenants')

    def get_schemas(self):
        return self.session.invoke(self.base_API_url + 'schemas')

    def refresh_schema(self):
        if self.dry:
            return self.dry_schema
        return self.session.invoke(self.schema_url)

    def update_schema(self, payload, requester=None):
        if not self.dry:
            self.session.invoke(self.schema_url, method='PUT', data=payload, requester=requester)

    def set_base_vars(self, data):
        self.schema_url = self.base_API_url + 'schemas/' + data['Schema-id']
        self.template_path = '/templates/' + data['Template-name'] + '/'
        self.query_url = self.schema_url + self.template_path
        self.template_displayName = data['Template-displayName']
        self.tenant_id = data['Tenant-id']
        self.template_name = data['Template-name']
        self.schema_id = data['Schema-id']
        self.pod = data['Pod-name']
        self.log = []
        if self.dry:
            self.dry_schema = self.session.invoke(self.schema_url)
            self.original_schema = self.session.invoke(self.schema_url)

    def check_if_exist(self, object_type, object_name):
        url = self.query_url + f'{object_type + "s"}/{object_name}'
        return False if self.session.invoke(url, method='GET')[object_type] is None else True

    @staticmethod
    def compare_lists(list_a, list_b):
        if isinstance(list_a, list) and isinstance(list_b, list):
            for item in list_a:
                if item not in list_b:
                    return True
            for item in list_b:
                if item not in list_a:
                    return True
            return False
        elif isinstance(list_a, dict) and isinstance(list_b, dict):
            assert False, 'Dictionary sent to compare_lists function!!!'
        else:
            return False if list_a == list_b else True

    def log_func(self, msg):
        self.log.append(msg)
        self.session.log.info(msg)

    def create_vrf_patch(self, params, op='add'):
        payload = [{
            "op": op,
            "path": self.template_path + 'vrfs/-',
            "value": {
                "displayName": params[3]['name'],
                "name": params[3]['name'],
                "pcEnfPref": params[3].get('pcEnfPref', 'enforced'),
                "pcEnfDir": params[3].get('pcEnfDir', 'ingress'),
                "ipDataPlaneLearning": params[3].get('ipDataPlaneLearning', 'enabled')
            }
        }]
        self.session.invoke(self.schema_url, method='PATCH', data=payload)

    def create_vrf(self, params):
        schema = self.refresh_schema()
        vrf_name = params[3]['name']
        vrf_found = False
        change = False
        payload = {
            "name": vrf_name,
            "displayName": vrf_name,
            "vrfRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/vrfs/{vrf_name}",
            "l3MCast": params[3].get('l3MCast', False),
            "preferredGroup": params[3].get('preferredGroup', False),
            "vzAnyEnabled": params[3].get('vzAnyEnabled', False),
            "vzAnyProviderContracts": params[3].get('vzAnyProviderContracts', []),
            "vzAnyConsumerContracts": params[3].get('vzAnyConsumerContracts', []),
            "rpConfigs": params[3].get('rpConfigs', []),
            "pcEnfPref": params[3].get('pcEnfPref', 'enforced'),
            "ipDataPlaneLearning": params[3].get('ipDataPlaneLearning', 'enabled')
        }
        for template in schema['templates']:
            if template['displayName'] == self.template_displayName and template['tenantId'] == self.tenant_id \
                    and template['name'] == self.template_name:
                for vrf in template['vrfs']:
                    if vrf['name'] == vrf_name or vrf['displayName'] == vrf_name:
                        self.session.log.info(f"VRF: {vrf_name} - found in config - checking parameters.")
                        vrf_found = True
                        for param in payload:
                            if vrf.get(param, False) and isinstance(vrf[param], list) or isinstance(vrf[param], tuple):
                                for p in payload.get(param, []):
                                    if p not in vrf[param]:
                                        self.log_func(f'Template: {template["name"]}, VRF: {vrf["name"]}, '
                                                      f'param: {param}, appending: {p}')
                                        vrf[param].append(p)
                                        change = True
                            else:
                                if vrf[param] != payload[param]:
                                    self.log_func(f'Template: {template["name"]}, VRF: {vrf["name"]}, '
                                                  f'param: {param}\nREPLACING: {vrf[param]}\nWITH: {payload[param]}')
                                    vrf[param] = payload[param]
                                    change = True
                if not vrf_found:
                    self.log_func(f'Template: {template["name"]}, Creating VRF object: {vrf_name}')
                    template['vrfs'].append(payload)
                    change = True
        if change:
            self.update_schema(schema, requester=f"create_vrf, {vrf_name}")
        else:
            self.session.log.info(f"VRF: {vrf_name} - nothing to update")

    def create_BD(self, params):
        vrf = params[4].get('vrf', None)
        schema = self.refresh_schema()
        subnets = []
        change = False
        bd_site_found = False
        bd_template_found = False
        if 'subnets' in params[4]:
            for subnet in params[4]['subnets']:
                subnets_dict_tmp = {
                    "ip": subnet['ip'],
                    "scope": "private" if "private" in subnet["scope"] else "public",
                    "shared": True if "shared" in subnet["scope"] else False,
                    "querier": True if "querier" in subnet["ctrl"] else False,
                    "noDefaultGateway": True if "noDefaultGateway" in subnet["ctrl"] else False,
                    "virtual": subnet.get("virtual", False)
                }
                subnets.append(subnets_dict_tmp)
        if params[4].get('l2Stretch', False):
            l2stretch = True
        else:
            l2stretch = False
        payload = {
            "name": params[3],
            "displayName": params[3],
            "bdRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/bds/{params[3]}",
            "l2UnknownUnicast": params[4].get('l2UnknownUnicast', "proxy"),
            "intersiteBumTrafficAllow": params[4].get('intersiteBumTrafficAllow', False),
            "optimizeWanBandwidth": params[4].get('optimizeWanBandwidth', False),
            "l2Stretch": l2stretch,
            "subnets": subnets if l2stretch else [],
            "vrfRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/vrfs/{vrf}",
            "unkMcastAct": params[4].get('unkMcastAct', "flood"),
            "v6unkMcastAct": params[4].get('v6unkMcastAct', "flood"),
            "arpFlood": True if params[4].get('arpFlood', 'true') == 'true' else False,
            "multiDstPktAct": params[4].get('multiDstPktAct', "bd-flood")
        }
        if payload["arpFlood"] and payload["l2Stretch"]:
            raise ErrorHandler.MutualExlusiveParametersSet(msg=payload)
        if not payload["l2Stretch"]:
            for site in schema['sites']:
                if site['templateName'] == self.template_name:
                    payload_site = {
                        "bdRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/bds/{params[3]}",
                        "subnets": subnets
                    }
                    for bd in site['bds']:
                        if bd['bdRef'] == payload_site['bdRef']:
                            self.session.log.info(f'BD object (site) - {params[3]} found, checking parameters')
                            bd_site_found = True
                            if self.compare_lists(bd['subnets'], payload_site['subnets']):
                                self.session.log.info(f'BD object (site) - {params[3]} updating subnets.')
                                for sub in payload_site['subnets']:
                                    if sub not in bd['subnets']:
                                        self.log_func(f'Site: {site["templateName"]}, BD: {params[3]}, '
                                                      f'appending subnet: {sub}')
                                        bd['subnets'].append(sub)
                                        change = True
                            else:
                                self.session.log.info(f'BD object (site) - {params[3]} nothing to update.')
                    if not bd_site_found:
                        self.log_func(f'Site: {site["templateName"]}, Creating BD: {params[3]}')
                        site['bds'].append(payload_site)
                        change = True
        for template in schema['templates']:
            if template['displayName'] == self.template_displayName and template['tenantId'] == self.tenant_id \
                    and template['name'] == self.template_name:
                for bds in template['bds']:
                    if bds['name'] == payload['name'] or bds['displayName'] == payload['displayName']:
                        self.session.log.info(f'BD object (template) - {params[3]} found, updating parameters')
                        bd_template_found = True
                        for param in payload:
                            if bds.get(param, False) and isinstance(bds[param], list) or isinstance(bds[param], tuple):
                                for p in payload.get(param, []):
                                    if p not in bds[param]:
                                        self.log_func(f'Template: {template["name"]}, BD: {bds["name"]}, '
                                                      f'param: {param}, appending: {p}')
                                        bds[param].append(p)
                                        change = True
                            else:
                                if bds[param] != payload[param]:
                                    self.log_func(f'Template: {template["name"]}, BD: {bds["name"]}, '
                                                  f'param: {param}\nREPLACING: {bds[param]}\nWITH: {payload[param]}')
                                    bds[param] = payload[param]
                                    change = True
                if not bd_template_found:
                    self.log_func(f'Template: {template["name"]}, Creating BD object (template): {params[3]}')
                    template['bds'].append(payload)
                    change = True
                break
        if change:
            self.update_schema(schema, requester=f"create_BD, {payload['name']}")
        else:
            self.session.log.info(f'BD object: {params[3]} - nothing to update')

    def create_filter(self, params):
        schema = self.refresh_schema()
        filter_found = False
        change = False
        payload = {
            "name": params[3]['name'],
            "displayName": params[3]['name'],
            "filterRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/filters/{params[3]['name']}",
            "entries": []
        }
        if params[3]["entries"]:
            for entry in params[3]["entries"]:
                tmp_entry_dict = {
                    "name": entry["name"],
                    "displayName": entry["name"],
                    "etherType": entry.get("etherT", "unspecified"),
                    "arpFlag": entry.get("arpFlag", "unspecified"),
                    "ipProtocol": entry.get("prot", "unspecified"),
                    "matchOnlyFragments": entry.get("matchOnlyFragments", False),
                    "stateful": entry.get("stateful", False),
                    "sourceFrom": entry.get("sFromPort", "unspecified"),
                    "sourceTo": entry.get("sToPort", "unspecified"),
                    "destinationFrom": entry.get("dFromPort", "unspecified"),
                    "destinationTo": entry.get("dToPort", "unspecified"),
                    "tcpSessionRules": entry.get("tcpSessionRules", ["unspecified"])
                }
                if tmp_entry_dict["sourceFrom"] in self.well_known_ports:
                    tmp_entry_dict["sourceFrom"] = self.well_known_ports[tmp_entry_dict["sourceFrom"]]
                if tmp_entry_dict["sourceTo"] in self.well_known_ports:
                    tmp_entry_dict["sourceTo"] = self.well_known_ports[tmp_entry_dict["sourceTo"]]
                if tmp_entry_dict["destinationFrom"] in self.well_known_ports:
                    tmp_entry_dict["destinationFrom"] = self.well_known_ports[tmp_entry_dict["destinationFrom"]]
                if tmp_entry_dict["destinationTo"] in self.well_known_ports:
                    tmp_entry_dict["destinationTo"] = self.well_known_ports[tmp_entry_dict["destinationTo"]]
                payload["entries"].append(tmp_entry_dict)
        for template in schema['templates']:
            if template['displayName'] == self.template_displayName and template['tenantId'] == self.tenant_id \
                    and template['name'] == self.template_name:
                for filt in template['filters']:
                    if filt['name'] == params[3]['name']:
                        self.session.log.info(f'FILTER - {params[3]["name"]} found, updating parameters')
                        filter_found = True
                        for param in payload:
                            if filt.get(param, False) and isinstance(filt[param], list) or \
                                    isinstance(filt[param], tuple):
                                for p in payload.get(param, []):
                                    if p not in filt[param]:
                                        self.log_func(f'Template: {template["name"]}, Filter: {filt["name"]}, '
                                                      f'param: {param}, appending: {p}')
                                        filt[param].append(p)
                                        change = True
                            else:
                                if filt[param] != payload[param]:
                                    self.log_func(f'Template: {template["name"]}, Filter: {filt["name"]}, '
                                                  f'param: {param}\nREPLACING: {filt[param]}\nWITH: {payload[param]}')
                                    filt[param] = payload[param]
                                    change = True
                        break
                if not filter_found:
                    self.log_func(f'Template: {template["name"]}, Creating FILTER object: {params[3]["name"]}')
                    template['filters'].append(payload)
                    change = True
                break
        if change:
            self.update_schema(schema, requester=f"create_filter, {payload['name']}")
        else:
            self.session.log.info(f'FILTER: {params[3]["name"]} - nothing to update')

    def create_contract(self, params):
        filters = []
        existing_filters = []
        schema = self.refresh_schema()
        contract_found = False
        contract_name = params[3]['name']
        change = False
        for template in schema['templates']:
            if template['displayName'] == self.template_displayName and template['tenantId'] == self.tenant_id \
                    and template['name'] == self.template_name:
                for fil in template['filters']:
                    existing_filters.append(fil.get('name', ''))
        existing_filters = list(set(existing_filters))
        for subj in params[3]['subjects']:
            for filt in subj['filters']:
                if 'name' not in filt or filt['name'] not in existing_filters:
                    self.session.log.error(f"Non-existing filter called: {filt}, contract: {contract_name} - skipping")
                    self.errors.append(f'Non-existing filter object: {filt}, called by contract: {contract_name}')
                if filt not in filters:
                    tmp_directives = []
                    if not filt.get('directives', None):
                        tmp_directives = ['none']
                    else:
                        tmp_directives.append(filt['directives'])
                    tmp_filt = {
                        "filterRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/filters/{filt['name']}",
                        "directives": tmp_directives,
                        "action": filt.get("action", "permit"),
                        "priorityOverride": filt.get("priorityOverride", "default")
                    }
                    filters.append(tmp_filt)
        scope = params[3].get("scope", "context")
        if scope == 'vrf':
            scope = 'context'
        payload = {
            "name": contract_name,
            "displayName": contract_name,
            "contractRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/contracts/{contract_name}",
            "filterRelationships": filters,
            "prio": params[3].get("prio", "unspecified"),
            "scope": scope,
            "filterType": params[3].get("filterType", "bothWay"),
            "filterRelationshipsProviderToConsumer": params[3].get("filterRelationshipsProviderToConsumer", []),
            "filterRelationshipsConsumerToProvider": params[3].get("filterRelationshipsConsumerToProvider", [])
        }
        for template in schema['templates']:
            if template['displayName'] == self.template_displayName and template['tenantId'] == self.tenant_id \
                    and template['name'] == self.template_name:
                for contract in template['contracts']:
                    if contract['name'] == contract_name:
                        self.session.log.info(f'CONTRACT - {params[3]["name"]} found, updating parameters')
                        contract_found = True
                        for param in payload:
                            if contract.get(param, False) and isinstance(contract[param], list) or \
                                    isinstance(contract[param], tuple):
                                for p in payload.get(param, []):
                                    if p not in contract[param]:
                                        self.log_func(f'Template: {template["name"]}, CONTRACT: {contract["name"]}, '
                                                      f'param: {param}, appending: {p["filterRef"].split("/")[-1]}')
                                        contract[param].append(p)
                                        change = True
                            else:
                                if contract[param] != payload[param]:
                                    self.log_func(f'Template: {template["name"]}, CONTRACT: {contract["name"]}, '
                                                  f'param: {param}\nREPLACING: {contract[param]}'
                                                  f'\nWITH: {payload[param]}')
                                    contract[param] = payload[param]
                                    change = True
                        break
                if not contract_found:
                    self.log_func(f'Template: {template["name"]}, Creating contract: {params[3]["name"]}')
                    template['contracts'].append(payload)
                    change = True
                break
        if change:
            self.update_schema(schema, requester=f"create_contract, {payload['name']}")
        else:
            self.session.log.info(f'Contract: {params[3]["name"]} - nothing to update')

    def create_anps(self, params):
        schema = self.refresh_schema()
        anpRef = f"/schemas/{self.schema_id}/templates/{self.template_name}/anps/{params[2]}"
        epgRef = f"/schemas/{self.schema_id}/templates/{self.template_name}/anps/{params[2]}/epgs/{params[3]}"
        change = False
        payload_epg = {
            "name": params[3],
            "displayName": params[3],
            "epgRef": epgRef,
            "contractRelationships": params[4].get('contractRelationships', []),
            "subnets": params[4].get('subnets', []),
            "uSegEpg": params[4].get("uSegEpg", False),
            "uSegAttrs": params[4].get('uSegAttrs', []),
            "intraEpg": params[4].get('intraEpg', "unenforced"),
            "prio": params[4].get('prio', "unspecified"),
            "proxyArp": params[4].get("proxyArp", False),
            "preferredGroup": params[4].get("preferredGroup", False),
            "bdRef": f"/schemas/{self.schema_id}/templates/{self.template_name}/bds/{params[4]['bridge-domain']}",
            "vrfRef": params[4].get("vrfRef", ""),
            "selectors": params[4].get('selectors', []),
            "epgType": params[4].get("epgType", "application")
        }
        static_ports = []
        for port in params[4].get('static-ports', []):
            port_path = port['path']
            port_encap = int(port['encap'])
            port_mode = port.get('mode', 'regular')
            if port_mode == 'trunk':
                port_mode = 'regular'
            port_deploymentImmediacy = port.get('instrImedcy', 'lazy')
            if port_deploymentImmediacy == 'on-demand':
                port_deploymentImmediacy = 'lazy'
            if 'vpc' in port['path'].lower():
                type_p = 'vpc'
                vpc_id_1 = port_path.split('-')[1]
                vpc_id_2 = port_path.split('-')[2]
                path = f'topology/{self.pod}/protpaths-{vpc_id_1}-{vpc_id_2}/pathep-[{port_path}]'
            else:
                type_p = 'port'
                leaf_id = port_path.split('-')[1]
                port_id = port_path.split('-')[-1]
                path = f'topology/{self.pod}/paths-{leaf_id}/pathep-[{port_id}]'
            tmp_port_dict = {
                "type": type_p,
                "path": path,
                "portEncapVlan": port_encap,
                "deploymentImmediacy": port_deploymentImmediacy,
                "mode": port_mode
            }
            static_ports.append(tmp_port_dict)
        payload_epg_site = {
            "epgRef": epgRef,
            "domainAssociations": [{
                "dn": f"uni/phys-{params[4]['fabric-domain']}",
                "domainType": params[4].get('domainType', "physicalDomain"),
                "deployImmediacy": params[4].get('deployImmediacy', "lazy"),
                "resolutionImmediacy": params[4].get('resolutionImmediacy', "lazy")
            }],
            "staticPorts": static_ports,
            "staticLeafs": params[4].get('staticLeafs', []),
            "uSegAttrs": params[4].get('uSegAttrs', []),
            "subnets": params[4].get('subnets', []),
            "selectors": params[4].get('selectors', [])
        }
        for site in schema['sites']:
            existing_app = False
            epg_found = False
            if site['templateName'] == self.template_name:
                for anp in site['anps']:
                    if anp.get('anpRef', '') == anpRef:
                        existing_app = True
                        for epg in anp['epgs']:
                            if epg['epgRef'] == epgRef:
                                epg_found = True
                                for param in epg:
                                    if param == 'contractRelationships':
                                        continue
                                    if epg.get(param, False) and isinstance(epg[param], list) or \
                                            isinstance(epg[param], tuple):
                                        for p in payload_epg_site.get(param, []):
                                            if p not in epg[param]:
                                                self.log_func(f'Site: {site["templateName"]}, ANP: {params[2]}, '
                                                              f'EPG: {params[3]}, param: {param}, appending: {p}')
                                                epg[param].append(p)
                                                change = True
                                    else:
                                        if epg[param] != payload_epg_site[param]:
                                            self.log_func(f'Site: {site["templateName"]}, ANP: {params[2]}, '
                                                          f'EPG: {params[3]}, param: {param}\nREPLACING: '
                                                          f'{epg[param]}\nWITH: {payload_epg_site[param]}')
                                            epg[param] = payload_epg_site[param]
                                            change = True
                        if not epg_found:
                            self.log_func(f'Site: {site["templateName"]}, ANP: {params[2]}, ADDING EPG: {params[3]}')
                            anp['epgs'].append(payload_epg_site)
                            change = True
                        break
                if not existing_app:
                    payload_site = {
                        "anpRef": anpRef,
                        "epgs": [payload_epg_site]
                    }
                    self.log_func(f'Site: {site["templateName"]}, ADDING ANP: {params[2]} WITH EPG: {params[3]}')
                    site['anps'].append(payload_site)
                    change = True
        for template in schema['templates']:
            existing_app = False
            epg_found = False
            if template['displayName'] == self.template_displayName and template['tenantId'] == self.tenant_id \
                    and template['name'] == self.template_name:
                for anp in template['anps']:
                    if anp['name'] == params[2]:
                        existing_app = True
                        for epg in anp['epgs']:
                            if epg['name'] == params[3]:
                                epg_found = True
                                for param in epg:
                                    if param == 'contractRelationships':
                                        continue
                                    if epg.get(param, False) and isinstance(epg[param], list) or \
                                            isinstance(epg[param], tuple):
                                        for p in payload_epg.get(param, []):
                                            if p not in epg[param]:
                                                self.log_func(f'Template: {template["name"]}, ANP: {params[2]}, '
                                                              f'EPG: {params[3]}, param: {param}, appending: {p}')
                                                epg[param].append(p)
                                                change = True
                                    else:
                                        if epg[param] != payload_epg[param]:
                                            self.log_func(f'Template: {template["name"]}, ANP: {params[2]}, '
                                                          f'EPG: {params[3]}, param: {param}\nREPLACING: '
                                                          f'{epg[param]}\nWITH: {payload_epg[param]}')
                                            epg[param] = payload_epg[param]
                                            change = True
                        if not epg_found:
                            self.log_func(f'Template: {template["name"]}, ANP: {params[2]}, ADDING EPG: {params[3]}')
                            anp['epgs'].append(payload_epg)
                            change = True
                        break
                if not existing_app:
                    payload = {
                        "name": params[2],
                        "displayName": params[2],
                        "anpRef": anpRef,
                        "epgs": [payload_epg]
                    }
                    self.log_func(f'Template: {template["name"]}, ADDING ANP: {params[2]} WITH EPG: {params[3]}')
                    template['anps'].append(payload)
                    change = True
        if change:
            self.session.log.info(f'Creating Application Profile - {params[2]}')
            self.update_schema(schema, requester=f"create_anps, {params[2]}")
        else:
            self.session.log.info(f'Application Profile: {params[2]} - nothing to update')

    def assign_contract(self, params):
        schema = self.refresh_schema()
        epg_name = params[3]['epg']
        epg_provides = params[3].get('provides', [])
        epg_consumes = params[3].get('consumes', [])
        epg_found = False
        change = False
        for template in schema['templates']:
            for anp in template['anps']:
                for epg in anp['epgs']:
                    if epg['name'] == epg_name:
                        epg_found = True
                        for prov in epg_provides:
                            contractRef = f"/schemas/{self.schema_id}/templates/{self.template_name}/contracts/{prov}"
                            payload_provider = {
                                "relationshipType": "provider",
                                "contractRef": contractRef
                            }
                            if payload_provider not in epg['contractRelationships']:
                                epg['contractRelationships'].append(payload_provider)
                                self.log_func(f'Template: {template["name"]}, ANP: {anp["name"]}, EPG: '
                                              f'{epg["name"]}, Creating relation - {epg_name} - provides: {prov}')
                                change = True
                        for con in epg_consumes:
                            contractRef = f"/schemas/{self.schema_id}/templates/{self.template_name}/contracts/{con}"
                            payload_consumer = {
                                "relationshipType": "consumer",
                                "contractRef": contractRef
                            }
                            if payload_consumer not in epg['contractRelationships']:
                                epg['contractRelationships'].append(payload_consumer)
                                self.log_func(f'Template: {template["name"]}, ANP: {anp["name"]}, EPG: '
                                              f'{epg["name"]}, Creating relation - {epg_name} - consumes: {con}')
                                change = True
        if not epg_found:
            self.session.log.error(f"Can't assigning Contract to EPG - {epg_name} - EPG doesn't exist.")
            self.errors.append(f"Can't assigning Contract to EPG - {epg_name} - EPG doesn't exist.")
        else:
            if change:
                self.session.log.info(f'Assigning Contract to EPG - {epg_name}')
                self.update_schema(schema, requester=f"assign_contract, {epg_name}")
            else:
                self.session.log.info(f'Assigning Contract to EPG: {epg_name} - nothing to assign.')

import ACI_Monitor_shared_methods as ACI_sh_meth
import ACI_Monitor_Site as ACI_Site


def solver_d(data_a, data_b, ret=False):
    relations = []
    cont_a_details = []
    cont_b_details = []
    if data_a[-1] == site_a.url:
        for ten in site_a.contract_to_epg:
            if ten == data_a[0][4]:
                for epg in site_a.contract_to_epg[ten]:
                    if epg == data_a[0][6]:
                        data_a.append(site_a.contract_to_epg[ten][epg])
    elif data_a[-1] == site_b.url:
        for ten in site_b.contract_to_epg:
            if ten == data_a[0][4]:
                for epg in site_b.contract_to_epg[ten]:
                    if epg == data_a[0][6]:
                        data_a.append(site_b.contract_to_epg[ten][epg])
    if data_b[-1] == site_a.url:
        for ten in site_a.contract_to_epg:
            if ten == data_b[0][4]:
                for epg in site_a.contract_to_epg[ten]:
                    if epg == data_b[0][6]:
                        data_b.append(site_a.contract_to_epg[ten][epg])
    elif data_b[-1] == site_b.url:
        for ten in site_b.contract_to_epg:
            if ten == data_b[0][4]:
                for epg in site_b.contract_to_epg[ten]:
                    if epg == data_b[0][6]:
                        data_b.append(site_b.contract_to_epg[ten][epg])
    sh_contr_a_b = list(set(data_a[-1]['prov']).intersection(data_b[-1]['cons']))
    sh_contr_b_a = list(set(data_b[-1]['prov']).intersection(data_a[-1]['cons']))
    if not sh_contr_a_b and not sh_contr_b_a:
        if ret:
            return False
        print('No OPEN ports between hosts')
        return
    for cont in sh_contr_a_b:
        relations.append((data_a[0][1], cont, data_b[0][1]))
        tmp_list = site_a.disp_contract_details(cont, data_return=True)
        for entry in tmp_list:
            if entry not in cont_a_details:
                cont_a_details.append(entry)
    for cont in sh_contr_b_a:
        relations.append((data_b[0][1], cont, data_a[0][1]))
        tmp_list = site_a.disp_contract_details(cont, data_return=True)
        for entry in tmp_list:
            if entry not in cont_b_details:
                cont_b_details.append(entry)
    cont_a_details = sorted(cont_a_details, key=lambda xx: (xx[-1], xx[3]))
    if ret:
        return cont_a_details
    cont_b_details = sorted(cont_b_details, key=lambda xx: (xx[-1], xx[3]))
    print()
    ACI_sh_meth.formatted_print(relations, ("Provider", "Contract", "Consumer"))
    print(f"\nExplicitly configured ports in direction from {data_a[0][1]} to {data_b[0][1]}")
    ACI_sh_meth.formatted_print(cont_a_details, ("Tenant", "Contract", "Filter", "EtherT", "Protocol", "Src port-range",
                                                 "Dst port-range", "Action"))
    print(f"\nExplicitly configured ports in direction from {data_b[0][1]} to {data_a[0][1]}")
    ACI_sh_meth.formatted_print(cont_b_details, ("Tenant", "Contract", "Filter", "EtherT", "Protocol", "Src port-range",
                                                 "Dst port-range", "Action"))


def solver_e(data_ip_source, data_ip_destination, data_protocol, destination_port):
    allowed_ports = solver_d(data_ip_source, data_ip_destination, ret=True)
    hit_rule = []
    tup_src = [(data_ip_source[1], data_ip_source[0][4], data_ip_source[0][5], data_ip_source[0][6],
                data_ip_source[0][3], data_ip_source[0][2])]
    tup_dst = [(data_ip_destination[1], data_ip_destination[0][4], data_ip_destination[0][5], data_ip_destination[0][6],
                data_ip_destination[0][3], data_ip_destination[0][2])]
    print('\nFlow details:\nSOURCE: ')
    ACI_sh_meth.formatted_print(tup_src, ("Site", "Tenant", "App", "EPG", "Vlan", "Port"))
    print('\nDESTINATION: ')
    ACI_sh_meth.formatted_print(tup_dst, ("Site", "Tenant", "App", "EPG", "Vlan", "Port"))
    for port in allowed_ports:
        curr_prot = port[4]
        # curr_src_port_a, curr_src_port_b = map(str, (port[4].split(' - ')))
        # curr_action = port[6]
        if curr_prot == 'icmp' and data_protocol == 'icmp':
            hit_rule = [port]
            break
        curr_dst_port_a, curr_dst_port_b = map(str, (port[6].split(' - ')))
        if curr_dst_port_a.lower() in ACI_known_ports:
            curr_dst_port_a = ACI_known_ports[curr_dst_port_a]
        if curr_dst_port_b.lower() in ACI_known_ports:
            curr_dst_port_b = ACI_known_ports[curr_dst_port_b]
        if curr_dst_port_a == 'unspecified':
            curr_dst_port_a = 0
        if curr_dst_port_b == 'unspecified':
            curr_dst_port_b = 65535
        curr_dst_port_a = int(curr_dst_port_a)
        curr_dst_port_b = int(curr_dst_port_b)
        if data_protocol == curr_prot and curr_dst_port_a <= destination_port <= curr_dst_port_b:
            hit_rule = [port]
            break
    if not hit_rule:
        print('\nNo rule found - flow is implicitly denied.')
    else:
        print(f"\nFlow matched below rule:")
        ACI_sh_meth.formatted_print(hit_rule, ("Tenant", "Contract", "Filter", "EtherT", "Protocol", "Src port-range",
                                               "Dst port-range", "Action"))


if __name__ == '__main__':
    apic_url_list = ['https://192.168.10.1', 'https://192.168.10.2']
    mso_url_list = ['https://192.168.10.3']
    tenants_to_skip = ['infra', 'common', 'mgmt']
    allowed_protocols = ['tcp', 'udp', 'icmp']
    active_sessions = []
    ACI_known_ports = {
        'https': 443,
        'smtp': 25,
        'http': 80,
        'ftp-data': 20,
        'dns': 53,
        'pop3': 110,
        'rtsp': 554,
        'ssh': 22
    }
    print('Configured APICs: ', ', '.join(apic_url_list))
    for url_apic in apic_url_list:
        session = ACI_sh_meth.est_session(url_apic)
        if session:
            print(f"ACIToolKit session to: {url_apic}, OK")
            active_sessions.append((url_apic, session))
        else:
            print(f"ACIToolKit session to: {url_apic}, FAILED")
    site_a = ACI_Site.RESTSite(apic_url_list[0], 'Site_A')
    site_b = ACI_Site.RESTSite(apic_url_list[1], 'Site_B')
    if site_a.session.status:
        print(f'REST session to: {site_a.url}, OK')
    else:
        print(f'REST session to: {site_a.url}, FAILED')
    if site_b.session.status:
        print(f'REST session to: {site_b.url}, OK')
    else:
        print(f'REST session to: {site_b.url}, FAILED')
    while True:
        print('\n1: Show NODEs')
        print('2: Show EPGs')
        print('3: Show EPG details')
        print('4: Show CONTRACTs')
        print('5: Show CONTRACT details')
        print('6: Show ENDPOINTs')
        print('a: Find HOST')
        print('b: Find NODE')
        print('c: Find IP')
        print('d: Check all open ports between two endpoints')
        print('e: Check flow between SRC and DST hosts')
        print('Quit - q/Q')
        choice = str(input('\nEnter Menu Item: '))
        if choice == '1':
            for session in active_sessions:
                print(f'\nData from: {session[0]}')
                ACI_sh_meth.disp_nodes(session[1])
        elif choice == '2':
            for session in active_sessions:
                print(f'\nData from: {session[0]}')
                ACI_sh_meth.disp_epgs(session[1], tenants_to_skip)
        elif choice == '3':
            epg_to_find = input('\nEnter the name of EPG you would like to find: ')
            print(f'\nData for "{epg_to_find}" contract from: {site_a.url}\n')
            site_a.disp_epg_details(epg_to_find)
            print(f'\nData for "{epg_to_find}" contract from: {site_a.url}\n')
            site_b.disp_epg_details(epg_to_find)
        elif choice == '4':
            for session in active_sessions:
                print(f'\nData from: {session[0]}')
                ACI_sh_meth.disp_contracts(session[1], tenants_to_skip)
        elif choice == '5':
            contract_to_find = input('\nEnter the name of contract you would like to find: ')
            print(f'\nData for "{contract_to_find}" contract from: {site_a.url}\n')
            site_a.disp_contract_details(contract_to_find)
            print(f'\nData for "{contract_to_find}" contract from: {site_b.url}\n')
            site_b.disp_contract_details(contract_to_find)
        elif choice == '6':
            for session in active_sessions:
                print(f'\nData from: {session[0]}')
                ACI_sh_meth.disp_endpoints(session[1])
        elif choice.lower() == 'a':
            print('The following items are searchable:\n')
            print('MACADDRESS         IPADDRESS        INTERFACE       ENCAP       TENANT    APP PROFILE  EPG')
            print('-----------------  ---------------  --------------  ----------  --------  -----------  ---')
            print('00:50:56:95:4A:38  192.168.77.77    eth 1/101/1/3   vlan-116    Tenant-1  Lab_App-1    DB-1')
            search_str_host = input('\nEnter the string you would like to find (partial is ok): ').lower()
            for session in active_sessions:
                print(f'\nData from: {session[0]}')
                ACI_sh_meth.find_host(session[1], search_str_host)
        elif choice.lower() == 'b':
            print('The following items are searchable:\n')
            print('Serial Number   Role        Node   Health   Model        OOB Address   TEP Address')
            print('--------------  ----------  -----  -------  -----------  ------------  ------------ ')
            print('TEP-1-101       leaf        101    None     N9K-C9396PX  0.0.0.0       10.0.248.0 ')
            search_str_node = input('\nEnter the string you would like to find (partial is ok): ').lower()
            for session in active_sessions:
                print(f'\nData from: {session[0]}')
                ACI_sh_meth.find_node(session[1], search_str_node)
        elif choice.lower() == 'c':
            search_str_ip = ACI_sh_meth.ip_checker('Enter the IP you would like to find: ')
            if not search_str_ip:
                continue
            for session in active_sessions:
                print(f'\nData from: {session[0]}')
                ACI_sh_meth.find_ip(session[1], search_str_ip)
        elif choice.lower() == 'd':
            search_ip_a = ACI_sh_meth.ip_checker('Enter first IP address: ')
            if not search_ip_a:
                continue
            search_ip_b = ACI_sh_meth.ip_checker('Enter second IP address: ')
            if not search_ip_b:
                continue
            data_ip_a = []
            data_ip_b = []
            for session in active_sessions:
                if not data_ip_a:
                    data_ip_a = ACI_sh_meth.find_host(session[1], search_ip_a, ret=True)
                    if data_ip_a:
                        data_ip_a.append(session[0])
                if not data_ip_b:
                    data_ip_b = ACI_sh_meth.find_host(session[1], search_ip_b, ret=True)
                    if data_ip_b:
                        data_ip_b.append(session[0])
            if not data_ip_a:
                print('First IP address not found...')
                continue
            if not data_ip_b:
                print('Second IP address not found...')
                continue
            if data_ip_a[0][4] != data_ip_b[0][4]:
                print('\nProvided hosts belong to different Tenants!')
                print(f'First IP {search_ip_a}, Tenant: {data_ip_a[0][4]}, controller: {data_ip_a[1]}')
                print(f'Second IP {search_ip_b}, Tenant: {data_ip_b[0][4]}, controller: {data_ip_b[1]}')
                continue
            if data_ip_a[0][6] == data_ip_b[0][6]:
                print(f'\nProvided hosts are part of the same EPG: {data_ip_b[0][6]}')
                continue
            solver_d(data_ip_a, data_ip_b)
        elif choice.lower() == 'e':
            search_ip_src = ACI_sh_meth.ip_checker('Enter SOURCE IP address: ')
            if not search_ip_src:
                continue
            search_ip_dst = ACI_sh_meth.ip_checker('Enter DESTINATION IP address: ')
            if not search_ip_dst:
                continue
            data_ip_src = []
            data_ip_dst = []
            for session in active_sessions:
                if not data_ip_src:
                    data_ip_src = ACI_sh_meth.find_host(session[1], search_ip_src, ret=True)
                    if data_ip_src:
                        data_ip_src.append(session[0])
                if not data_ip_dst:
                    data_ip_dst = ACI_sh_meth.find_host(session[1], search_ip_dst, ret=True)
                    if data_ip_dst:
                        data_ip_dst.append(session[0])
            if not data_ip_src:
                print('First IP address not found...')
                continue
            if not data_ip_dst:
                print('Second IP address not found...')
                continue
            if data_ip_src[0][4] != data_ip_dst[0][4]:
                print('\nProvided hosts belong to different Tenants!')
                print(f'First IP {search_ip_src}, Tenant: {data_ip_src[0][4]}, controller: {data_ip_src[1]}')
                print(f'Second IP {search_ip_dst}, Tenant: {data_ip_dst[0][4]}, controller: {data_ip_dst[1]}')
                continue
            if data_ip_src[0][6] == data_ip_dst[0][6]:
                print(f'\nProvided hosts are part of the same EPG: {data_ip_dst[0][6]}')
                continue
            while True:
                protocol = input(f'Enter protocol {allowed_protocols}: ').lower()
                if protocol in allowed_protocols:
                    break
                else:
                    continue
            dst_port = 'unspecified'
            if protocol != 'icmp':
                while True:
                    try:
                        dst_port = int(input(f'Enter DESTINATION port number: '))
                        if dst_port < 0 or dst_port > 65535:
                            print('Wrong port number... 0 - 65535 allowed')
                            continue
                        else:
                            break
                    except ValueError:
                        print('Wrong format. Provide correct port number.')
                        continue
            solver_e(data_ip_src, data_ip_dst, protocol, dst_port)
        elif choice.lower() == 'q':
            print('Bye!')
            exit(0)
        else:
            continue

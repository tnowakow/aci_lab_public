import cobra.mit.access
import cobra.mit.request
import cobra.mit.session
import cobra.model.fv
import cobra.model.pol
import cobra.model.fabric
import cobra.model.infra
import cobra.model.cdp
import cobra.model.lldp
import cobra.model.lacp
import cobra.model.fvns
import cobra.model.phys
import cobra.model.pol
import cobra.model.vz
import requests
import urllib3
import ErrorHandler
import logging
from logging import config


class SDKsession:
    """ Class that includes general Cobra SDK session related variables (url to APIC, password, username),
    basic APIC variables: Policy Universe - polUni, MoDirectory - moDir, Infrastructure - infraInfra and functions.
    """

    # disable warnings
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    config_dict = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'consoleFormatter': {
                'format': '%(asctime)s - %(module)15s - %(levelname)-8s - %(name)-12s - %(message)s',
            },
            'fileFormatter': {
                'format': '%(asctime)s - %(module)15s - %(levelname)-8s - %(name)-12s - %(message)s',
            },
        },
        'handlers': {
            'file': {
                'filename': 'logs/ACIsdk.log',
                'level': 'DEBUG',
                'class': 'logging.handlers.TimedRotatingFileHandler',
                'formatter': 'fileFormatter',
                'when': 'midnight',
                'backupCount': 7
            },
            'console': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'formatter': 'consoleFormatter',
            },
        },
        'loggers': {
            '': {
                'handlers': ['file', 'console'],
                'level': 'DEBUG',
            },
        },
    }

    def __init__(self, login, password, url):
        logging.config.dictConfig(SDKsession.config_dict)
        self.log = logging.getLogger(__name__)
        self.login = login
        self.password = password
        self.url = url
        self.moDir = self.login_to_ACI_MoDirectory()
        self.polUni = cobra.model.pol.Uni('')
        self.infraInfra = cobra.model.infra.Infra(self.polUni)

    def logout(self):
        """ Logs out from APIC """

        self.moDir.logout()
        self.log.info(f'OK - session to MoDirectory - down, {self.url}')
        self.log.info(' !!! --- SESSION - SDK - END --- !!! ')

    def login_to_ACI_MoDirectory(self):
        """ Function that establishes session to APIC.
        Raises proper error in case of any issues, returns the session otherwise.
        """

        auth = cobra.mit.session.LoginSession(self.url, self.login, self.password)
        session = cobra.mit.access.MoDirectory(auth)
        try:
            session.login()
        except requests.exceptions.HTTPError as e:
            raise ErrorHandler.HTTPError(log=self.log, msg=e)
        except (requests.exceptions.ConnectionError, TimeoutError) as e:
            raise ErrorHandler.SessionError(log=self.log, msg=e)
        except Exception as e:
            raise ErrorHandler.Error(log=self.log, msg=e)
        self.log.info(' !!! --- SESSION - SDK - START --- !!! ')
        self.log.info(f'OK - session to MoDirectory - up, {self.url}')
        return session

    def object_not_created(self, fun, params):
        """ Function that raises error and may terminate the session in case of failure in creating an object."""

        print(f"'{fun}' failed to create object with params", params)
        print('Press y/Y to contine (dependand objects will NOT be created) or any other character to escape')
        if str(input(': ')).lower() != 'y':
            raise ErrorHandler.SessionTerminatedByUserError(log=self.log, msg=params)

    def params_check(self, params, names_tuples):
        """ Function that checks if all necessary parameters are included in dictionary
        passed to a method or function as a parameter. Only parameters crucial for object creation are being checked.
        Takes params (list or dictionary) to be checked and list of tuples (index, name) as arguments.
        """

        for tup in names_tuples:
            tmp_var = params[tup[0]].get(tup[1], False)
            if not tmp_var:
                raise ErrorHandler.MissingArgumentError(log=self.log, msg=f'{tup[1]} missing in {params[tup[0]]}')

    def check_if_created(self, cls_name, name, fun, params, pre_check=False):
        """ Function that checks if object has been created successfully
        If pre_check is True - returns the outcome without logging or raising an error.
        Otherwise invokes 'object_not_created' function so session may be interrupted.
        """

        if cls_name == 'vzRsSubjFiltAtt':
            obj_check = self.moDir.lookupByClass(cls_name, propFilter=f'eq({cls_name}.tnVzFilterName, "{name}"')
        else:
            obj_check = self.moDir.lookupByClass(cls_name, propFilter=f'eq({cls_name}.name, "{name}"')
        obj_check = obj_check[0] if obj_check else None
        if pre_check:
            return obj_check
        if obj_check:
            self.log.info(f"OK - '{fun}' created object: {params}")
        else:
            self.log.error(f"ERROR - '{fun}' failed to create object: {params}")
            self.object_not_created(fun, params)

    def cobra_commit(self, realm):
        """ Function that commits (sends) configuration to APIC """

        c = cobra.mit.request.ConfigRequest()
        c.addMo(realm)
        self.moDir.commit(c)

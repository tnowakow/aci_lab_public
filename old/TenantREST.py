import argparse
import ErrorHandler
import DataHandler
import ObjectsTenantREST
from RESTapi import RESTaci


def create_tenant(name):
    """ Creates instance of Tenant class based on name passed as a argument.
    Creates Tenant object if it's not present in configuration.
    """

    tenant = ObjectsTenantREST.Tenant(name)
    if not tenant.exist:
        tenant.create()


def create_vrf(params):
    """ Creates instance of VRF class based on params (dictionary) passed as argument.
    Checks if all necessary parameters are present in passed 'params' argument (params_check).
    Creates VRF object if it's not present in configuration.
    """

    data.params_check(params, [(3, 'name')])
    vrf = ObjectsTenantREST.VRF(params[3])
    if not vrf.exist:
        vrf.create()


def create_bridge_domain(params):
    """ Creates instance of Bridge-Domain class based on params (dictionary) passed as argument.
    Checks if all necessary parameters are present in passed 'params' argument (params_check).
    Creates Bridge-Domain object if it's not present in configuration.
    Assigns VRF to Bridge-Domain if vrf is included in params.
    Creates subnets objects under Bridge-Domain if they are included in params.
    """

    data.params_check(params, [(4, 'arpFlood')])
    bd = ObjectsTenantREST.BD(params)
    if not bd.exist:
        bd.create()
    if bd.vrf:
        bd.assign_vrf()
    else:
        REST_session.log.error(f'ERROR - Missing VRF for BD - {params}')
    for subnet in params[4]['subnets']:
        bd.assign_subnet(subnet)


def create_filter(params):
    """ Creates instance of Filter class based on params (dictionary) passed as argument.
    Checks if all necessary parameters are present in passed 'params' argument (params_check).
    Creates Filter object if it's not present in configuration.
    Assigns Entries to Filter if entries are included in params.
    """

    data.params_check(params, [(3, 'name')])
    vzFilter = ObjectsTenantREST.Filter(params)
    if not vzFilter.exist:
        vzFilter.create()
    for entry in params[3]['entries']:
        vzFilter.assign_entry(entry)


def create_standard_contract(params):
    """ Creates instance of Contract class based on params (dictionary) passed as argument.
    Checks if all necessary parameters are present in passed 'params' argument (params_check).
    Creates Contract object if it's not present in configuration.
    Assigns Subjects to Contract if subjects are included in params.
    Assigns Filters to Subjects if filters are included in params.
    """

    data.params_check(params, [(3, 'name')])
    contract = ObjectsTenantREST.Contract(params[3])
    if not contract.exist:
        contract.create()
    for subject in params[3]['subjects']:
        contract.assign_subject(subject)
        for vzFilter in subject['filters']:
            contract.assign_filter(vzFilter)


def create_app_profile(params):
    """ Creates instance of Application Profile class based on params (dictionary) passed as argument.
    Checks if all necessary parameters are present in passed 'params' argument (params_check).
    Creates Application Profile object if it's not present in configuration.
    Assigns EPGs to Application Profile if EPGs are included in params.
    """

    data.params_check(params, [(4, 'bridge-domain')])
    app_profile = ObjectsTenantREST.AppProfile(params)
    if not app_profile.exist:
        app_profile.create()
    if app_profile.epg_present:
        app_profile.assign_epg(params[4])


def create_EPG_to_contract_relation(params):
    """ Assigns contracts to EPGs. EPG can either consume or provide contract.
    Params - includes dictionary with name of EPG and two lists with names of contracts - 'provides' and 'consumes'.
    """

    data.params_check(params, [(3, 'epg')])
    ObjectsTenantREST.EPGrelation(params[3])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", type=str, help="Name of file including input data")
    parser.add_argument("-u", "--url", type=str, help="APIC URL")
    args = parser.parse_args()
    if args.url:
        url = args.url
    else:
        url = 'https://192.168.10.2'
    if args.file:
        input_data_file = args.file
    else:
        input_data_file = '../InputFiles/RestTestTenantInputFile'
    REST_session = RESTaci('admin', 'ciscocisco', url)
    data = DataHandler.DataHandlerTenant(input_data_file, log=REST_session.log)
    ACI = ObjectsTenantREST.ACIobject(url, REST_session, 'pod-1')
    create_tenant(data.paths[0][0])

    # checks last item in a path which is a pointer to proper function
    # removes last item so all passed parameters are object related
    # invokes the function with 'path' list or dictionary as parameter

    for path in data.paths_sorted_by_priority:
        if path[-1] == 'create_vrf':
            del path[-1]
            create_vrf(path)
        elif path[-1] == 'create_bridge_domain':
            del path[-1]
            create_bridge_domain(path)
        elif path[-1] == 'create_filter':
            del path[-1]
            create_filter(path)
        elif path[-1] == 'create_standard_contract':
            del path[-1]
            create_standard_contract(path)
        elif path[-1] == 'create_app_profile':
            del path[-1]
            create_app_profile(path)
        elif path[-1] == 'create_EPG_to_contract_relation':
            del path[-1]
            create_EPG_to_contract_relation(path)
        else:
            raise ErrorHandler.MissingMethodError(log=REST_session.log, msg=path)
    REST_session.log.info(' !!! --- SESSION - REST - END --- !!! ')

import json
import ErrorHandler as Er
from random import randint


class ACIfabric:
    session = None
    base_API_url = ''
    url = ''
    dry = True
    dry_log = []
    ACI_objects_dict = {}
    ACI_errors = []
    pc_pol = ['vpc', 'channel', 'lacp', 'pagp', 'static']

    def __init__(self, url, session, dry=True):
        ACIfabric.session = session
        ACIfabric.base_API_url = url + '/api/node/mo/'
        ACIfabric.url = url
        ACIfabric.dry = dry

    @staticmethod
    def create(message, url, payload):
        if ACIfabric.dry:
            ACIfabric.dry_log.append(message)
        else:
            ACIfabric.session.log.info(message)
            ACIfabric.session.invoke(url, method='POST', data=json.dumps(payload))

    @staticmethod
    def generate_random_name():
        out = ''
        for z in range(16):
            n = randint(0, 1)
            if n:
                out += chr(randint(97, 102))
                continue
            out += chr(randint(48, 57))
        return out


class VPCdomain:

    def __init__(self, params):
        self.params = params[1]
        self.name = self.params['name']
        self.rn = f'expgep-{self.name}'
        self.dn = f"uni/fabric/protpol/{self.rn}"
        self.children = self.fill_children_list()
        self.url = ACIfabric.base_API_url + self.dn + '.json'
        self.exist = True if int(ACIfabric.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIfabric.session.log.info(f'Checking VPCdomain object - {self.name} - EXIST - {self.exist}.')
        self.payload = {
            "fabricExplicitGEp": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "id": self.params['vpc_id'],
                    "rn": self.rn
                },
                "children": self.children
            }
        }

    def fill_children_list(self):
        children = []
        for param in self.params:
            if 'sw' in param.lower():
                children.append({
                    "fabricNodePEp": {
                        "attributes": {
                            "dn": f"{self.dn}/nodepep-{self.params[param]}",
                            "id": self.params[param],
                            "rn": f"nodepep-{self.params[param]}"
                        },
                        "children": []
                    }
                })
        return children

    def create(self):
        ACIfabric.create(f'Creating VPCdomain: {self.name}', self.url, self.payload)


class IntPol:

    def __init__(self, params):
        self.params = params
        self.name = self.params['name']
        self.children = []
        self.url = ''
        self.payload = {}

    def create(self):
        ACIfabric.create(f'Creating IntPol: {self.name}', self.url, self.payload)


class IntPolCDP(IntPol):

    def __init__(self, params):
        super().__init__(params)
        self.rn = f'cdpIfP-{self.name}'
        self.dn = f"uni/infra/{self.rn}"
        self.url = ACIfabric.base_API_url + self.dn + '.json'
        self.exist = True if int(ACIfabric.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIfabric.session.log.info(f'Checking IntPolCDP object - {self.name} - EXIST - {self.exist}.')
        self.payload = {
            "cdpIfPol": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "adminSt": params.get("adminSt", "enabled"),
                    "rn": self.rn
                },
                "children": self.children
            }
        }


class IntPolLLDP(IntPol):

    def __init__(self, params):
        super().__init__(params)
        self.rn = f'lldpIfP-{self.name}'
        self.dn = f"uni/infra/{self.rn}"
        self.url = ACIfabric.base_API_url + self.dn + '.json'
        self.exist = True if int(ACIfabric.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIfabric.session.log.info(f'Checking IntPolLLDP object - {self.name} - EXIST - {self.exist}.')
        self.payload = {
            "lldpIfPol": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "adminRxSt": params.get("adminRxSt", "enabled"),
                    "adminTxSt": params.get("adminTxSt", "enabled"),
                    "rn": self.rn
                },
                "children": self.children
            }
        }


class IntPolPC(IntPol):

    def __init__(self, params):
        super().__init__(params)
        self.rn = f'lacplagp-{self.name}'
        self.dn = f"uni/infra/{self.rn}"
        self.url = ACIfabric.base_API_url + self.dn + '.json'
        self.exist = True if int(ACIfabric.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIfabric.session.log.info(f'Checking IntPolPC object - {self.name} - EXIST - {self.exist}.')
        self.payload = {
            "lacpLagPol": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "mode": params.get("mode", False),
                    "minLinks": params.get("minLinks", "1"),
                    "rn": self.rn
                },
                "children": self.children
            }
        }
        if not self.payload["lacpLagPol"]["attributes"]["mode"]:
            del self.payload["lacpLagPol"]["attributes"]["mode"]


class GlobPol:

    def __init__(self, params):
        self.params = params
        self.name = self.params['name']
        self.rn = f'attentp-{self.name}'
        self.dn = f"uni/infra/{self.rn}"
        self.children = []
        self.url = ACIfabric.base_API_url + self.dn + '.json'
        self.exist = True if int(ACIfabric.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIfabric.session.log.info(f'Checking GlobPol object - {self.name} - EXIST - {self.exist}.')
        self.payload = {
            "infraAttEntityP": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "rn": self.rn
                },
                "children": self.children
            }
        }

    def create(self):
        ACIfabric.create(f'Creating GlobPol: {self.name}', self.url, self.payload)

    def attach_phys_domain(self):
        if 'domain' not in self.params:
            ACIfabric.session.log.error(f'Domain is missing in params for AAEP: {self.name}.')
            ACIfabric.ACI_errors.append(f'Domain is missing in params for AAEP: {self.name}.')
        else:
            domain_name = self.params['domain']
            attached = False
            phys_dom_payload = {
                "infraRsDomP": {
                    "attributes": {
                        "tDn": f"uni/phys-{domain_name}"
                    },
                    "children": []
                }
            }
            attached_domains = ACIfabric.session.invoke(self.url + '?query-target=children&target-subtree-class='
                                                                   'infraRsDomP', method='GET')['imdata']
            for dom in attached_domains:
                if dom['infraRsDomP']['attributes']['tDn'] == phys_dom_payload['infraRsDomP']['attributes']['tDn']:
                    attached = True
                    break
            if attached:
                ACIfabric.session.log.info(f'Domain: {domain_name}, already attached to AAEP: {self.name}.')
            else:
                ACIfabric.create(f'Attaching PhysDom: {domain_name} to AAEP: {self.name}', self.url, phys_dom_payload)


class VlanPool:

    def __init__(self, params):
        self.params = params
        self.mode = params.get('allocMode', 'static')
        self.name = self.params['name']
        if ACIfabric.dry:
            ACIfabric.ACI_objects_dict[self.name] = [self.mode]
        self.rn = f'vlanns-[{self.name}]-{self.mode}'
        self.dn = f"uni/infra/{self.rn}"
        self.children = params.get('vlans', [])
        self.url = ACIfabric.base_API_url + self.dn + '.json'
        self.exist = True if int(ACIfabric.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIfabric.session.log.info(f'Checking Pool object - {self.name} - EXIST - {self.exist}.')
        if not self.exist:
            self.payload = {
                "fvnsVlanInstP": {
                    "attributes": {
                        "dn": self.dn,
                        "name": self.name,
                        "allocMode": self.mode,
                        "rn": self.rn
                    },
                    "children": []
                }
            }

    def create(self):
        ACIfabric.create(f'Creating Vlan Pool: {self.name} ({self.mode})', self.url, self.payload)

    def assign_pool(self, vlan_payload):
        vlan_rn = vlan_payload["fvnsEncapBlk"]["attributes"]["rn"]
        url = ACIfabric.base_API_url + self.dn + f'/{vlan_rn}.json'
        ACIfabric.create(f'Assigning Vlan Block: {vlan_rn}, to pool: {self.name}', url, vlan_payload)

    def fill_children_list(self):
        curr_children = ACIfabric.session.invoke(f'{ACIfabric.base_API_url + self.dn}.json?query-target='
                                                 f'children&target-subtree-class=fvnsEncapBlk', method='GET')
        for child_input in self.children:
            valid_pool = True
            child_input_vars_list = child_input.split('-')
            vl_from_input = int(child_input_vars_list[0])
            vl_to_input = int(child_input_vars_list[1])
            if len(child_input_vars_list) == 2:
                vlan_alloc = self.mode
            else:
                vlan_alloc = child_input_vars_list[2]
                if vlan_alloc == 'dynamic' and self.mode == 'static':
                    raise Er.InvalidVlanMode(log=ACIfabric.session.log,
                                             msg=f'DYNAMIC BLOCK not allowed under STATIC POOL')
            if vl_from_input > vl_to_input:
                vl_from_input, vl_to_input = vl_to_input, vl_from_input
            for curr_child in curr_children['imdata']:
                vl_from_curr = int(curr_child['fvnsEncapBlk']['attributes']['from'].split('-')[-1])
                vl_to_curr = int(curr_child['fvnsEncapBlk']['attributes']['to'].split('-')[-1])
                if (vl_from_input < vl_from_curr and vl_to_input < vl_from_curr) or \
                        (vl_from_input > vl_to_curr and vl_to_input > vl_to_curr):
                    continue
                else:
                    valid_pool = False
                    ACIfabric.session.log.error(f'Invalid vlan block: {vl_from_input}-{vl_to_input} - '
                                                f'overlaps with existing one - skipping')
                    ACIfabric.ACI_errors.append(f'Invalid vlan block: {vl_from_input}-{vl_to_input} - '
                                                f'overlaps with existing one - skipping')
                    break
            if valid_pool:
                vlan_payload = {
                    "fvnsEncapBlk": {
                        "attributes": {
                            "dn": f"{self.dn}/from-[vlan-{vl_from_input}]-to-[vlan-{vl_to_input}]",
                            "from": f"vlan-{vl_from_input}",
                            "to": f"vlan-{vl_to_input}",
                            "allocMode": vlan_alloc,
                            "rn": f"from-[vlan-{vl_from_input}]-to-[vlan-{vl_to_input}]"
                        },
                        "children": []
                    }
                }
                self.assign_pool(vlan_payload)


class Domain:

    def __init__(self, params):
        self.params = params
        self.name = self.params['name']
        self.children = []
        self.url = ''
        self.payload = {}

    def create(self):
        ACIfabric.create(f'Creating Domain: {self.name}', self.url, self.payload)

    @staticmethod
    def check_vlan_pool():
        existing_vlan_pool_query_url = f'{ACIfabric.url}/api/node/class/fvnsVlanInstP.json'
        existing_pools = ACIfabric.session.invoke(existing_vlan_pool_query_url, method='GET')
        if int(existing_pools['totalCount']) > 0:
            for pool in existing_pools['imdata']:
                ACIfabric.ACI_objects_dict[pool['fvnsVlanInstP']['attributes']['name']] = \
                    pool['fvnsVlanInstP']['attributes']['allocMode']


class PhysDomain(Domain):

    def __init__(self, params):
        super().__init__(params)
        self.rn = f'phys-{self.name}'
        self.dn = f"uni/{self.rn}"
        self.url = ACIfabric.base_API_url + self.dn + '.json'
        self.exist = True if int(ACIfabric.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIfabric.session.log.info(f'Checking PhysDomain object - {self.name} - EXIST - {self.exist}.')
        self.vlan_pool = False
        self.payload = {
            "physDomP": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "rn": self.rn
                },
                "children": self.children
            }
        }
        if 'vlan_pool' in params:
            self.vlan_pool_name = params['vlan_pool']
            self.check_vlan_pool()
            if self.vlan_pool_name not in ACIfabric.ACI_objects_dict:
                raise Er.NonExistingVlanPool(log=ACIfabric.session.log, msg=self.vlan_pool_name)
            else:
                self.vlan_pool_mode = ACIfabric.ACI_objects_dict[self.vlan_pool_name]
            self.vlan_pool_payload = {
                "infraRsVlanNs": {
                    "attributes": {
                        "tDn": f"uni/infra/vlanns-[{self.vlan_pool_name}]-{self.vlan_pool_mode}"
                    },
                    "children": []
                }
            }
            self.vlan_pool = True

    def assign_pool(self):
        if self.vlan_pool:
            existing_pool = ACIfabric.session.invoke(ACIfabric.base_API_url + self.dn + '/rsvlanNs.json', method='GET')
            if int(existing_pool['totalCount']) > 0:
                existing_tdn = existing_pool['imdata'][0]['infraRsVlanNs']['attributes']['tDn']
                if existing_tdn != self.vlan_pool_payload["infraRsVlanNs"]["attributes"]["tDn"]:
                    raise Er.WrongVlanPool(log=ACIfabric.session.log,
                                           msg=f'Cannot assign {self.vlan_pool_name} as there is a different pool'
                                               f' {existing_tdn} already assigned to {self.name} domain')
                else:
                    ACIfabric.session.log.info(f'Vlan pool: {self.vlan_pool_name}, already assigned to: {self.name}')
            else:
                ACIfabric.create(f'Assigning vlan pool: {self.vlan_pool_name}, to PhysDomain: {self.name}',
                                 ACIfabric.base_API_url + self.dn + '/rsvlanNs.json', self.vlan_pool_payload)
        else:
            ACIfabric.session.log.info(f'Cannot assign vlan pool to: {self.name} domain, pool missing in input data.')


class LeafIntPolGrp:

    def __init__(self, params):
        self.params = params
        self.name = self.params['name']
        self.children = []
        self.url = ''
        self.payload = {}
        self.policies = params.get('policies', [])
        self.aaep = params.get('aaep', False)
        self.exist = False
        self.dn = ''

    def create(self):
        ACIfabric.create(f'Creating LeafIntPolGrp: {self.name}', self.url, self.payload)
        self.attach_aaep()
        self.attach_policies()

    def attach_aaep(self):
        if not self.aaep:
            ACIfabric.session.log.info(f'No AAEP specified in input data for: {self.name}.')
            return
        if self.exist:
            conf_aaep = ACIfabric.session.invoke(ACIfabric.base_API_url + self.dn + '/rsattEntP.json',
                                                 method='GET')['imdata']
            if conf_aaep:
                current_aaep = conf_aaep[0].get('infraRsAttEntP', {}).get('attributes', {}).get('tDn', False)
                if current_aaep and current_aaep.startswith('uni/infra/attentp-'):
                    current_aaep = current_aaep[18:]
                else:
                    raise Er.WrongAAEP(log=ACIfabric.session.log, msg=f'AAEP in input data: {self.aaep} is different'
                                                                      f' than configured: {current_aaep}')
            else:
                current_aaep = None
        else:
            current_aaep = None
        if current_aaep:
            if current_aaep != self.aaep:
                raise Er.WrongAAEP(log=ACIfabric.session.log, msg=f'AAEP in input data: {self.aaep} '
                                                                  f'is different than configured: {current_aaep}')
            else:
                ACIfabric.session.log.info(f'Proper AAEP: {self.aaep}, already attached.')
        else:
            aaep_payload = {
                "infraRsAttEntP": {
                    "attributes": {
                        "tDn": f"uni/infra/attentp-{self.aaep}"
                    }
                }
            }
            ACIfabric.create(f'Attaching AAEP: {self.aaep} to IntPolGrp: {self.name}',
                             ACIfabric.base_API_url + self.dn + '/rsattEntP.json', aaep_payload)

    def attach_policies(self):
        for policy in self.policies:
            if ACIfabric.dry and not self.exist:
                ACIfabric.create(f'Attaching Policy: {policy} to IntPolGrp: {self.name}', None, None)
                continue
            if 'cdp' in policy.lower():
                cp = ACIfabric.session.invoke(ACIfabric.base_API_url + self.dn + '/rscdpIfPol.json',
                                              method='GET')['imdata']
                cp = cp[0].get('infraRsCdpIfPol', {}).get('attributes', {}).get('tnCdpIfPolName', False)
                if cp:
                    if cp == policy:
                        ACIfabric.session.log.info(f'{self.name} - Policy: {policy} already attached.')
                        continue
                    else:
                        ACIfabric.session.log.info(f'{self.name} - replacing Policy: {cp} with {policy}')
                        if ACIfabric.dry:
                            ACIfabric.dry_log.append(f'{self.name} - replacing Policy: {cp} with {policy}')
                policy_payload = {
                    "infraRsCdpIfPol": {
                        "attributes": {
                            "tnCdpIfPolName": policy
                        }
                    }
                }
                ACIfabric.create(f'Attaching Policy: {policy} to IntPolGrp: {self.name}',
                                 ACIfabric.base_API_url + self.dn + '/rscdpIfPol.json', policy_payload)
            elif 'lldp' in policy.lower():
                cp = ACIfabric.session.invoke(ACIfabric.base_API_url + self.dn + '/rslldpIfPol.json',
                                              method='GET')['imdata']
                cp = cp[0].get('infraRsLldpIfPol', {}).get('attributes', {}).get('tnLldpIfPolName', False)
                if cp:
                    if cp == policy:
                        ACIfabric.session.log.info(f'{self.name} - Policy: {policy} already attached.')
                        continue
                    else:
                        ACIfabric.session.log.info(f'{self.name} - replacing Policy: {cp} with {policy}')
                        if ACIfabric.dry:
                            ACIfabric.dry_log.append(f'{self.name} - replacing Policy: {cp} with {policy}')
                policy_payload = {
                    "infraRsLldpIfPol": {
                        "attributes": {
                            "tnLldpIfPolName": policy
                        }
                    }
                }
                ACIfabric.create(f'Attaching Policy: {policy} to IntPolGrp: {self.name}',
                                 ACIfabric.base_API_url + self.dn + '/rslldpIfPol.json', policy_payload)
            elif any([pol in policy.lower() for pol in ACIfabric.pc_pol]):
                cp = ACIfabric.session.invoke(ACIfabric.base_API_url + self.dn + '/rslacpPol.json',
                                              method='GET')['imdata']
                cp = cp[0].get('infraRsLacpPol', {}).get('attributes', {}).get('tnLacpLagPolName', False)
                if cp:
                    if cp == policy:
                        ACIfabric.session.log.info(f'{self.name} - Policy: {policy} already attached.')
                        continue
                    else:
                        ACIfabric.session.log.info(f'{self.name} - replacing Policy: {cp} with {policy}')
                        if ACIfabric.dry:
                            ACIfabric.dry_log.append(f'{self.name} - replacing Policy: {cp} with {policy}')
                policy_payload = {
                    "infraRsLacpPol": {
                        "attributes": {
                            "tnLacpLagPolName": policy
                        }
                    }
                }
                ACIfabric.create(f'Attaching Policy: {policy} to IntPolGrp: {self.name}',
                                 ACIfabric.base_API_url + self.dn + '/rslacpPol.json', policy_payload)
            else:
                raise Er.UnknownPolType(log=ACIfabric.session.log, msg=f'{policy}')


class LeafAccessIntPolGrp(LeafIntPolGrp):

    def __init__(self, params):
        super().__init__(params)
        self.rn = f'accportgrp-{self.name}'
        self.dn = f"uni/infra/funcprof/{self.rn}"
        self.url = ACIfabric.base_API_url + self.dn + '.json'
        self.exist = True if int(ACIfabric.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIfabric.session.log.info(f'Checking LeafAccessIntPolGrp object - {self.name} - EXIST - {self.exist}.')
        self.payload = {
            "infraAccPortGrp": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "rn": self.rn
                },
                "children": self.children
            }
        }
        if self.exist:
            self.attach_aaep()
            self.attach_policies()


class LeafVPCIntPolGrp(LeafIntPolGrp):

    def __init__(self, params):
        super().__init__(params)
        self.rn = f'accbundle-{self.name}'
        self.dn = f"uni/infra/funcprof/{self.rn}"
        self.url = ACIfabric.base_API_url + self.dn + '.json'
        self.pc_policy = params.get('port-channel policy', None)
        self.policies.append(self.pc_policy)
        self.exist = True if int(ACIfabric.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIfabric.session.log.info(f'Checking LeafVPCIntPolGrp object - {self.name} - EXIST - {self.exist}.')
        self.payload = {
            "infraAccBndlGrp": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "lagT": self.params.get("lagT", "node"),
                    "rn": self.rn
                },
                "children": self.children
            }
        }
        if self.exist:
            self.attach_aaep()
            self.attach_policies()


class LeafIntProf:

    def __init__(self, params):
        self.params = params
        self.name = self.params[0]
        self.selector_name = self.params[1]['name']
        self.selector_rn = f'hports-{self.selector_name}-typ-range'
        self.policy_group = self.params[1].get('policy_group', None)
        self.rn = f'accportprof-{self.name}'
        self.dn = f"uni/infra/{self.rn}"
        self.selector_dn = f'{self.dn}/{self.selector_rn}'
        self.children = []
        self.url = ACIfabric.base_API_url + self.dn + '.json'
        self.selector_url = ACIfabric.base_API_url + self.dn + f'/{self.selector_rn}.json'
        self.exist = True if int(ACIfabric.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIfabric.session.log.info(f'Checking LeafIntProf object - {self.name} - EXIST - {self.exist}.')
        if self.exist:
            if int(ACIfabric.session.invoke(self.selector_url, method='GET')['totalCount']) > 0:
                self.check_policy()
            else:
                self.add_selector()
        self.payload = {
            "infraAccPortP": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "rn": self.rn
                },
                "children": self.children
            }
        }

    def create(self):
        ACIfabric.create(f'Creating LeafIntProf: {self.name}', self.url, self.payload)
        self.add_selector()

    def add_selector(self):
        block_to_port = self.params[1]['toPort']
        block_from_port = self.params[1].get('fromPort', block_to_port)
        block_children = self.params[1].get('children', [])
        block_name = self.params[1].get('block', ACIfabric.generate_random_name())
        selector_payload = {
            "infraHPortS": {
                "attributes": {
                    "dn": self.selector_dn,
                    "name": self.selector_name,
                    "rn": self.selector_rn,
                    "status": "created,modified"
                },
                "children": [
                    {
                        "infraPortBlk": {
                            "attributes": {
                                "dn": f"{self.selector_dn}/portblk-{block_name}",
                                "fromPort": block_from_port,
                                "toPort": block_to_port,
                                "name": block_name,
                                "rn": f"portblk-{block_name}",
                                "status": "created,modified"
                            },
                            "children": block_children
                        }
                    }
                ]
            }
        }
        ACIfabric.create(f'Interface Profile: {self.name}, adding Selector: {self.selector_name}', self.selector_url,
                         selector_payload)
        self.apply_policy()

    def apply_policy(self):
        if not self.policy_group:
            ACIfabric.session.log.info(f'No policy in input data for - Interface Profile: {self.name}, '
                                       f'Port Selector: {self.selector_name}')
            return
        if any([pol in self.policy_group.lower() for pol in ACIfabric.pc_pol]):
            policy_rn = f'accbundle-{self.policy_group}'
        else:
            policy_rn = f'accportgrp-{self.policy_group}'
        policy_url = ACIfabric.base_API_url + self.dn + f'/{self.selector_rn}/rsaccBaseGrp.json'
        policy_payload = {
            "infraRsAccBaseGrp": {
                "attributes": {
                    "tDn": f"uni/infra/funcprof/{policy_rn}"
                },
                "children": []
            }
        }
        ACIfabric.create(f'Interface Profile: {self.name}, Selector: {self.selector_name}, applying Policy: '
                         f'{self.policy_group}', policy_url, policy_payload)

    def check_policy(self):
        if not self.policy_group:
            ACIfabric.session.log.info(f'No policy in input data for - Interface Profile: {self.name}, '
                                       f'Port Selector: {self.selector_name}')
            return
        curr_pol = ACIfabric.session.invoke(ACIfabric.base_API_url + self.dn + f'/{self.selector_rn}/rsaccBaseGrp.json',
                                            method='GET')['imdata']
        if curr_pol:
            curr_pol = curr_pol[0].get('infraRsAccBaseGrp', {}).get('attributes', {}).get('tDn', False)
            if 'accbundle' in curr_pol:
                curr_pol = curr_pol[29:]
            elif 'accportgrp' in curr_pol:
                curr_pol = curr_pol[30:]
            if curr_pol == self.policy_group:
                ACIfabric.session.log.info(
                    f'Interface Profile: {self.name}, Port Selector: {self.selector_name}, Policy: {self.policy_group}'
                    f' already applied.')
            else:
                raise Er.WrongLeafIntPolicy(
                    log=ACIfabric.session.log, msg=f'Interface Profile: {self.name}, Port Selector: '
                                                   f'{self.selector_name}, Policy-input: {self.policy_group}, '
                                                   f'Policy-found: {curr_pol}')
        else:
            self.apply_policy()


class LeafSwitchPolGrp:

    def __init__(self, params):
        self.params = params
        self.name = self.params['name']
        self.rn = f'accnodepgrp-{self.name}'
        self.dn = f"uni/infra/funcprof/{self.rn}"
        self.children = []
        self.policies = params.get('policies', [])
        self.policies_dict = {}
        self.url = ACIfabric.base_API_url + self.dn + '.json'
        self.exist = True if int(ACIfabric.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        if self.exist:
            self.apply_policies()
        ACIfabric.session.log.info(f'Checking LeafSwitchPolGrp object - {self.name} - EXIST - {self.exist}.')
        self.payload = {
            "infraAccNodePGrp": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "rn": self.rn
                },
                "children": self.children
            }
        }

    def create(self):
        ACIfabric.create(f'Creating LeafSwitchPolGrp: {self.name}', self.url, self.payload)
        self.apply_policies()

    def apply_policies(self):
        self.check_policies()
        for policy in self.policies:
            if 'cdp' in policy.lower():
                if 'Cdp' in self.policies_dict:
                    if self.policies_dict['Cdp'] == policy:
                        ACIfabric.session.log.info(f'LeafSwitchPolGrp: {self.name}, CDP: {policy} already configured.')
                        continue
                    else:
                        ACIfabric.session.log.info(f'{self.name} - Replacing CDP policy: {self.policies_dict["Cdp"]}'
                                                   f' with {policy}')
                        if ACIfabric.dry:
                            ACIfabric.dry_log.append(f'{self.name} - Replacing CDP policy: {self.policies_dict["Cdp"]}'
                                                     f' with {policy}')
                cdp_payload = {
                    "infraRsLeafPGrpToCdpIfPol": {
                        "attributes": {
                            "tnCdpIfPolName": policy
                        },
                        "children": []
                    }
                }
                ACIfabric.create(f'Applying CDP policy: {policy} to LeafSwitchPolGrp: {self.name}',
                                 ACIfabric.base_API_url + self.dn + '/rsleafPGrpToCdpIfPol.json', cdp_payload)
            elif 'lldp' in policy.lower():
                if 'Lldp' in self.policies_dict:
                    if self.policies_dict['Lldp'] == policy:
                        ACIfabric.session.log.info(f'LeafSwitchPolGrp: {self.name}, LLDP: {policy} already configured.')
                        continue
                    else:
                        ACIfabric.session.log.info(f'{self.name} - Replacing LLDP policy: {self.policies_dict["Lldp"]}'
                                                   f' with {policy}')
                        if ACIfabric.dry:
                            ACIfabric.dry_log.append(f'{self.name} - Replacing LLDP policy: {self.policies_dict["Lldp"]}'
                                                   f' with {policy}')
                lldp_payload = {
                    "infraRsLeafPGrpToLldpIfPol": {
                        "attributes": {
                            "tnLldpIfPolName": policy
                        },
                        "children": []
                    }
                }
                ACIfabric.create(f'Applying LLDP policy: {policy} to LeafSwitchPolGrp: {self.name}',
                                 ACIfabric.base_API_url + self.dn + '/rsleafPGrpToLldpIfPol.json', lldp_payload)
            else:
                raise Er.UnknownPolType(log=ACIfabric.session.log, msg=f'{policy}')

    def check_policies(self):
        pol_list = ['Cdp', 'Lldp']
        for pol in pol_list:
            policy_url = ACIfabric.base_API_url + self.dn + f'/rsleafPGrpTo{pol}IfPol.json'
            curr_pol = ACIfabric.session.invoke(policy_url, method='GET')['imdata']
            if curr_pol:
                curr_pol = curr_pol[0].get(f'infraRsLeafPGrpTo{pol}IfPol', {}).get('attributes', {}). \
                    get(f'tn{pol}IfPolName', False)
                if curr_pol:
                    self.policies_dict[pol] = curr_pol


class LeafSwitchProf:

    def __init__(self, params):
        self.params = params
        self.name = self.params[0]
        self.rn = f'nprof-{self.name}'
        self.dn = f"uni/infra/{self.rn}"
        self.children = []
        self.int_profiles = self.params[1].get('int_profiles', [])
        self.url = ACIfabric.base_API_url + self.dn + '.json'
        self.exist = True if int(ACIfabric.session.invoke(self.url, method='GET')['totalCount']) > 0 else False
        ACIfabric.session.log.info(f'Checking LeafSwitchProf object - {self.name} - EXIST - {self.exist}.')
        self.payload = {
            "infraNodeP": {
                "attributes": {
                    "dn": self.dn,
                    "name": self.name,
                    "rn": self.rn
                },
                "children": self.children
            }
        }
        self.selector = False
        if 'name' in self.params[1] and 'selector' in self.params[1] and 'fromLeaf' in self.params[1]:
            self.selector = True
        self.profile_dict = {
            "selectors": [],
            "policies": [],
            "int_profs": []
        }
        if self.exist:
            self.fill_profile_dict()
            if self.selector:
                if self.params[1]['name'] in self.profile_dict['selectors']:
                    ACIfabric.session.log.info(f'LeafSwitchProf: {self.name}, Selector - {self.params[1]["name"]},'
                                               f' already configured.')
                else:
                    self.attach_selector()
            if self.int_profiles:
                self.attach_int_profile()

    def create(self):
        ACIfabric.create(f'Creating LeafSwitchProf: {self.name}', self.url, self.payload)
        if self.selector:
            self.attach_selector()
        if self.int_profiles:
            self.attach_int_profile()

    def fill_profile_dict(self):
        query = '?query-target=subtree&target-subtree-class=infraLeafS&target-subtree-class=infraRsAccPortP&target-' \
                'subtree-class=infraNodeBlk,infraRsAccNodePGrp&query-target=subtree'
        objects = ACIfabric.session.invoke(self.url + query, method='GET')
        for obj in objects['imdata']:
            sel = obj.get('infraLeafS', {}).get('attributes', {}).get('name', False)
            pol = obj.get('infraRsAccNodePGrp', {}).get('attributes', {}).get('tDn', False)
            int_prf = obj.get('infraRsAccPortP', {}).get('attributes', {}).get('tDn', False)
            if sel and sel not in self.profile_dict['selectors']:
                self.profile_dict['selectors'].append(sel)
            if pol and 'uni/infra/funcprof/accnodepgrp-' in pol and pol[31:] not in self.profile_dict['policies']:
                self.profile_dict['policies'].append(pol[31:])
            if int_prf and 'uni/infra/accportprof-' in int_prf and int_prf[22:] not in self.profile_dict['int_profs']:
                self.profile_dict['int_profs'].append(int_prf[22:])

    def attach_selector(self):
        sel_name = self.params[1]["name"]
        sel_blk = self.params[1].get("selector", ACIfabric.generate_random_name())
        sel_to = self.params[1].get("toLeaf", self.params[1]["fromLeaf"])
        selector_payload = {
            "infraLeafS": {
                "attributes": {
                    "dn": f"{self.dn}/leaves-{sel_name}-typ-range",
                    "type": self.params[1].get("type", "range"),
                    "name": sel_name,
                    "rn": f"leaves-Leaf-{sel_name}-typ-range"
                },
                "children": [
                    {
                        "infraNodeBlk": {
                            "attributes": {
                                "dn": f"{self.dn}/leaves-{sel_name}-typ-range/nodeblk-{sel_blk}",
                                "from_": self.params[1]["fromLeaf"],
                                "to_": sel_to,
                                "name": sel_blk,
                                "rn": f"nodeblk-{sel_blk}"
                            },
                            "children": self.params[1].get("children", [])
                        }
                    }
                ]
            }
        }
        url = ACIfabric.base_API_url + self.dn + f'/leaves-{sel_name}-typ-range.json'
        ACIfabric.create(f'LeafSwitchProf: {self.name}, attaching Selector: {sel_name}', url, selector_payload)
        self.apply_policy()

    def apply_policy(self):
        if 'policy_group' not in self.params[1]:
            ACIfabric.session.log.info(f'LeafSwitchProf: {self.name}, Selector - {self.params[1]["name"]},'
                                       f' Policy not defined in input.')
            return
        if self.profile_dict['policies'] and self.params[1]['policy_group'] not in self.profile_dict['policies']:
            ACIfabric.session.log.error(f'LeafSwitchProf: {self.name}, configured Policy-group - '
                                        f'{self.profile_dict["policies"]} is different than one in input data: '
                                        f'{self.params[1]["policy_group"]} - LeafSwitchProf can use only one PolGrp')
            ACIfabric.ACI_errors.append(f'LeafSwitchProf: {self.name}, configured Policy-group - '
                                        f'{self.profile_dict["policies"]} is different than one in input data: '
                                        f'{self.params[1]["policy_group"]} - LeafSwitchProf can use only one PolGrp')
            return
        policy_payload = {
            "infraLeafS": {
                "attributes": {
                    "dn": f"{self.dn}/leaves-{self.params[1]['name']}-typ-range"
                },
                "children": [
                    {
                        "infraRsAccNodePGrp": {
                            "attributes": {
                                "tDn": f"uni/infra/funcprof/accnodepgrp-{self.params[1]['policy_group']}"
                            },
                            "children": self.params[1].get("policy_group_children", [])
                        }
                    }
                ]
            }
        }
        url = ACIfabric.base_API_url + self.dn + f'/leaves-{self.params[1]["name"]}-typ-range.json'
        ACIfabric.create(f'LeafSwitchProf: {self.name}, Selector: {self.params[1]["name"]}, '
                         f'applying Policy: {self.params[1]["policy_group"]}', url, policy_payload)

    def attach_int_profile(self):
        for prof in self.int_profiles:
            if prof in self.profile_dict['int_profs']:
                ACIfabric.session.log.info(f'LeafSwitchProf: {self.name}, '
                                           f'Interface Profile - {prof}, already configured.')
                continue
            p_payload = {
                "infraRsAccPortP": {
                    "attributes": {
                        "tDn": f"uni/infra/accportprof-{prof}"
                    },
                    "children": []
                }
            }
            ACIfabric.create(f'LeafSwitchProf: {self.name}, attaching Interface Profile: {prof}', self.url, p_payload)

import ErrorHandler
import argparse
import DataHandler
import FabricObjectsCLS
from RESTapi import RESTaci


def create_vpc_domain(params):
    data.params_check(params, [(1, 'name'), (1, 'vpc_id')])
    vpc_domain = FabricObjectsCLS.VPCdomain(params)
    if not vpc_domain.exist:
        vpc_domain.create()
    else:
        REST_session.log.info(f'VPCdomain: {vpc_domain.name} already exists')


def create_interface_policy(params):
    data.params_check(params, [(1, 'name')])
    if "cdp" in params[0].lower():
        int_pol = FabricObjectsCLS.IntPolCDP(params[1])
        if not int_pol.exist:
            int_pol.create()
    elif "lldp" in params[0].lower():
        int_pol = FabricObjectsCLS.IntPolLLDP(params[1])
        if not int_pol.exist:
            int_pol.create()
    elif "port channel" in params[0].lower():
        int_pol = FabricObjectsCLS.IntPolPC(params[1])
        if not int_pol.exist:
            int_pol.create()


def create_global_policy(params):
    data.params_check(params, [(1, 'name')])
    aaep = FabricObjectsCLS.GlobPol(params[1])
    if not aaep.exist:
        aaep.create()
    aaep.attach_phys_domain()


def create_pool(params):
    data.params_check(params, [(1, 'name')])
    if 'vlan' in params[0].lower():
        vlan_pool = FabricObjectsCLS.VlanPool(params[1])
        if not vlan_pool.exist:
            vlan_pool.create()
        vlan_pool.fill_children_list()


def create_domain(params):
    data.params_check(params, [(0, 'name')])
    if 'phys' in params[0]['name'].lower():
        phys_dom = FabricObjectsCLS.PhysDomain(params[0])
        if not phys_dom.exist:
            phys_dom.create()
        phys_dom.assign_pool()


def create_leaf_interface_policy_group(params):
    data.params_check(params, [(1, 'name')])
    if 'access port' in params[0].lower():
        leaf_acc_int_pol_grp = FabricObjectsCLS.LeafAccessIntPolGrp(params[1])
        if not leaf_acc_int_pol_grp.exist:
            leaf_acc_int_pol_grp.create()
    elif 'vpc' in params[0].lower():
        leaf_vpc_int_pol_grp = FabricObjectsCLS.LeafVPCIntPolGrp(params[1])
        if not leaf_vpc_int_pol_grp.exist:
            leaf_vpc_int_pol_grp.create()


def create_leaf_interface_profile(params):
    data.params_check(params, [(1, 'name'), (1, 'toPort')])
    leaf_int_prof = FabricObjectsCLS.LeafIntProf(params)
    if not leaf_int_prof.exist:
        leaf_int_prof.create()


def create_leaf_switch_policy_group(params):
    data.params_check(params, [(0, 'name')])
    leaf_sw_pol_grp = FabricObjectsCLS.LeafSwitchPolGrp(params[0])
    if not leaf_sw_pol_grp.exist:
        leaf_sw_pol_grp.create()


def create_leaf_switch_profile(params):
    leaf_sw_profile = FabricObjectsCLS.LeafSwitchProf(params)
    if not leaf_sw_profile.exist:
        leaf_sw_profile.create()


def display_dry_log():
    print('DRY-RUN Log:')
    log_list = []
    for log in ACI.dry_log:
        if log not in log_list:
            print(log)
            log_list.append(log)
    if ACI.ACI_errors:
        print('!!! Below errors occurred during dry-run: !!!')
        for error in ACI.ACI_errors:
            print(error)
        print('-' * 100)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", type=str, help="Name of file including input data")
    parser.add_argument("-u", "--url", type=str, help="APIC URL")
    args = parser.parse_args()
    if args.url:
        url = args.url
    else:
        url = 'https://192.168.10.1'
    if args.file:
        input_data_file = args.file
    else:
        input_data_file = 'InputFiles/TestFabricInputFile'
        # input_data_file = 'FabricGen/FabricAutoGenOut'
    REST_session = RESTaci('admin', 'ciscocisco', url)
    ACI = FabricObjectsCLS.ACIfabric(url, REST_session, dry=True)
    data = DataHandler.DataHandlerInfra(input_data_file, log=REST_session.log)
    for path in data.paths_sorted_by_priority:
        if path[-1] == 'create_pool':
            create_pool(path[2:-1])
        elif path[-1] == 'create_switch_policy':
            create_vpc_domain(path[3:-1])
        elif path[-1] == 'create_interface_policy':
            create_interface_policy(path[3:-1])
        elif path[-1] == 'create_global_policy':
            create_global_policy(path[3:-1])
        elif path[-1] == 'create_domain':
            create_domain(path[3:-1])
        elif path[-1] == 'create_leaf_interface_policy_group':
            create_leaf_interface_policy_group(path[4:-1])
        elif path[-1] == 'create_leaf_interface_profile':
            create_leaf_interface_profile(path[4:-1])
        elif path[-1] == 'create_leaf_switch_policy_group':
            create_leaf_switch_policy_group(path[4:-1])
        elif path[-1] == 'create_leaf_switch_profile':
            create_leaf_switch_profile(path[4:-1])
        else:
            raise ErrorHandler.MissingMethodError(log=REST_session.log, msg=path)
    REST_session.log.info(' !!! --- SESSION - FABRIC - REST - END --- !!! ')
    if ACI.dry:
        display_dry_log()

import cobra.mit.access
import cobra.mit.request
import cobra.mit.session
import cobra.model.fv
import cobra.model.pol
import cobra.model.fabric
import cobra.model.infra
import cobra.model.cdp
import cobra.model.lldp
import cobra.model.lacp
import cobra.model.fvns
import cobra.model.phys
import ErrorHandler
import argparse
import DataHandler
from SDKsession import SDKsession



def create_switch_policy(params):
    """ Creates VPC domains under 'Policies -> Switch -> virtual port channel default'
    Checks if all necessary parameters are present in passed 'params' argument (SDK_session.params_check).
    Checks if objects have been configured successfully.
    """

    if params[0].lower() == 'virtual port channel default':
        SDK_session.params_check(params, [(1, 'name'), (1, 'vpc_id'), (1, 'sw_1_id'), (1, 'sw_2_id')])
        name = params[1]['name']
        fabricInst = cobra.model.fabric.Inst(SDK_session.polUni)
        fabricProtPol = cobra.model.fabric.ProtPol(fabricInst)
        fabricExplicitGEp = cobra.model.fabric.ExplicitGEp(fabricProtPol, name=name, id=params[1]['vpc_id'])
        fabricNodePEp = cobra.model.fabric.NodePEp(fabricExplicitGEp, id=params[1]['sw_1_id'])
        fabricNodePEp2 = cobra.model.fabric.NodePEp(fabricExplicitGEp, id=params[1]['sw_2_id'])
        SDK_session.cobra_commit(fabricProtPol)
        SDK_session.check_if_created('fabricExplicitGEp', name, 'create_switch_policy', params)


def create_interface_policy(params):
    """ Creates Interface level policies under 'Policies -> Interface -> policy'
    Checks if all necessary parameters are present in passed 'params' argument (SDK_session.params_check).
    Only CDP, LLDP and port-channel related policies have been implemented at the moment.
    Checks if objects have been configured successfully.
    """

    if "cdp" in params[0].lower():
        SDK_session.params_check(params, [(1, 'name')])
        name = params[1]['name']
        enabled = params[1].get('adminSt', '')
        if enabled == 'enabled':
            cdpIfPol = cobra.model.cdp.IfPol(SDK_session.infraInfra, name=name, adminSt='enabled')
        else:
            cdpIfPol = cobra.model.cdp.IfPol(SDK_session.infraInfra, name=name, adminSt='disabled')
        SDK_session.cobra_commit(SDK_session.infraInfra)
        SDK_session.check_if_created('cdpIfPol', name, 'create_interface_policy', params)
    elif "lldp" in params[0].lower():
        SDK_session.params_check(params, [(1, 'name')])
        name = params[1]['name']
        adminRxSt = params[1].get('adminRxSt', '')
        adminTxSt = params[1].get('adminTxSt', '')
        if adminRxSt == 'disabled' and  adminTxSt == 'disabled':
            lldpIfPol = cobra.model.lldp.IfPol(SDK_session.infraInfra, name=name, adminRxSt='disabled',
                                               adminTxSt='disabled')
        else:
            lldpIfPol = cobra.model.lldp.IfPol(SDK_session.infraInfra, name=name)
        SDK_session.cobra_commit(SDK_session.infraInfra)
        SDK_session.check_if_created('lldpIfPol', name, 'create_interface_policy', params)
    elif "port channel" in params[0].lower():
        SDK_session.params_check(params, [(1, 'name')])
        name = params[1]['name']
        minLinks = params[1].get('minLinks', '1')
        if "Static" in name:
            lacpLagPol = cobra.model.lacp.LagPol(SDK_session.infraInfra, name=name, minLinks=minLinks)
        elif "Active" in name:
            lacpLagPol = cobra.model.lacp.LagPol(SDK_session.infraInfra, name=name, mode='active', minLinks=minLinks)
        elif "Passive" in name:
            lacpLagPol = cobra.model.lacp.LagPol(SDK_session.infraInfra, name=name, mode='passive', minLinks=minLinks)
        else:
            raise ErrorHandler.MissingArgumentError(log=SDK_session.log, msg=f'{name} - missing port-channel mode')
        SDK_session.cobra_commit(SDK_session.infraInfra)
        SDK_session.check_if_created('lacpLagPol', name, 'create_interface_policy', params)


def create_global_policy(params):
    """ Creates AAEPs under 'Policies -> Global -> Attachable Access Entity Profiles -> aaep'.
    Assigns AAEP to a physical domain if domain is present in params.
    Checks if all necessary parameters are present in passed 'params' argument (SDK_session.params_check).
    Checks if objects have been configured successfully.
    """

    if 'attachable access entity profiles' in params[0].lower():
        SDK_session.params_check(params, [(1, 'name')])
        name = params[1]['name']
        domain = params[1].get('domain', '')
        infraAttEntityP = cobra.model.infra.AttEntityP(SDK_session.infraInfra, name=name)
        if domain:
            infraRsDomP = cobra.model.infra.RsDomP(infraAttEntityP, tDn=f'uni/phys-{domain}')
        infraFuncP = cobra.model.infra.FuncP(SDK_session.infraInfra)
        SDK_session.cobra_commit(SDK_session.polUni)
        SDK_session.check_if_created('infraAttEntityP', name, 'create_global_policy', params)


def create_pool(params):
    """ Creates Vlan Pools under 'Pools -> Vlan' and Encap Blocks under proper Vlan Pools.
    Encap Blocks are parameters for vlan pools. Vlan Pool can be either 'dynamic' or 'static' which is described by
    parameter named - 'allocMode' (default value is 'dynamic').
    Checks if all necessary parameters are present in passed 'params' argument (SDK_session.params_check).
    Checks if objects have been configured successfully.
    """

    if "vlan" in params[0].lower():
        SDK_session.params_check(params, [(1, 'name')])
        name = params[1]['name']
        allocMode = params[1].get('allocMode', '')
        vlan_pool_obj = SDK_session.check_if_created('fvnsVlanInstP', name, 'create_pool', params, pre_check=True)
        if vlan_pool_obj and not allocMode:
            allocMode = vlan_pool_obj.allocMode
        elif not vlan_pool_obj and not allocMode:
            allocMode =  'dynamic'
        vlans_list = params[1].get('vlans', [])
        for vlan in vlans_list:
            vlan_beg = 'vlan-' + str(vlan.split('-')[0])
            vlan_end = 'vlan-' + str(vlan.split('-')[1])
            fvnsVlanInstP = cobra.model.fvns.VlanInstP(SDK_session.infraInfra, name=name, allocMode=allocMode)
            fvnsEncapBlk = cobra.model.fvns.EncapBlk(fvnsVlanInstP, from_=vlan_beg, to=vlan_end)
            SDK_session.cobra_commit(SDK_session.infraInfra)
        SDK_session.check_if_created('fvnsVlanInstP', name, 'create_pool', params)


def create_domain(params):
    """ Creates Physical Domains under 'Physical and External Domains -> Physical Domains' and assigns proper vlan pools
    to the domain - if vlan pool is present in params.
    Checks if all necessary parameters are present in passed 'params' argument (SDK_session.params_check).
    Checks if objects have been configured successfully.
    """

    SDK_session.params_check(params, [(0, 'name')])
    name = params[0]['name']
    vlan_pool_name = params[0].get('vlan_pool', False)
    physDomP = cobra.model.phys.DomP(SDK_session.polUni, name=name)
    if vlan_pool_name:
        vlan_pool_obj = SDK_session.check_if_created('fvnsVlanInstP', vlan_pool_name, 'create_domain', params,
                                                     pre_check=True)
        if vlan_pool_obj:
            vlan_pool_mode = vlan_pool_obj.allocMode
        else:
            vlan_pool_mode = 'dynamic'
        infraRsVlanNs = cobra.model.infra.RsVlanNs(physDomP, tDn=f'uni/infra/vlanns-[{vlan_pool_name}]-{vlan_pool_mode}')
    SDK_session.cobra_commit(SDK_session.polUni)
    SDK_session.check_if_created('physDomP', name, 'create_domain', params)


def create_leaf_interface_policy_group(params):
    """ Creates Leaf Interface Policies under:
    'Interfaces -> Leaf Interfaces -> Policy Groups - > Leaf Access Port' - for standalone access ports
    'Interfaces -> Leaf Interfaces -> Policy Groups - > VPC Interface' - for VPC port-channels
    CDP, LLDP policies are implemented for access ports. CDP, LLDP and port-channel policies are implemented for VPC.
    Assigns AAEP to a Policy Group if aaep is present in passed params argument (dictionary).
    Checks if all necessary parameters are present in passed 'params' argument (SDK_session.params_check).
    Checks if objects have been configured successfully.
    """

    SDK_session.params_check(params, [(1, 'name')])
    name = params[1]['name']
    if "access port" in params[0].lower():
        infraFuncP = cobra.model.infra.FuncP(SDK_session.infraInfra)
        infraAccPortGrp = cobra.model.infra.AccPortGrp(infraFuncP, name=name)
        aaep = params[1].get('aaep', False)
        if aaep:
            infraRsAttEntP = cobra.model.infra.RsAttEntP(infraAccPortGrp, tDn=f'uni/infra/attentp-{aaep}')
        policies = params[1].get('policies', False)
        if policies:
            for policy in policies:
                if 'cdp' in policy.lower():
                    infraRsCdpIfPol = cobra.model.infra.RsCdpIfPol(infraAccPortGrp, tnCdpIfPolName=policy)
                elif 'lldp' in policy.lower():
                    infraRsLldpIfPol = cobra.model.infra.RsLldpIfPol(infraAccPortGrp, tnLldpIfPolName=policy)
        SDK_session.cobra_commit(infraFuncP)
        SDK_session.check_if_created('infraAccPortGrp', name, 'create_leaf_interface_policy_group', params)
    elif 'vpc' in params[0].lower():
        infraFuncP = cobra.model.infra.FuncP(SDK_session.infraInfra)
        infraAccBndlGrp = cobra.model.infra.AccBndlGrp(infraFuncP, lagT='node', name=name)
        aaep = params[1].get('aaep', False)
        if aaep:
            infraRsAttEntP = cobra.model.infra.RsAttEntP(infraAccBndlGrp, tDn=f'uni/infra/attentp-{aaep}')
        policies = params[1].get('policies', False)
        if policies:
            for policy in policies:
                if 'cdp' in policy.lower():
                    infraRsCdpIfPol = cobra.model.infra.RsCdpIfPol(infraAccBndlGrp, tnCdpIfPolName=policy)
                elif 'lldp' in policy.lower():
                    infraRsLldpIfPol = cobra.model.infra.RsLldpIfPol(infraAccBndlGrp, tnLldpIfPolName=policy)
        pcpolicy = params[1].get('port-channel policy', False)
        if pcpolicy:
            infraRsLacpPol = cobra.model.infra.RsLacpPol(infraAccBndlGrp, tnLacpLagPolName=pcpolicy)
        SDK_session.cobra_commit(infraFuncP)
        SDK_session.check_if_created('infraAccBndlGrp', name, 'create_leaf_interface_policy_group', params)


def create_leaf_interface_profile(params):
    """ Creates Leaf Interface Profiles under 'Interfaces -> Leaf Interfaces -> Profiles'
    Creates Interface Selectors (one per port - that is the approach at the moment) under proper Profile
    and assigns proper Policy Groups to those selectors.
    Policy Groups can be of two types - VPC for port-channels and Access for standalone ports.
    Checks if all necessary parameters are present in passed 'params' argument (SDK_session.params_check).
    Checks if objects have been configured successfully.
    """

    name = params[0]
    SDK_session.params_check(params, [(1, 'name'), (1, 'toPort')])
    selector_name = params[1]['name']
    toPort = str(params[1]['toPort'])
    fromPort = params[1].get('fromPort', '1')
    block = params[1].get('block', 'block2')
    policy_group = params[1].get('policy_group', '')
    infraAccPortP = cobra.model.infra.AccPortP(SDK_session.infraInfra, name=name)
    infraHPortS = cobra.model.infra.HPortS(infraAccPortP, type='range', name=selector_name)
    infraPortBlk = cobra.model.infra.PortBlk(infraHPortS, fromPort=fromPort ,toPort=toPort, name=block)
    if policy_group:
        if 'vpc' in policy_group.lower():
            infraRsAccBaseGrp = cobra.model.infra.RsAccBaseGrp(infraHPortS,
                                                               tDn=f'uni/infra/funcprof/accbundle-{policy_group}')
        else:
            infraRsAccBaseGrp = cobra.model.infra.RsAccBaseGrp(infraHPortS,
                                                                tDn=f'uni/infra/funcprof/accportgrp-{policy_group}')
    SDK_session.cobra_commit(SDK_session.infraInfra)
    SDK_session.check_if_created('infraAccPortP', name, 'create_leaf_interface_profile', params)


def create_leaf_switch_policy_group(params):
    """ Creates Leaf Switch Policy Groups under 'Switches -> Leaf Switches -> Policy Groups'.
    CDP and LLDP policies have been implemented so far - other policies will be added on request.
    Checks if all necessary parameters are present in passed 'params' argument (SDK_session.params_check).
    Checks if objects have been configured successfully.
    """

    SDK_session.params_check(params, [(0, 'name')])
    name = params[0]['name']
    infraFuncP = cobra.model.infra.FuncP(SDK_session.infraInfra)
    infraAccNodePGrp = cobra.model.infra.AccNodePGrp(infraFuncP, name=name)
    policies = params[0].get('policies', False)
    if policies:
        for policy in policies:
            if 'cdp' in policy.lower():
                infraRsLeafPGrpToCdpIfPol = cobra.model.infra.RsLeafPGrpToCdpIfPol(infraAccNodePGrp,
                                                                                   tnCdpIfPolName=policy)
            elif 'lldp' in policy.lower():
                infraRsLeafPGrpToLldpIfPol = cobra.model.infra.RsLeafPGrpToLldpIfPol(infraAccNodePGrp,
                                                                                     tnLldpIfPolName=policy)
    SDK_session.cobra_commit(infraFuncP)
    SDK_session.check_if_created('infraAccNodePGrp', name, 'create_leaf_switch_policy_group', params)


def create_leaf_switch_profile(params):
    """ Creates Leaf Switch Profiles under 'Switches -> Leaf Switches -> Profiles'.
    Creates Switch Selectors under proper Profiles. The policy is - one switch profile per VPC domain
    (for port-channels, with selector covering both switches from VPC domain)
    and additional one per switch (for standalone ports).
    Assigns proper Policy Groups to Selectors.
    Checks if all necessary parameters are present in passed 'params' argument (SDK_session.params_check).
    Checks if objects have been configured successfully.
    """

    name = params[0]
    fromLeaf = params[1].get('fromLeaf', '')
    toLeaf = params[1].get('toLeaf', '')
    selector_name = params[1].get('name', '')
    policy_group_name = params[1].get('policy_group', '')
    interface_profiles = params[1].get('int_profiles', '')
    infraNodeP = cobra.model.infra.NodeP(SDK_session.infraInfra, name=name)
    if (not selector_name or not fromLeaf or not toLeaf) and not policy_group_name:
        SDK_session.cobra_commit(SDK_session.infraInfra)
    for interface in interface_profiles:
        if selector_name and fromLeaf and toLeaf:
            infraLeafS = cobra.model.infra.LeafS(infraNodeP, type='range', name=selector_name)
            infraNodeBlk = cobra.model.infra.NodeBlk(infraLeafS, from_=fromLeaf, to_=toLeaf, name=selector_name)
        if policy_group_name:
            infraRsAccNodePGrp = cobra.model.infra.RsAccNodePGrp(
                infraLeafS, tDn=f'uni/infra/funcprof/accnodepgrp-{policy_group_name}')
        infraRsAccPortP = cobra.model.infra.RsAccPortP(infraNodeP, tDn=f'uni/infra/accportprof-{interface}')
        SDK_session.cobra_commit(SDK_session.infraInfra)
    SDK_session.check_if_created('infraNodeP', name, 'create_leaf_switch_profile', params)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", type=str, help="Name of file including input data")
    parser.add_argument("-u", "--url", type=str, help="APIC URL")
    args = parser.parse_args()
    if args.url:
        URL = args.url
    else:
        URL = 'https://192.168.10.10'
    if args.file:
        input_data_file = args.file
    else:
        #input_data_file = 'FabricGen/FabricAutoGenOut'
        input_data_file = '../InputFiles/TestFabricInputFile'
    SDK_session = SDKsession('admin', 'ciscocisco', URL)
    data = DataHandler.DataHandlerInfra(input_data_file, log=SDK_session.log)

    # checks last item in a path which is a pointer to proper function
    # removes last item and path to the object itself so all passed parameters are object related
    # invokes function with 'path' list or dictionary as parameter

    for path in data.paths_sorted_by_priority:
        if path[-1] == 'create_pool':
            del path[-1]; del path[:2]
            create_pool(path)
        elif path[-1] == 'create_switch_policy':
            del path[-1]; del path[:3]
            create_switch_policy(path)
        elif path[-1] == 'create_interface_policy':
            del path[-1]; del path[:3]
            create_interface_policy(path)
        elif path[-1] == 'create_global_policy':
            del path[-1]; del path[:3]
            create_global_policy(path)
        elif path[-1] == 'create_domain':
            del path[-1]; del path[:3]
            create_domain(path)
        elif path[-1] == 'create_leaf_interface_policy_group':
            del path[-1]; del path[:4]
            create_leaf_interface_policy_group(path)
        elif path[-1] == 'create_leaf_interface_profile':
            del path[-1]; del path[:4]
            create_leaf_interface_profile(path)
        elif path[-1] == 'create_leaf_switch_policy_group':
            del path[-1]; del path[:4]
            create_leaf_switch_policy_group(path)
        elif path[-1] == 'create_leaf_switch_profile':
            del path[-1]; del path[:4]
            create_leaf_switch_profile(path)
        else:
            raise ErrorHandler.MissingMethodError(log=SDK_session.log, msg=path)
    SDK_session.logout()
    exit(0)
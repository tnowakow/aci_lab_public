import requests
import urllib3
import logging
import json
import ErrorHandler
from logging import config


class REST:
    """ Class that includes general REST session related variables (url to APIC, password, username, token)
    and functions.
    """

    # disable warnings
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    config_dict = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'consoleFormatter': {
                'format': '%(asctime)s - %(module)15s - %(levelname)-8s - %(name)-12s - %(message)s',
            },
            'fileFormatter': {
                'format': '%(asctime)s - %(module)15s - %(levelname)-8s - %(name)-12s - %(message)s',
            },
        },
        'handlers': {
            'file': {
                'filename': 'logs/ACIrest.log',
                'level': 'DEBUG',
                'class': 'logging.handlers.TimedRotatingFileHandler',
                'formatter': 'fileFormatter',
                'when': 'midnight',
                'backupCount': 7
            },
            'console': {
                'level': 'ERROR',
                'class': 'logging.StreamHandler',
                'formatter': 'consoleFormatter',
            },
        },
        'loggers': {
            '': {
                'handlers': ['file', 'console'],
                'level': 'DEBUG',
            },
        },
    }

    def __init__(self, username, password, url):
        logging.config.dictConfig(REST.config_dict)
        self.log = logging.getLogger(__name__)
        self.__username = username
        self.__password = password
        self.url = url
        self.status = False
        self.session = requests.session()
        self.headers = {}

    def invoke(self, url, method='GET', data=None, requester=None):
        """ Function that includes REST methods
        'GET_with_filter - ACI specific, checks if item exist in ACI
                         - based on item class and item specific parameter (name, dn, etc.)'
        Returns False if response status != 200 or json otherwise.
        """
        response = None
        if data is None:
            data = {}
        if method == 'GET':
            response = self.session.get(url, verify=False, headers=self.headers, data=data)
        elif method == 'GET_with_filter':
            url += f'?query-target-filter=eq({data["class"]}.{data["parameter_type"]},' \
                   f'"{data["parameter_value"]}")'
            response = self.session.get(url, verify=False, headers=self.headers)
        elif method == 'POST':
            response = self.session.post(url, verify=False, headers=self.headers, data=data)
        elif method == 'PATCH':
            response = self.session.patch(url, verify=False, headers=self.headers, json=data)
        elif method == 'PUT':
            response = self.session.put(url, verify=False, headers=self.headers, json=data)
        if not response:
            if requester:
                self.log.error(f"ERROR - '{self.url, method}', requester: {requester}, response: {response.text}")
            else:
                self.log.error(f"ERROR - '{self.url, method}', response: {response.text}")
            return False
        if response.status_code != 200:
            if requester:
                self.log.error(f"ERROR - '{self.url, method}', returned status code: {response.status_code}, "
                               f"\nresponse: {response.text},\nurl: {response.url}, requester: {requester}")
            else:
                self.log.error(f"ERROR - '{self.url, method}', returned status code: {response.status_code}, "
                               f"\nresponse: {response.text},\nurl: {response.url}")
            return False
        else:
            if requester:
                self.log.info(f"OK - {self.url, method}, returned status code: 200, url: {response.url}, data: {data}, "
                              f"requester: {requester}")
            else:
                self.log.info(f"OK - {self.url, method}, returned status code: 200, url: {response.url}, data: {data}")
            return response.json()


class RESTaci(REST):

    def __init__(self, username, password, url):
        super().__init__(username, password, url)
        self.__token_attributes = None
        self.token = None
        self.get_token()

    def get_token(self):
        """ ACI APIC specific function. Obtains auth token - based on username and password. """

        self.__token_attributes = json.dumps({
            "aaaUser": {
                "attributes": {
                    "name": self._REST__username,
                    "pwd": self._REST__password
                }
            }
        })
        login_url = self.url + '/api/aaaLogin.json'
        result = requests.post(url=login_url, data=self.__token_attributes, verify=False)
        result.raise_for_status()
        self.token = result.json()['imdata'][0]['aaaLogin']['attributes']['token']
        if self.token:
            self.log.info(' !!! --- SESSION - REST - START --- !!! ')
            self.log.info(f'OK - {self.url} - token obtained')
            self.headers = {"Cookie": f"APIC-Cookie={self.token}"}
            self.status = True
        else:
            raise ErrorHandler.MissingTokenError(log=self.log, msg=self.url)


class RESTmso(REST):

    def __init__(self, username, password, url):
        super().__init__(username, password, url)
        self.__token_attributes = None
        self.token = None
        self.userId = None
        self.get_token()

    def get_token(self):
        """ ACI MSO specific function. Obtains auth token - based on username and password. """

        self.__token_attributes = json.dumps({
            "username": self._REST__username,
            "password": self._REST__password
        })
        login_url = self.url + '/api/v1/auth/login'
        mso_headers = {'Content-Type': 'application/json'}
        result = requests.post(url=login_url, data=self.__token_attributes, verify=False, headers=mso_headers)
        result.raise_for_status()
        self.token = result.json()['token']
        self.userId = result.json().get('userId', None)
        if self.token:
            self.log.info(' !!! --- SESSION - REST - START --- !!! ')
            self.log.info(f'OK - {self.url} - token obtained')
            self.headers = {"Authorization": "Bearer " + self.token, "Accept": "application/json"}
            self.status = True
        else:
            raise ErrorHandler.MissingTokenError(log=self.log, msg=self.url)

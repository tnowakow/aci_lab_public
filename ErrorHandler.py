class Error(Exception):
    """Base class for other exceptions"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "Error raised with no parameter"
        print(f'Error raised with: {self.msg}')
        if self.log:
            self.log.error(f'Error raised with: {self.msg}')
        exit(1)


class SessionError(Error):
    """Raised when IP is wrong"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "SessionError raised with no parameter"
        print(f'SessionError raised with: {self.msg} - HOST UNRESPONSIVE or WRONG IP ???')
        if self.log:
            self.log.error(f'SessionError raised with: {self.msg} - HOST UNRESPONSIVE or WRONG IP ???')
        exit(1)


class HTTPError(Error):
    """Raised when credentials are wrong"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "HTTPError raised with no parameter"
        print(f'HTTPError raised with: {self.msg} - WRONG CREDENTIALS ???')
        if self.log:
            self.log.error(f'HTTPError raised with: {self.msg} - WRONG CREDENTIALS ???')
        exit(1)


class NameTooLongError(Error):
    """Raised when name of an object is too long"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "NameTooLong raised with no parameter"
        print(f'NameTooLong raised with: {self.msg} - the name is too long - 64 chrs MAX')
        if self.log:
            self.log.error(f'NameTooLong raised with: {self.msg} - the name is too long - 64 chrs MAX')
        exit(1)


class WrongNameError(Error):
    """Raised when name of an object includes forbidden characters"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "WrongNameError raised with no parameter"
        print(f'WrongNameError raised with: {self.msg} - the name can include alphanumerics only and "-" or "_"')
        if self.log:
            self.log.error(f'WrongNameError raised with: {self.msg} - '
                           f'the name can include alphanumerics only and "-" or "_"')
        exit(1)


class InputFileError(Error):
    """Raised when issue with input file occurs"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "InputFileError raised with no parameter"
        print(f'InputFileError raised with: {self.msg} - input file does not exist or corrupted')
        if self.log:
            self.log.error(f'InputFileError raised with: {self.msg} - input file does not exist or corrupted')
        exit(1)


class InputDataError(Error):
    """Raised when issue with input data occurs"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "InputDataError raised with no parameter"
        print(f'InputDataError raised with: {self.msg} - input data is neither list nor dictionary')
        if self.log:
            self.log.error(f'InputDataError raised with: {self.msg} - '
                           f'input data is neither list nor dictionary')
        exit(1)


class WrongIPError(Error):
    """Raised when incorrect IP is included in input JSON file"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "WrongIPError raised with no parameter"
        print(f'WrongIPError raised with: {self.msg} - wrong IP format')
        if self.log:
            self.log.error(f'WrongIPError raised with: {self.msg} - wrong IP format')
        exit(1)


class WrongMACError(Error):
    """Raised when incorrect MAC is included in input JSON file"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "WrongMACError raised with no parameter"
        print(f'WrongMACError raised with: {self.msg} - wrong MAC/CEP format')
        if self.log:
            self.log.error(f'WrongMACError raised with: {self.msg} - wrong MAC/CEP format')
        exit(1)


class MissingParamsError(Error):
    """Raised when 'create' function is invoked with missing parameters"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "MissingParamsError raised with no parameter"
        print(f'MissingParamsError raised with: {self.msg} - function invoked with no arguments')
        if self.log:
            self.log.error(f'MissingParamsError raised with: {self.msg} - function invoked with no arguments')
        exit(1)


class MissingMethodError(Error):
    """Raised when there is a 'create' method missing for an object"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "MissingMethodError raised with no parameter"
        print(f'MissingMethodError raised with: {self.msg} - lack of "create" function for the object')
        if self.log:
            self.log.error(f'MissingMethodError raised with: {self.msg} - '
                           f'lack of "create" function for the object')
        exit(1)


class MissingArgumentError(Error):
    """Raised when one or more key arguments is missing in dictionary"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "MissingArgumentError raised with no parameter"
        print(f'MissingArgumentError raised with: {self.msg} - missing key argument')
        if self.log:
            self.log.error(f'MissingArgumentError raised with: {self.msg} - - missing key argument')
        exit(1)


class SessionTerminatedByUserError(Error):
    """Raised when user terminates the session due to an error"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "SessionTerminatedByUserError raised with no parameter"
        print(f'SessionTerminatedByUserError raised with: {self.msg} - failed to create an object')
        if self.log:
            self.log.error(f'SessionTerminatedByUserError raised with: {self.msg} '
                           f'- failed to create an object')
        exit(1)


class MissingParentError(Error):
    """Raised when parent is missing for an object, during assignment"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "MissingParentError raised with no parameter"
        print(f'MissingParentError raised with: {self.msg} - parent object is missing')
        if self.log:
            self.log.error(f'MissingParentError raised with: {self.msg} - parent object is missing')
        exit(1)


class MissingTokenError(Error):
    """Raised when token could not be obtained from API"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "MissingTokenError raised with no parameter"
        print(f'MissingTokenError raised with: {self.msg} - token could not be obtained!')
        if self.log:
            self.log.error(f'MissingTokenError raised with: {self.msg} - token could not be obtained!')
        exit(1)


class WrongGlobalVARS(Error):
    """Raised when a global variable from input file is missing in running config"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "MissingOrWrongGlobalVARSforMSO raised with no parameter"
        print(f'MissingOrWrongGlobalVARSforMSO raised with: {self.msg} - missing or wrong global variables for MSO!')
        if self.log:
            self.log.error(f'MissingOrWrongGlobalVARSforMSO raised with: {self.msg} '
                           f'- missing or wrong global variables for MSO!')
        exit(1)


class MissingOrWrongTenant(Error):
    """Raised when tenant from input file is missing in running config"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "MissingOrWrongTenant raised with no parameter"
        print(f'MissingOrWrongTenant raised with: {self.msg} - missing or wrong tenant!')
        if self.log:
            self.log.error(f'MissingOrWrongTenant raised with: {self.msg} - missing or wrong tenant!')
        exit(1)


class MutualExlusiveParametersSet(Error):
    """Raised when mutually exclusive parameters have been configured for an object"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "MutualExlusiveParametersSet raised with no parameter"
        print(f'MutualExlusiveParametersSet raised with: {self.msg} - '
              f'mutually exclusive parameters have been configured for an object!')
        if self.log:
            self.log.error(f'MutualExlusiveParametersSet raised with: {self.msg} '
                           f'mutually exclusive parameters have been configured for an object')
        exit(1)


class InvalidVlanMode(Error):
    """Raised when vlan block mode is not compatible with vlan pool mode"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "InvalidVlanMode raised with no parameter"
        print(f'InvalidVlanMode raised with: {self.msg} - VLAN mode not valid! dynamic block is not allowed under '
              f'static pool')
        if self.log:
            self.log.error(f'InvalidVlanMode raised with: {self.msg} - VLAN mode not valid! dynamic block is not '
                           f'allowed under static pool')
        exit(1)


class NonExistingVlanPool(Error):
    """Raised when non-existing vlan pool is being assigned to a domain"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "NonExistingVlanPool raised with no parameter"
        print(f'NonExistingVlanPool raised with: {self.msg} - vlan pool does not exist in APIC')
        if self.log:
            self.log.error(f'NonExistingVlanPool raised with: {self.msg} - vlan pool does not exist in APIC')
        exit(1)


class WrongVlanPool(Error):
    """Raised when a different vlan pool has been found for PhysDomain"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "WrongVlanPool raised with no parameter"
        print(f'WrongVlanPool raised with: {self.msg}')
        if self.log:
            self.log.error(f'WrongVlanPool raised with: {self.msg}')
        exit(1)


class WrongAAEP(Error):
    """Raised when a different aaep has been found for Interface Policy or AAEP format is wrong"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "WrongAAEP raised with no parameter"
        print(f'WrongAAEP raised with: {self.msg}')
        if self.log:
            self.log.error(f'WrongAAEP raised with: {self.msg}')
        exit(1)


class UnknownPolType(Error):
    """Raised when a policy that has not been implemented yet was found in input data"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "UnknownPolType raised with no parameter"
        print(f'UnknownPolType raised with: {self.msg} - policy not implemented in script')
        if self.log:
            self.log.error(f'UnknownPolType raised with: {self.msg} - policy not implemented in script')
        exit(1)


class NonExistingObject(Error):
    """Raised when a non-existing object is being called"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "NonExistingObject raised with no parameter"
        print(f'NonExistingObject raised with: {self.msg} - non-existing object called')
        if self.log:
            self.log.error(f'NonExistingObject raised with: {self.msg} - non-existing object called')
        exit(1)


class WrongLeafIntPolicy(Error):
    """Raised when a configured Leaf Interface Policy is different than one in input data"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "WrongLeafIntPolicy raised with no parameter"
        print(f'WrongLeafIntPolicy raised with: {self.msg} - different policy already configured.')
        if self.log:
            self.log.error(f'WrongLeafIntPolicy raised with: {self.msg} - different policy already configured.')
        exit(1)


class TestErrorDEBUG(Error):
    """TEST ERROR CLASS - for debugging purposes"""

    def __init__(self, log=None, msg=None):
        self.msg = msg
        self.log = log
        if not self.msg:
            self.msg = "TestError_DEBUG raised with no parameter"
        print(f'TestError_DEBUG raised with: {self.msg} - !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        if self.log:
            self.log.error(f'TestError_DEBUG raised with: {self.msg} - !!!!!!!!!!!!!!!!!!!!!!')
        exit(1)

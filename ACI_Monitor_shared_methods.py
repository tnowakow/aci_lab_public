import ipaddress
import acitoolkit.acitoolkit as aci
from acitoolkit.aciphysobject import Node
from ipaddr import IPNetwork


def est_session(url_apic):
    login = 'admin'
    password = 'ciscocisco'
    session_apic = aci.Session(url_apic, login, password)
    resp = session_apic.login()
    if resp.status_code != 200:
        session_apic.close()
        return None
    return session_apic


def formatted_print(l_var, headers):
    l_max = []
    for v in headers:
        l_max.append(len(v)+1)
    for val in l_var:
        for var in range(len(val)):
            if val[var]:
                if len(val[var]) > l_max[var]:
                    l_max[var] = len(val[var])
            else:
                if l_max[var] < 6:
                    l_max[var] = 6
    tmpl = ''
    for l_val in range(len(l_max)):
        tmpl += '{' + str(l_val) + ':' + str(l_max[l_val]+1) + '} '
    tmpl = tmpl.rstrip()
    dashes = ["-" * l_max[x] for x in range(len(l_max))]
    print(tmpl.format(*headers))
    print(tmpl.format(*dashes))
    disp_data(l_var, tmpl)


def disp_data(data, tmpl):
    for val in data:
        try:
            print(tmpl.format(*val))
        except TypeError:
            myrec = ['None' if v is None else v for v in list(val)]
            print(tmpl.format(*myrec))


def disp_nodes(session_apic):
    data = []
    items = Node.get(session_apic)
    for item in items:
        data.append((item.serial, item.role, item.node, item.health, item.model, item.oob_mgmt_ip, item.tep_ip))
    if not data:
        print(f'{session_apic.ipaddr} - no NODES to display.')
        return
    data = sorted(data)
    formatted_print(data, ("Serial Number", "Role", "Node", "Health", "Model", "OOB Address", "TEP Address"))


def disp_epgs(session_apic, tenants_to_skip):
    data = []
    tenants = aci.Tenant.get_deep(session_apic)
    for tenant in tenants:
        if tenant.name in tenants_to_skip:
            continue
        apps = aci.AppProfile.get(session_apic, tenant)
        for app in apps:
            epgs = aci.EPG.get(session_apic, app, tenant)
            epgs_list = []
            for epg in epgs:
                epgs_list.append(epg.name)
            data.append((tenant.name, app.name, str(epgs_list)))
    if not data:
        print(f'{session_apic.ipaddr} - no EPGs to display.')
        return
    formatted_print(data, ("Tenant", "App Profile", "EPGs"))


def disp_endpoints(session_apic):
    data = []
    endpoints = aci.Endpoint.get(session_apic)
    num_of_eps = 0
    for ep in endpoints:
        epg = ep.get_parent()
        app_profile = epg.get_parent()
        tenant = app_profile.get_parent()
        data.append((ep.mac, ep.ip, ep.if_name, ep.encap, tenant.name, app_profile.name, epg.name))
        num_of_eps += 1
    if not data:
        print(f'{session_apic.ipaddr} - no ENDPOINTS to display.')
        return
    formatted_print(data, ("MACADDRESS", "IPADDRESS", "INTERFACE", "ENCAP", "TENANT", "APP PROFILE", "EPG"))
    print(f'Total number of ENDPOINTS - {num_of_eps}')


def disp_contracts(session_apic, tenants_to_skip):
    data = []
    tenants = aci.Tenant.get_deep(session_apic)
    for tenant in tenants:
        if tenant.name in tenants_to_skip:
            continue
        contracts = aci.Contract.get(session_apic, tenant)
        data.append((tenant.name, str([c.name for c in contracts])))
    formatted_print(data, ("Tenant", "Contracts"))


def find_host(session_apic, search_str_host, ret=False):
    data = []
    endpoints = aci.Endpoint.get(session_apic)
    for ep in endpoints:
        epg = ep.get_parent()
        app_profile = epg.get_parent()
        tenant = app_profile.get_parent()
        tmp_tup = (ep.mac, ep.ip, ep.if_name, ep.encap, tenant.name, app_profile.name, epg.name)
        if any(search_str_host in str(s).lower() for s in tmp_tup):
            data.append(tmp_tup)
    if ret:
        return data
    if not data:
        print(f'{session_apic.ipaddr} - no HOSTS found.')
        return
    formatted_print(data, ("MACADDRESS", "IPADDRESS", "INTERFACE", "ENCAP", "TENANT", "APP PROFILE", "EPG"))


def find_node(session_apic, search_str_node):
    data = []
    items = Node.get(session_apic)
    for item in items:
        tmp_tup = (item.serial, item.role, item.node, item.health, item.model, item.oob_mgmt_ip, item.tep_ip)
        if any(search_str_node in str(s).lower() for s in tmp_tup):
            data.append(tmp_tup)
    if not data:
        print(f'{session_apic.ipaddr} - no NODES found.')
        return
    data = sorted(data)
    formatted_print(data, ("Serial Number", "Role", "Node", "Health", "Model", "OOB Address", "TEP Address"))


def find_ip(session_apic, search_str_ip):
    data = []
    tenants = aci.Tenant.get_deep(session_apic)
    for tenant in tenants:
        bds = aci.BridgeDomain.get(session_apic, tenant)
        for bd in bds:
            subnets = aci.Subnet.get(session_apic, bd, tenant)
            for subnet in subnets:
                net = IPNetwork(subnet.addr)
                if net.Contains(IPNetwork(search_str_ip)):
                    data.append((tenant.name, bd.name, subnet.addr, subnet.get_scope()))
    if not data:
        print(f'{search_str_ip} - IP not found.')
        return
    formatted_print(data, ("Tenant", "Bridge-Domain", "Subnet", "BD-Properties"))


def ip_checker(msg):
    while True:
        ip_to_check = input(msg)
        if ip_to_check.lower() == 'q':
            return False
        try:
            ipaddress.ip_network(ip_to_check, strict=False)
            break
        except ValueError:
            print('Wrong format. Provide correct IP address.')
            continue
    return ip_to_check
